-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- seleccionamos la base de datos
USE diccionarios;

--
-- Estructura de tabla para la tabla `provincias`
--

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `provincias`;

-- la recreamos
CREATE TABLE IF NOT EXISTS `provincias` (
  `ID` smallint(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `PAIS` smallint(3) UNSIGNED NOT NULL,
  `NOM_PROV` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `COD_PROV` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `POBLACION` int(8) UNSIGNED NOT NULL,
  `USUARIO` int(4) UNSIGNED NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NOM_PROV` (`NOM_PROV`),
  UNIQUE KEY `COD_PROV` (`COD_PROV`),
  KEY `USUARIO` (`USUARIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de Provincias Georeferenciadas';

--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`ID`, `PAIS`, `NOM_PROV`, `COD_PROV`, `POBLACION`, `USUARIO`, `FECHA_ALTA`) VALUES
(1, 1, 'SANTA FE', '82', 3194537, 1, '2011-09-05'),
(2, 1, 'SAN LUIS', '74', 432310, 1, '2011-09-05'),
(3, 1, 'SALTA', '66', 1214441, 1, '2011-09-05'),
(4, 1, 'LA PAMPA', '42', 318951, 1, '2011-09-05'),
(5, 1, 'CORDOBA', '14', 3308876, 1, '2011-09-05'),
(6, 1, 'JUJUY', '38', 673307, 202, '2017-11-01'),
(7, 1, 'CORRIENTES', '18', 992595, 1, '2011-09-05'),
(8, 1, 'CHACO', '22', 1055259, 1, '2011-09-05'),
(9, 1, 'FORMOSA', '34', 530162, 1, '2011-09-05'),
(10, 1, 'MISIONES', '54', 1101593, 1, '2011-09-05'),
(11, 1, 'ENTRE RIOS', '30', 1235994, 1, '2011-09-05'),
(12, 1, 'RIO NEGRO', '62', 638645, 1, '2011-09-05'),
(13, 1, 'NEUQUEN', '58', 551266, 1, '2011-09-05'),
(14, 1, 'MENDOZA', '50', 1738929, 1, '2011-09-05'),
(15, 1, 'LA RIOJA', '46', 333642, 1, '2011-09-05'),
(16, 1, 'SAN JUAN', '70', 681055, 30, '2016-07-25'),
(17, 1, 'TUCUMAN', '90', 1448188, 1, '2011-09-05'),
(18, 1, 'SANTIAGO DEL ESTERO', '86', 874006, 1, '2011-09-05'),
(19, 1, 'CATAMARCA', '10', 367828, 1, '2011-09-05'),
(20, 1, 'BUENOS AIRES', '06', 15625084, 1, '2011-09-05'),
(21, 1, 'CIUDAD DE BUENOS AIRES', '02', 2890151, 1, '2011-09-05'),
(22, 1, 'SANTA CRUZ', '78', 273964, 1, '2011-09-05'),
(23, 1, 'CHUBUT', '26', 509108, 1, '2011-09-05'),
(24, 1, 'TIERRA DEL FUEGO', '94', 127205, 1, '2011-09-05');

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_provincias;

-- la recreamos
CREATE TABLE IF NOT EXISTS `auditoria_provincias` (
  `ID` smallint(4) UNSIGNED NOT NULL,
  `PAIS` smallint(3) UNSIGNED NOT NULL,
  `NOM_PROV` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `COD_PROV` varchar(4) COLLATE utf8_spanish_ci NOT NULL,
  `POBLACION` int(8) UNSIGNED NOT NULL,
  `USUARIO` int(4) UNSIGNED NOT NULL,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_EVENTO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EVENTO` enum("Edicion", "Eliminacion"),
  KEY `ID` (`ID`),
  KEY `NOM_PROV` (`NOM_PROV`),
  kEY `COD_PROV` (`COD_PROV`),
  KEY `USUARIO` (`USUARIO`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Provincias Georeferenciadas';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_provincias;

-- lo recreamos
CREATE TRIGGER edicion_provincias
AFTER UPDATE ON provincias
FOR EACH ROW
INSERT INTO auditoria_provincias
       (ID,
        PAIS,
        NOM_PROV,
        COD_PROV,
        POBLACION,
        USUARIO,
        FECHA_ALTA,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.PAIS,
        OLD.NOM_PROV,
        OLD.COD_PROV,
        OLD.POBLACION,
        OLD.USUARIO,
        OLD.FECHA_ALTA,
        'Edicion');

-- eliminamos el trigger de eliminación si existe
DROP TRIGGER IF EXISTS eliminacion_provincias;

-- lo recreamos
CREATE TRIGGER eliminacion_provincias
AFTER DELETE ON provincias
FOR EACH ROW
INSERT INTO auditoria_provincias
       (ID,
        PAIS,
        NOM_PROV,
        COD_PROV,
        POBLACION,
        USUARIO,
        FECHA_ALTA,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.PAIS,
        OLD.NOM_PROV,
        OLD.COD_PROV,
        OLD.POBLACION,
        OLD.USUARIO,
        OLD.FECHA_ALTA,
        'Eliminacion');

-- eliminamos la vista de provincias si existe
DROP VIEW IF EXISTS v_provincias;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_provincias AS
  SELECT diccionarios.provincias.id AS id,
         diccionarios.provincias.pais AS idpais,
         diccionarios.paises.nombre AS pais,
         diccionarios.provincias.nom_prov AS provincia,
         diccionarios.provincias.cod_prov AS cod_prov,
         diccionarios.provincias.poblacion AS poblacion,
         DATE_FORMAT(diccionarios.provincias.fecha_alta, '%d/%m/%Y') AS fecha_alta,
         diccionarios.provincias.usuario AS idusuario,
         cce.responsables.usuario AS usuario
  FROM diccionarios.provincias INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.paises.id
                               INNER JOIN cce.responsables ON diccionarios.provincias.usuario = cce.responsables.id;
