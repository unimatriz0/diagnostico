/*

    Nombre: diagnostico.sql
    Fecha: 20/03/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: definicion de la estructura de base de datos del sistema
                 de diagnostico

    Se crean tablas de auditoria y los triggers porque el control de
    auditoría se hace demasiado complejo por código e introduce
    errores

    Para la distribución es necesario analizar
    1. Distribuir toda la plataforma en una sola base de datos
    2. Utilizar una base de datos para Calidad, otra para Diagnóstico
       y una tercera para las tablas auxiliares (localidades, documentos, etc.)

    Crear las bases para control de stock

    El acceso de los usuarios es independiente entre el sistema de diagnóstico
    y el de control de calidad (ya que un laboratorio puede participar del
    CCE y tener un sistema propio de diagnóstico)

    Así vamos a tener:

    En la base de datos CCE
    1. La tabla de Laboratorios
    2. La tabla de Usuarios Autorizados de CCE
    3. La tabla de determinaciones
    4. La tabla de otras determinaciones
    5. La tabla de etiquetas
    6. La tabla de notas recibidas
    7. La tabla de notas enviadas a los responsables
    8. La tabla de operativos chagas
    9. La tabla de palets (pivot con la de muestras / etiquetas)
   10. La tabla de técnicas
   11. La tabla de valores de las técnicas
   12. La tabla de no conformidades
   13. Las tablas de auditoría correspondientes

    En la base de datos de diagnóstico
    1. La tabla de pacientes / personas
    2. La tabla de chagas congénito
    3. La tabla de resultados de las pruebas
    4. La tabla de toma de muestras
    5. La tabla de entrevistas
    6. La tabla de motivos de consulta
    7. La tabla de transfusiones recibidas
    8. La tabla de transplantes recibidos
    9. El diccionario de órganos de transplante
   10. La tabla de usuarios y sus permisos
   11. La tabla de técnicas (la independizamos de CCE)
   12. La tabla de valores de corte (independiente de CCE)
   13. La tabla de centros asistenciales
   14. La tabla de protocolos o certificados
   15. La tabla de marcas de reactivos (porque usamos una tabla de tecnicas distinta)
   16. Tabla con las fechas de entrega de resultados según fecha de toma

   Usa como diccionario la tabla de laboratorios del CCE

   En la base de datos de stock tenemos
   1. Los items del stock
   2. La tabla de ingresos
   3. La tabla de egresos
   4. La tabla de inventario
   5. La tabla de fuentes de financiamiento
   6. La tabla de solicitudes de stock

   Usa la tabla de usuarios autorizados de diagnóstico

   En la base de Diccionarios tendremos (comunes a las dos plataformas)
   1. La tabla de países
   2. La tabla de jurisdicciones
   3. La tabla de localidades
   4. El diccionario de tipos de documento
   5. El diccionario de dependencias

*/

-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- eliminamos la base si existe
DROP DATABASE IF EXISTS diagnostico;

-- la creamos
CREATE DATABASE IF NOT EXISTS diagnostico
DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;

-- seleccionamos
USE diagnostico;

/*************************************************************************/
/*                                                                       */
/*                        Tabla de Personas                              */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla de personas
 protocolo, entero 8 digitos autonumerico, numero de protocolo
 id_laboratorio, entero 6 digitos, laboratorio propietario del registro
 historia_clinica, varchar(20) numero identificador de la historia clinica
 apellido, varchar(50) apellido del paciente
 nombre, varchar(50) nombre del paciente
 documento, varchar(12) numero de documento del paciente (para poder adaptarlo
            a otros paises donde quiza usen letras)
 tipo_documento, entero pequeño (1) clave con el tipo de documento
 fecha_nacimiento, date fecha de nacimiento del paciente
 edad entero(2) edad del paciente puede ser calculado o ingresado manualmente
 sexo entero pequeño (1) clave con el sexo del paciente
 estado_civil entero pequeño (1) clave con el estado civil del paciente
 hijos entero pequeño(2) numero de hijos del paciente
 direccion varchar(100) direccion postal del paciente
 telefono varchar(20) numero de telefono
 celular varchar(30) número de celular
 compania tinyint(1) clave con la compañía de celular para enviar textos
 localidad_nacimiento entero(4) clave con la localidad de nacimiento
 coordenadas_nacimiento varchar(50) coordenadas gps
 localidad_residencia entero(4) clave con la localidad de residencia
 coordenadas_residencia varchar(50) coordenadas gps
 localidad_madre entero(4) clave con la localidad de origen de la madre
 madre_positiva enum (si no no sabe) indica si la madre era positiva
 ocupacion varchar(250) descripcion de la ocupacion
 obra_social varchar(100) nombre de la obra social
 motivo entero pequeño(1) clave con el motivo de consulta
 derivacion varchar(100) nombre del medico que indico el analisis
 tratamiento enum (si no) recibio tratamiento
 duracion si recibio tratamiento la duracion
 otras_enfermedades indica si tiene otras enfermedades
 terminado enum (si no) indica si el seguimiento fue terminado
 usuario entero(4) clave de la tabla de usuarios autorizados
 fecha_alta date fecha en que ingreso el registro
 comentarios: texto, comentarios y observaciones

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS personas;

-- creamos la tabla
CREATE TABLE personas (
    protocolo int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    historia_clinica varchar(20) DEFAULT NULL,
    apellido varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    documento varchar(12) NOT NULL,
    tipo_documento tinyint(1) UNSIGNED NOT NULL,
    fecha_nacimiento date DEFAULT NULL,
    edad int(2) UNSIGNED DEFAULT NULL,
    sexo tinyint(1) UNSIGNED NOT NULL,
    estado_civil tinyint(1) UNSIGNED NOT NULL,
    hijos tinyint(2) UNSIGNED DEFAULT NULL,
    direccion varchar(100) DEFAULT NULL,
    telefono varchar(20) DEFAULT NULL,
    celular varchar(30) DEFAULT NULL,
    compania int(1) DEFAULT NULL,
    mail varchar(50) DEFAULT NULL,
    localidad_nacimiento int(4) UNSIGNED NOT NULL,
    coordenadas_nacimiento varchar(50) DEFAULT NULL,
    localidad_residencia int(4) UNSIGNED NOT NULL,
    coordenadas_residencia varchar(50) DEFAULT NULL,
    localidad_madre int(4) UNSIGNED DEFAULT NULL,
    madre_positiva enum("Si", "No", "No Sabe") DEFAULT "No Sabe",
    ocupacion varchar(250) DEFAULT NULL,
    obra_social varchar(100) DEFAULT NULL,
    motivo tinyint(1) UNSIGNED NOT NULL,
    derivacion varchar(100) NOT NULL,
    tratamiento enum ("Si", "No", "No Sabe") NOT NULL,
    duracion tinyint(2) UNSIGNED DEFAULT NULL,
    terminado enum("Si", "No") DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY historia_clinica(historia_clinica),
    KEY apellido(apellido),
    KEY nombre(nombre),
    KEY nombre_completo(apellido, nombre),
    KEY documento(documento),
    KEY tipo_documento(tipo_documento),
    KEY compania(compania),
    KEY sexo(sexo),
    KEY estado_civil(estado_civil),
    KEY localidad_nacimiento(localidad_nacimiento),
    KEY localidad_residencia(localidad_residencia),
    KEY localidad_madre(localidad_madre),
    KEY motivo(motivo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los pacientes';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_personas;

-- creamos la tabla
CREATE TABLE auditoria_personas (
    protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    historia_clinica varchar(20) DEFAULT NULL,
    apellido varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    documento varchar(12) NOT NULL,
    tipo_documento tinyint(1) UNSIGNED NOT NULL,
    fecha_nacimiento date DEFAULT NULL,
    edad int(2) UNSIGNED DEFAULT NULL,
    sexo tinyint(1) UNSIGNED NOT NULL,
    estado_civil tinyint(1) UNSIGNED NOT NULL,
    hijos tinyint(2) UNSIGNED DEFAULT NULL,
    direccion varchar(100) DEFAULT NULL,
    telefono varchar(20) DEFAULT NULL,
    celular varchar(30) DEFAULT NULL,
    compania int(1) DEFAULT NULL,
    mail varchar(50) DEFAULT NULL,
    localidad_nacimiento int(4) UNSIGNED NOT NULL,
    coordenadas_nacimiento varchar(50) DEFAULT NULL,
    localidad_residencia int(4) UNSIGNED NOT NULL,
    coordenadas_residencia varchar(50) DEFAULT NULL,
    localidad_madre int(4) UNSIGNED DEFAULT NULL,
    madre_positiva enum("Si", "No", "No Sabe") DEFAULT "No Sabe",
    ocupacion varchar(250) DEFAULT NULL,
    obra_social varchar(100) DEFAULT NULL,
    motivo tinyint(1) UNSIGNED NOT NULL,
    derivacion varchar(100) NOT NULL,
    tratamiento enum ("Si", "No", "No Sabe") NOT NULL,
    duracion tinyint(2) UNSIGNED DEFAULT NULL,
    terminado enum("Si", "No") DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY protocolo(protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY historia_clinica(historia_clinica),
    KEY apellido(apellido),
    KEY nombre(nombre),
    KEY nombre_completo(apellido, nombre),
    KEY documento(documento),
    KEY compania(compania),
    KEY tipo_documento(tipo_documento),
    KEY sexo(sexo),
    KEY estado_civil(estado_civil),
    KEY localidad_nacimiento(localidad_nacimiento),
    KEY localidad_residencia(localidad_residencia),
    KEY localidad_madre(localidad_madre),
    KEY motivo(motivo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de los pacientes';

-- eliminamos el trigger de edición de personas
DROP TRIGGER IF EXISTS edicion_personas;

-- creamos el trigger
CREATE TRIGGER edicion_personas
AFTER UPDATE ON personas
FOR EACH ROW
INSERT INTO auditoria_personas
     (protocolo,
      id_laboratorio,
      historia_clinica,
      apellido,
      nombre,
      documento,
      tipo_documento,
      fecha_nacimiento,
      edad,
      sexo,
      estado_civil,
      hijos,
      direccion,
      telefono,
      celular,
      compania,
      mail,
      localidad_nacimiento,
      coordenadas_nacimiento,
      localidad_residencia,
      coordenadas_residencia,
      localidad_madre,
      madre_positiva,
      ocupacion,
      obra_social,
      motivo,
      derivacion,
      tratamiento,
      duracion,
      terminado,
      usuario,
      fecha_alta,
      comentarios,
      evento)
     VALUES
     (OLD.protocolo,
      OLD.id_laboratorio,
      OLD.historia_clinica,
      OLD.apellido,
      OLD.nombre,
      OLD.documento,
      OLD.tipo_documento,
      OLD.fecha_nacimiento,
      OLD.edad,
      OLD.sexo,
      OLD.estado_civil,
      OLD.hijos,
      OLD.direccion,
      OLD.telefono,
      OLD.celular,
      OLD.compania,
      OLD.mail,
      OLD.localidad_nacimiento,
      OLD.coordenadas_nacimiento,
      OLD.localidad_residencia,
      OLD.coordenadas_residencia,
      OLD.localidad_madre,
      OLD.madre_positiva,
      OLD.ocupacion,
      OLD.obra_social,
      OLD.motivo,
      OLD.derivacion,
      OLD.tratamiento,
      OLD.duracion,
      OLD.terminado,
      OLD.usuario,
      OLD.fecha_alta,
      OLD.comentarios,
      "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_personas;

-- creamos el trigger
CREATE TRIGGER eliminacion_personas
AFTER DELETE ON personas
FOR EACH ROW
INSERT INTO auditoria_personas
     (protocolo,
      id_laboratorio,
      historia_clinica,
      apellido,
      nombre,
      documento,
      tipo_documento,
      fecha_nacimiento,
      edad,
      sexo,
      estado_civil,
      hijos,
      direccion,
      telefono,
      celular,
      compania,
      mail,
      localidad_nacimiento,
      coordenadas_nacimiento,
      localidad_residencia,
      coordenadas_residencia,
      localidad_madre,
      madre_positiva,
      ocupacion,
      obra_social,
      motivo,
      derivacion,
      tratamiento,
      duracion,
      terminado,
      usuario,
      fecha_alta,
      comentarios,
      evento)
     VALUES
     (OLD.protocolo,
      OLD.id_laboratorio,
      OLD.historia_clinica,
      OLD.apellido,
      OLD.nombre,
      OLD.documento,
      OLD.tipo_documento,
      OLD.fecha_nacimiento,
      OLD.edad,
      OLD.sexo,
      OLD.estado_civil,
      OLD.hijos,
      OLD.direccion,
      OLD.telefono,
      OLD.celular,
      OLD.compania,
      OLD.mail,
      OLD.localidad_nacimiento,
      OLD.coordenadas_nacimiento,
      OLD.localidad_residencia,
      OLD.coordenadas_residencia,
      OLD.localidad_madre,
      OLD.madre_positiva,
      OLD.ocupacion,
      OLD.obra_social,
      OLD.motivo,
      OLD.derivacion,
      OLD.tratamiento,
      OLD.duracion,
      OLD.terminado,
      OLD.usuario,
      OLD.fecha_alta,
      OLD.comentarios,
      "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*              Fechas de entrega de resultados                          */
/*                                                                       */
/*************************************************************************/

/*

    Estructura de la tabla
    id int(4) clave del registro (a una entrada por día tenemos tabla
              para 25 años)
    recepcion date fecha de recepción de la muestra
    entrega date fecha de entrega de resultados
    laboratorio int(6) clave del laboratorio (cada laboratorio puede
                tener una fecha distinta de entrega)
    usuario int(4) usuario que ingresó el registro

    En esta tabla no usamos registro de transacciones

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS entregas;

-- la recreamos
CREATE TABLE IF NOT EXISTS entregas (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    recepcion date NOT NULL,
    entrega date NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY laboratorio(laboratorio),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Fechas de entregas de resultados';


/*************************************************************************/
/*                                                                       */
/*                Diccionario de Enfermedades                            */
/*                                                                       */
/*************************************************************************/

/*

  Estructura de la tabla con el diccionario de enfermedades
  id entero(3) autonumérico, clave del registro
  enfermedad varchar(100) descripción de la enfermedad
  id_usuario entero(4) clave del usuario
  fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS enfermedades;

-- la recreamos
CREATE TABLE enfermedades (
    id int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    enfermedad varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY enfermedad(enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de enfermedades';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_enfermedades;

-- la recreamos
CREATE TABLE auditoria_enfermedades (
    id int(3) UNSIGNED NOT NULL,
    enfermedad varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta DATE NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY enfermedad(enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de enfermedades';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_enfermedades;

-- lo recreamos
CREATE TRIGGER edicion_enfermedades
AFTER UPDATE ON enfermedades
FOR EACH ROW
INSERT INTO auditoria_enfermedades
    (id,
     enfermedad,
     id_usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.enfermedad,
     OLD.id_usuario,
     OLD.fecha_alta,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_enfermedades;

-- lo recreamos
CREATE TRIGGER eliminacion_enfermedades
AFTER DELETE ON enfermedades
FOR EACH ROW
INSERT INTO auditoria_enfermedades
    (id,
     enfermedad,
     id_usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.enfermedad,
     OLD.id_usuario,
     OLD.fecha_alta,
     "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Otras Enfermedades                            */
/*                                                                       */
/*************************************************************************/

/*
   Estructura de la tabla de otras enfermedades, esta tabla es un pivot
   entre el diccionario de enfermedades y el de pacientes, agrega un
   registro por cada enfermedad sufrida por el paciente
   id int(9) clave del registro
   id_protocoloi int(8) clave del paciente
   id_enfermedad int(3) clave de la enfermedad
   id_usuario int(4) clave del usuario
   fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS otras_enfermedades;

-- la recreamos
CREATE TABLE otras_enfermedades(
    id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_enfermedad int(3) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_enfermedad(id_enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Otras Enfermedades de los pacientes';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_otras_enfermedades;

-- la recreamos
CREATE TABLE auditoria_otras_enfermedades(
    id int(9) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_enfermedad int(3) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_enfermedad(id_enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de otras enfermedades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_otras_enfermedades;

-- lo recreamos
CREATE TRIGGER edicion_otras_enfermedades
AFTER UPDATE ON otras_enfermedades
FOR EACH ROW
INSERT INTO auditoria_otras_enfermedades
       (id,
        id_protocolo,
        id_enfermedad,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_protocolo,
        OLD.id_enfermedad,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_otras_enfermedades;

-- lo recreamos
CREATE TRIGGER eliminacion_otras_enfermedades
AFTER DELETE ON otras_enfermedades
FOR EACH ROW
INSERT INTO auditoria_otras_enfermedades
       (id,
        id_protocolo,
        id_enfermedad,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_protocolo,
        OLD.id_enfermedad,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Chagas Congenito                              */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla congenito, esta tabla al ser menor, agrega como
 registro auxiliar los datos de chagas congenito
 id entero(8) autonumerico clave unica del registro
 id_protocolo entero(8) clave del paciente
 id_madre entero(8) clave del protocolo de la madre
 id_sivila varchar(20) clave del sivila
 reportado date fecha en que fue informado al sivila
 parto enum (normal / cesarea / otro) tipo de parto
 peso entero(4) peso al nacer en gramos
 prematuro enum (si / no) indica si el bebe fue prematuro
 institucion entero(8) clave de la institucion
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha_alta date fecha de alta del registro
 comentarios texto, comentarios especificos

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS congenito;

-- creamos la tabla
CREATE TABLE congenito (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_madre int(8) UNSIGNED NOT NULL,
    id_sivila varchar(20) DEFAULT NULL,
    reportado date DEFAULT NULL,
    parto enum("Normal", "Cesárea", "No Sabe") NOT NULL,
    peso int(4) UNSIGNED NOT NULL,
    prematuro enum("Si", "No", "No Sabe"),
    institucion int(8) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_madre(id_madre),
    KEY id_sivila(id_sivila),
    KEY institucion(institucion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de chagas congenito';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_congenito;

-- la recreamos
CREATE TABLE auditoria_congenito (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_madre int(8) UNSIGNED NOT NULL,
    id_sivila varchar(20) DEFAULT NULL,
    reportado date DEFAULT NULL,
    parto enum("Normal", "Cesárea", "No Sabe") NOT NULL,
    peso int(4) UNSIGNED NOT NULL,
    prematuro enum("Si", "No", "No Sabe"),
    institucion int(8) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_madre(id_madre),
    KEY id_sivila(id_sivila),
    KEY institucion(institucion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de chagas congenito';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_congenito;

-- lo recreamos
CREATE TRIGGER edicion_congenito
AFTER UPDATE ON congenito
FOR EACH ROW
INSERT INTO auditoria_congenito
    (id,
     id_protocolo,
     id_madre,
     id_sivila,
     reportado,
     parto,
     peso,
     prematuro,
     institucion,
     id_usuario,
     fecha_alta,
     comentarios,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.id_madre,
     OLD.id_sivila,
     OLD.reportado,
     OLD.parto,
     OLD.peso,
     OLD.prematuro,
     OLD.institucion,
     OLD.id_usuario,
     OLD.fecha_alta,
     OLD.comentarios,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_congenito;

-- lo recreamos
CREATE TRIGGER eliminacion_congenito
AFTER DELETE ON congenito
FOR EACH ROW
INSERT INTO auditoria_congenito
    (id,
     id_protocolo,
     id_madre,
     id_sivila,
     reportado,
     parto,
     peso,
     prematuro,
     institucion,
     id_usuario,
     fecha_alta,
     comentarios,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.id_madre,
     OLD.id_sivila,
     OLD.reportado,
     OLD.parto,
     OLD.peso,
     OLD.prematuro,
     OLD.institucion,
     OLD.id_usuario,
     OLD.fecha_alta,
     OLD.comentarios,
     "Eliminacion");

/****************************************************************************/
/*                                                                          */
/*                      Resultados de las Pruebas                           */
/*                                                                          */
/****************************************************************************/

/*

 Estructura de la tabla de resultados
 id_resultado entero(10) autonumerico, clave del registro
 id_muestra entero(8) clave de la muestra
 resultado enum (Reactivo / No Reactivo / Indeterminado) resultado obtenido
 valor_corte varchar(10) valor de corte utilizado
 valor_lectura varchar(10) valor de lectura obtenido
 id_tecnica entero pequeño (3) clave de la tecnica utilizada
 fecha_determinacion fecha en que se realizo la determinacion
 comentarios texto comentarios y observaciones
 id_usuario entero(4) usuario que realizo la determinacion
 id_verifico entero(4) usuario que verificó el resultado
 id_firmo entero(4) usuario que firmó o autorizó el protocolo
 fecha date fecha de alta del registro

*/

-- elimina la tabla
DROP TABLE IF EXISTS resultados;

-- creamos la tabla
CREATE TABLE resultados (
    id_resultado int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_muestra int(8) UNSIGNED NOT NULL,
    resultado enum("Reactivo", "No Reactivo", "Indeterminado") NOT NULL,
    valor_corte varchar(10) NOT NULL,
    valor_lectura varchar(10) NOT NULL,
    id_tecnica tinyint(3) UNSIGNED NOT NULL,
    fecha_determinacion date NOT NULL,
    comentarios text DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    id_verifico int(4) UNSIGNED DEFAULT NULL,
    id_firmo int(4) UNSIGNED DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_resultado),
    KEY id_muestra(id_muestra),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario),
    KEY id_verifico(id_verifico),
    KEY id_firmo(id_firmo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Resultados de las pruebas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_resultados;

-- creamos la tabla
CREATE TABLE auditoria_resultados (
    id_resultado int(8) UNSIGNED NOT NULL,
    id_muestra int(8) UNSIGNED NOT NULL,
    resultado enum("Reactivo", "No Reactivo", "Indeterminado") NOT NULL,
    valor_corte varchar(10) NOT NULL,
    valor_lectura varchar(10) NOT NULL,
    id_tecnica tinyint(3) UNSIGNED NOT NULL,
    fecha_determinacion date NOT NULL,
    comentarios text DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    id_verifico int(4) UNSIGNED DEFAULT NULL,
    id_firmo int(4) UNSIGNED DEFAULT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id_resultado(id_resultado),
    KEY id_muestra(id_muestra),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario),
    KEY id_verifico(id_verifico),
    KEY id_firmo(id_firmo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los resultados';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_resultados;

-- lo recreamos
CREATE TRIGGER edicion_resultados
AFTER UPDATE ON resultados
FOR EACH ROW
INSERT INTO auditoria_resultados
    (id_resultado,
     id_muestra,
     resultado,
     valor_corte,
     valor_lectura,
     id_tecnica,
     fecha_determinacion,
     comentarios,
     id_usuario,
     id_verifico,
     id_firmo,
     fecha,
     evento)
    VALUES
    (OLD.id_resultado,
     OLD.id_muestra,
     OLD.resultado,
     OLD.valor_corte,
     OLD.valor_lectura,
     OLD.id_tecnica,
     OLD.fecha_determinacion,
     OLD.comentarios,
     OLD.id_usuario,
     OLD.id_verifico,
     OLD.id_firmo,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_resultados;

-- lo recreamos
CREATE TRIGGER eliminacion_resultados
AFTER DELETE ON resultados
FOR EACH ROW
INSERT INTO auditoria_resultados
    (id_resultado,
     id_muestra,
     resultado,
     valor_corte,
     valor_lectura,
     id_tecnica,
     fecha_determinacion,
     comentarios,
     id_usuario,
     id_verifico,
     id_firmo,
     fecha,
     evento)
    VALUES
    (OLD.id_resultado,
     OLD.id_muestra,
     OLD.resultado,
     OLD.valor_corte,
     OLD.valor_lectura,
     OLD.id_tecnica,
     OLD.fecha_determinacion,
     OLD.comentarios,
     OLD.id_usuario,
     OLD.id_verifico,
     OLD.id_firmo,
     OLD.fecha,
     "Eliminacion");

/*************************************************************************/
/*                                                                       */
/*                       Toma de Muestras                                */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla de muestras
 id_muestra entero(8) autonumerico
 id_protocolo entero(8) clave del paciente o protocolo
 id_laboratorio entero(6) laboratorio donde se tomó la muestra
 comentarios texto comentarios y observaciones
 id_genero entero(4) clave del usuario que generó el registro
 fecha_alta date fecha en que fue tomada la muestra
 id_tomo entero(4) clave del usuario que tomó la muestra
 fecha_toma date fecha en que fue tomada la muestra

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS muestras;

-- la recreamos desde cero
CREATE TABLE muestras (
    id_muestra int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    comentarios TEXT,
    id_genero int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_tomo int(4) UNSIGNED DEFAULT NULL,
    fecha_toma date DEFAULT NULL,
    PRIMARY KEY(id_muestra),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_genero(id_genero),
    KEY id_tomo(id_tomo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Muestras de los pacientes';

-- eliminamos la auditoria de las muestras
DROP TABLE IF EXISTS auditoria_muestras;

-- la recreamos desde cero
CREATE TABLE auditoria_muestras (
    id_muestra int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    comentarios TEXT,
    id_genero int(4) UNSIGNED NOT NULL,
    fecha_alta date,
    id_tomo int(4) DEFAULT NULL,
    fecha_toma date,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id_muestra(id_muestra),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_genero(id_genero),
    KEY id_tomo(id_tomo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las muestras';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_muestras;

-- lo recreamos
CREATE TRIGGER edicion_muestras
AFTER UPDATE ON muestras
FOR EACH ROW
INSERT INTO auditoria_muestras
    (id_muestra,
     id_protocolo,
     id_laboratorio,
     comentarios,
     id_genero,
     fecha_alta,
     id_tomo,
     fecha_toma,
     evento)
    VALUES
    (OLD.id_muestra,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.comentarios,
     OLD.id_genero,
     OLD.fecha_alta,
     OLD.id_tomo,
     OLD.fecha_toma,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_muestras;

-- lo recreamos
CREATE TRIGGER eliminacion_muestras
AFTER DELETE ON muestras
FOR EACH ROW
INSERT INTO auditoria_muestras
    (id_muestra,
     id_protocolo,
     id_laboratorio,
     comentarios,
     id_genero,
     fecha_alta,
     id_tomo,
     fecha_toma,
     evento)
    VALUES
    (OLD.id_muestra,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.comentarios,
     OLD.id_genero,
     OLD.fecha_alta,
     OLD.id_tomo,
     OLD.fecha_toma,
     "Eliminacion");


/************************************************************************/
/*                                                                      */
/*                  Entrevistas a Pacientes                             */
/*                                                                      */
/************************************************************************/

/*

 Estructura de la tabla de entrevistas
 id_entrevista entero(6) autonumerico, clave del registro
 id_protocolo entero(8) clave del paciente
 id_laboratorio int(6) laboratorio donde se realizó la entrevista
 resultado enum(positivo / negativo / otro) resultado de la entrevista
 actitud enum (positiva / negativa / otra) actitud del paciente
 concurrio enum (si / no) indica si concurrio a la entrevista
 tipo (presencial / telefonica) modalidad de la entrevista
 fecha date fecha de la entrevista
 comentarios text comentarios y observaciones del entrevistador
 id_usuario entero(4) clave del usuario que entrevisto

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS entrevistas;

-- la recreamos
CREATE TABLE entrevistas (
    id_entrevista int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    resultado enum("Positivo", "Negativo", "Otro") NOT NULL,
    actitud enum("Positiva", "Negativa", "Indiferente") NOT NULL,
    concurrio enum("Si", "No") NOT NULL,
    tipo enum("Presencial", "Telefonica") NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id_entrevista),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Entrevistas a pacientes';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_entrevistas;

-- la recreamos
CREATE TABLE auditoria_entrevistas (
    id_entrevista int(6) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    resultado enum("Positivo", "Negativo", "Otro") NOT NULL,
    actitud enum("Positiva", "Negativa", "Indiferente") NOT NULL,
    concurrio enum("Si", "No") NOT NULL,
    tipo enum("Presencial", "Telefonica") NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text,
    id_usuario int(4) UNSIGNED NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id_entrevista(id_entrevista),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de entrevistas a pacientes';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_entrevistas;

-- lo recreamos
CREATE TRIGGER edicion_entrevistas
AFTER UPDATE ON entrevistas
FOR EACH ROW
INSERT INTO auditoria_entrevistas
    (id_entrevista,
     id_protocolo,
     id_laboratorio,
     resultado,
     actitud,
     concurrio,
     tipo,
     comentarios,
     id_usuario,
     evento)
    VALUES
    (OLD.id_entrevista,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.resultado,
     OLD.actitud,
     OLD.concurrio,
     OLD.tipo,
     OLD.comentarios,
     OLD.id_usuario,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_entrevistas;

-- lo recreamos
CREATE TRIGGER eliminacion_entrevistas
AFTER DELETE ON entrevistas
FOR EACH ROW
INSERT INTO auditoria_entrevistas
    (id_entrevista,
     id_protocolo,
     id_laboratorio,
     resultado,
     actitud,
     concurrio,
     tipo,
     comentarios,
     id_usuario,
     evento)
    VALUES
    (OLD.id_entrevista,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.resultado,
     OLD.actitud,
     OLD.concurrio,
     OLD.tipo,
     OLD.comentarios,
     OLD.id_usuario,
     "Eliminacion");

/************************************************************************/
/*                                                                      */
/*                     Motivos de Consulta                              */
/*                                                                      */
/************************************************************************/

/*

 Estructura de la tabla de motivos de consulta
 id_motivo entero pequeño (2) autonumerico, clave del registro
 motivo varchar(50) descripcion del motivo de la consulta
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS motivos;

-- la recreamos
CREATE TABLE motivos (
    id_motivo tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    motivo varchar(50) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_motivo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Motivos de consulta';

-- insertamos los valores iniciales
INSERT INTO motivos (motivo, id_usuario) VALUES ("Derivacion", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Seguimiento", 1);


/***************************************************************************/
/*                                                                         */
/*                       Transfusiones Recibidas                           */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de transfusiones
 id entero(8) autonumerico, clave del registro
 id_protocolo entero(8) clave del protocolo
 fecha_transfusion date fecha de la transfusion
 motivo varchar(200) descripcion del motivo
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS transfusiones;

-- la recreamos
CREATE TABLE transfusiones (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    fecha_transfusion date NOT NULL,
    motivo varchar(200) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Transfusiones recibidas';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_transfusiones;

-- la recreamos
CREATE TABLE auditoria_transfusiones (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    fecha_transfusion date NOT NULL,
    motivo varchar(200) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las Transfusiones';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_transfusiones;

-- lo recreamos
CREATE TRIGGER edicion_transfusiones
AFTER UPDATE ON transfusiones
FOR EACH ROW
INSERT INTO auditoria_transfusiones
    (id,
     id_protocolo,
     fecha_transfusion,
     motivo,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.fecha_transfusion,
     OLD.motivo,
     OLD.id_usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_transfusiones;

-- lo recreamos
CREATE TRIGGER eliminacion_transfusiones
AFTER DELETE ON transfusiones
FOR EACH ROW
INSERT INTO auditoria_transfusiones
    (id,
     id_protocolo,
     fecha_transfusion,
     motivo,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.fecha_transfusion,
     OLD.motivo,
     OLD.id_usuario,
     OLD.fecha,
     "Eliminacion");

/**************************************************************************/
/*                                                                        */
/*                            Transplantes Recibidos                      */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de transplantes
 id entero(8) autonumerico, clave del registro
 id_protocolo entero(8) clave del protocolo
 organo entero pequeño (2) clave del organo recibido
 positivo enum (si / no / no sabe) indica si el organo recibido era
          positivo para chagas
 fecha_transplante date fecha del transplante
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS transplantes;

-- la creamos
CREATE TABLE transplantes (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    organo tinyint(2) UNSIGNED NOT NULL,
    positivo enum("Si", "No", "No sabe") NOT NULL,
    fecha_transplante date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY organo(organo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Transplantes recibidos';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_transplantes;

-- la creamos
CREATE TABLE auditoria_transplantes (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    organo tinyint(2) UNSIGNED NOT NULL,
    positivo enum("Si", "No", "No Sabe") NOT NULL,
    fecha_transplante date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY organo(organo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Transplantes';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_transplantes;

-- lo recreamos
CREATE TRIGGER edicion_transplantes
AFTER UPDATE ON transplantes
FOR EACH ROW
INSERT INTO auditoria_transplantes
    (id,
     id_protocolo,
     organo,
     positivo,
     fecha_transplante,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.organo,
     OLD.positivo,
     OLD.fecha_transplante,
     OLD.id_usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_transplantes;

-- lo recreamos
CREATE TRIGGER eliminacion_transplantes
AFTER DELETE ON transplantes
FOR EACH ROW
INSERT INTO auditoria_transplantes
    (id,
     id_protocolo,
     organo,
     positivo,
     fecha_transplante,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.organo,
     OLD.positivo,
     OLD.fecha_transplante,
     OLD.id_usuario,
     OLD.fecha,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                       Diccionario de Organos                           */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de organos
 id entero pequeño (2) clave del registro
 organo varchar(100) nombre del organo
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS organos;

-- la creamos
CREATE TABLE organos (
    id tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    organo varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de Organos';

-- cargamos los registros iniciales
INSERT INTO organos (organo, id_usuario) VALUES ("Corazon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Higado", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Riñon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Pulmon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Medula", 1);


/**************************************************************************/
/*                                                                        */
/*                         Usuarios Autorizados                           */
/*                                                                        */
/**************************************************************************/

/*
 Para evitar complicaciones, vamos a usar a tabla de usuarios de control
 de calidad y relacionarlo con la tabla de permisos

 Estructura de la tabla de usuarios

 id, entero(4) clave de la tabla de usuarios sincronizada con la tabla
     de usuarios del sistema cce
 autorizo, clave del usuario que autorizo
 laboratorio, entero clave del laboratorio al que pertenece (es distinto
              de la clave laboratorio del sistema cce)
 administrador enum (es o no administrador)
 personas enum(si o no puede editar personas)
 congenito enum(si / no) si puede editar chagas congenito
 resultados enum(si / no) si puede cargar resultados
 muestras enum(si / no) si puede cargar muestras
 entrevistas enum(si / no) si puede cargar entrevistas
 auxiliares enum(si / no) si puede editar tablas auxiliares
 protocolos enum(si / no) si puede firmar protocolos
 stock enum (si / no) si puede editar el stock
 usuarios enum (si / no) si puede editar usuarios del sistema
 firma blob contenido de la firma (si puede firmar protocolos)

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS usuarios;

-- la recreamos desde cero
CREATE TABLE usuarios(
    id int(4) UNSIGNED NOT NULL,
    autorizo int(4) UNSIGNED NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    administrador enum("Si", "No") DEFAULT "No",
    personas enum("Si", "No") DEFAULT "No",
    congenito enum("Si", "No") DEFAULT "No",
    resultados enum("Si", "No") DEFAULT "No",
    muestras enum("Si", "No") DEFAULT "No",
    entrevistas enum("Si", "No") DEFAULT "No",
    auxiliares enum("Si", "No") DEFAULT "No",
    protocolos enum("Si", "No") DEFAULT "No",
    stock enum("Si", "No") DEFAULT "No",
    usuarios enum("Si", "No") DEFAULT "No",
    firma blob DEFAULT NULL,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY autorizo(autorizo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Usuarios autorizados del sistema';

-- insertamos el primer registro en la base
INSERT INTO usuarios
    (id,
     autorizo,
     laboratorio,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios)
    VALUES
    ('1',
     '1',
     '397',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si');

-- elimina la tabla de auditoria de usuarios si existe
DROP TABLE IF EXISTS auditoria_usuarios;

-- creacion de la tabla de auditoria de usuarios
CREATE TABLE auditoria_usuarios(
    id int(4) UNSIGNED NOT NULL,
    autorizo int(4) UNSIGNED NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    administrador enum("Si", "No") DEFAULT "No",
    personas enum("Si", "No") DEFAULT "No",
    congenito enum("Si", "No") DEFAULT "No",
    resultados enum("Si", "No") DEFAULT "No",
    muestras enum("Si", "No") DEFAULT "No",
    entrevistas enum("Si", "No") DEFAULT "No",
    auxiliares enum("Si", "No") DEFAULT "No",
    protocolos enum("Si", "No") DEFAULT "No",
    stock enum("Si", "No") DEFAULT "No",
    usuarios enum("Si", "No") DEFAULT "No",
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY autorizo(autorizo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de usuarios del sistema';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_usuarios;

-- lo recreamos
CREATE TRIGGER edicion_usuarios
AFTER UPDATE ON usuarios
FOR EACH ROW
INSERT INTO auditoria_usuarios
    (id,
     autorizo,
     laboratorio,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     evento)
    VALUES
    (OLD.id,
     OLD.autorizo,
     OLD.laboratorio,
     OLD.administrador,
     OLD.personas,
     OLD.congenito,
     OLD.resultados,
     OLD.muestras,
     OLD.entrevistas,
     OLD.auxiliares,
     OLD.protocolos,
     OLD.stock,
     OLD.usuarios,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_usuarios;

-- lo recreamos
CREATE TRIGGER eliminacion_usuarios
AFTER DELETE ON usuarios
FOR EACH ROW
INSERT INTO auditoria_usuarios
    (id,
     autorizo,
     laboratorio,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     evento)
    VALUES
    (OLD.id,
     OLD.autorizo,
     OLD.laboratorio,
     OLD.administrador,
     OLD.personas,
     OLD.congenito,
     OLD.resultados,
     OLD.muestras,
     OLD.entrevistas,
     OLD.auxiliares,
     OLD.protocolos,
     OLD.stock,
     OLD.usuarios,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                      Pruebas Reconocidas                               */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de pruebas o tecnicas
 id entero pequeño(3) clave del registro
 tecnica varchar(30) nombre genérico de la técnica
 nombre varchar(30) nombre completo de la técnica
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tecnicas;

-- la recreamos
CREATE TABLE tecnicas (
    id int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    tecnica VARCHAR(30) DEFAULT NULL,
    nombre VARCHAR(60) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY tecnica(tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de técnicas';

-- cargamos los valores iniciales
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (1, 'AP', 'Aglutinacion de Particulas', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (2, 'ELISA', 'Ensayo por Inmunoadsorción Ligado a Enzimas', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (3, 'HAI', 'Hemaglutinacion Indirecta', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (4, 'IFI', 'Inmunofluorescencia', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (5, 'Quimioluminiscencia', 'Quimioluminiscencia', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (6, 'OTRA', 'Otra', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (7, 'PCR', 'Reaccion en Cadena de la Polimerasa', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (8, 'Micrometodo', 'Micrometodo', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (9, 'XENO', 'Xenodiagnostico', 1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tecnicas;

-- la recreamos
CREATE TABLE auditoria_tecnicas (
    id int(3) UNSIGNED NOT NULL,
    tecnica VARCHAR(30) DEFAULT NULL,
    nombre VARCHAR(60) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY tecnica(tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de técnicas';

-- eliminamos el trigger de edicion
DROP TRIGGER IF EXISTS edicion_tecnicas;

-- lo recreamos
CREATE TRIGGER edicion_tecnicas
AFTER UPDATE ON tecnicas
FOR EACH ROW
INSERT INTO auditoria_tecnicas
       (id,
        tecnica,
        nombre,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.tecnica,
        OLD.nombre,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_tecnicas;

-- lo recreamos
CREATE TRIGGER eliminacion_tecnicas
AFTER DELETE ON tecnicas
FOR EACH ROW
INSERT INTO auditoria_tecnicas
       (id,
        tecnica,
        nombre,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.tecnica,
        OLD.nombre,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                         Marcas de Reactivos                              */
/*                                                                          */
/****************************************************************************/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `marcas`;

-- recreamos desde cero
CREATE TABLE IF NOT EXISTS `marcas` (
  `ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de las marcas utilizadas';

-- agregamos los registros iniciales
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (1,'Serodia','1','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (3,'Biomerieux','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (4,'Lemos','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (5,'Bioschile','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (6,'Biozima','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (7,'Lisado de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (8,'Polychaco','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (9,'Fatalakit','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (10,'Hemave','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (11,'Biocientifica','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (12,'Inmunofluor','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (13,'Ag Fatala','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (14,'Otra','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (15,'Otra','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (19,'Otra','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (21,'Otra','1','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (24,'Recombinante 3.0 de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (25,'Recombinante 4.0 de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (26,'Wiener','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (27,'ABBOT Architect','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (29,'Otra','6','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (30,'Lemos Recombinante','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (31,'ABBOT Architect','5','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (32,'Otra','5','1');

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_marcas;

-- creamos la tabla de auditoría marcas
CREATE TABLE IF NOT EXISTS `auditoria_marcas` (
  `ID` tinyint(2) UNSIGNED NOT NULL,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_EVENTO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `EVENTO` enum("Edicion", "Eliminacion"),
  KEY `ID`(`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las marcas utilizadas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_marcas;

-- lo recreamos
CREATE TRIGGER edicion_marcas
AFTER UPDATE ON marcas
FOR EACH ROW
INSERT INTO auditoria_marcas
       (ID,
        MARCA,
        TECNICA,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.MARCA,
        OLD.TECNICA,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Edicion");

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS eliminacion_marcas;

-- lo recreamos
CREATE TRIGGER eliminacion_marcas
AFTER DELETE ON marcas
FOR EACH ROW
INSERT INTO auditoria_marcas
       (ID,
        MARCA,
        TECNICA,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.MARCA,
        OLD.TECNICA,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                         Valores de Corte                                 */
/*                                                                          */
/****************************************************************************/

/*

 Estructura de la tabla de valores de corte de cada prueba, si una técnica
 tiene varios valores el sistema debe presentar un desplegable, si tiene
 solo un valor entiende que se trata de una máscara de entrada
 id entero(4) autonumerico, clave unica del registro
 id_tecnica entero pequeño(3) clave de la tecnica
 valor varchar(10) uno de los valores aceptados o la mascara de entrada
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS valores_tecnicas;

-- la recreamos
CREATE TABLE valores_tecnicas (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_tecnica smallint(3) UNSIGNED NOT NULL,
    valor varchar(10) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de valores de las técnicas';

-- insertamos los valores iniciales
INSERT INTO valores_tecnicas
        (id_tecnica, valor, id_usuario) VALUES
        (1,"1/4",1),
        (1,"1/8",1),
        (1,"1/16",1),
        (1,"1/32",1),
        (1,"1/64",1),
        (1,"1/128",1),
        (1,"1/256",1),
        (1,"1/512",1),
        (1,"1/1024",1),
        (1,"1/2048",1),
        (1,"1/4096",1),
        (3,"1/4",1),
        (3,"1/8",1),
        (3,"1/16",1),
        (3,"1/32",1),
        (3,"1/64",1),
        (3,"1/128",1),
        (3,"1/256",1),
        (3,"1/512",1),
        (3,"1/1024",1),
        (3,"1/2048",1),
        (3,"1/4096",1),
        (4,"1/4",1),
        (4,"1/8",1),
        (4,"1/16",1),
        (4,"1/32",1),
        (4,"1/64",1),
        (4,"1/128",1),
        (4,"1/256",1),
        (4,"1/512",1),
        (4,"1/1024",1),
        (4,"1/2048",1),
        (4,"1/4096",1),
        (2,"9.999",1),
        (5,"99.999",1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_valores;

-- la recreamos
CREATE TABLE auditoria_valores (
    id int(4) UNSIGNED NOT NULL,
    id_tecnica smallint(3) UNSIGNED NOT NULL,
    valor varchar(10) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de valores de las técnicas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_valores;

-- lo recreamos
CREATE TRIGGER edicion_valores
AFTER UPDATE ON valores_tecnicas
FOR EACH ROW
INSERT INTO auditoria_valores
       (id,
        id_tecnica,
        valor,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.id_tecnica,
        OLD.valor,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_valores;

-- lo recreamos
CREATE TRIGGER eliminacion_valores
AFTER DELETE ON valores_tecnicas
FOR EACH ROW
INSERT INTO auditoria_valores
       (id,
        id_tecnica,
        valor,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.id_tecnica,
        OLD.valor,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                  Instituciones Asistenciales                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de instituciones asistenciales
 id_institucion entero(6) autonumerico, clave del registro
 siisa varchar(20) clave siisa de la institución
 nombre varchar(250) nombre del centro asistencial
 id_localidad varchar(9) clave de la localidad del centro (a traves de la
              localidad obtenemos la jurisdiccion y el pais)
 direccion varchar(250) direccion postal de la institucion
 codigo_postal varchar(20) codigo postal de la direccion
 telefono varchar(20) telefono de la institucion
 mail varchar(50) direccion de correo de la institucion
 responsable varchar(150) nombre del responsable o director
 dependencia entero pequeño (1) clave de la dependencia
 coordenadas varchar(100) coordenadas gps
 comentarios texto comentarios del usuario
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS centros_asistenciales;

-- creamos la tabla
CREATE TABLE `centros_asistenciales` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `siisa` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `localidad` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_postal` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `responsable` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(4) unsigned NOT NULL,
  `dependencia` tinyint(1) unsigned NOT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentarios` text COLLATE utf8_spanish_ci,
  `coordenadas` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siisa` (`siisa`),
  KEY `responsable` (`responsable`),
  KEY `id_usuario` (`id_usuario`),
  KEY `dependencia` (`dependencia`),
  KEY `nombre` (`nombre`),
  KEY `localidad` (`localidad`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Centros Asistenciales';

-- eliminamos la tabla
DROP TABLE IF EXISTS auditoria_centros_asistenciales;

-- creamos la tabla
CREATE TABLE `auditoria_centros_asistenciales` (
  `id` int(6) unsigned NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `siisa` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `localidad` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_postal` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `responsable` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(4) unsigned NOT NULL,
  `dependencia` tinyint(1) unsigned NOT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_evento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentarios` text COLLATE utf8_spanish_ci,
  `coordenadas` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `evento` enum('Edicion', 'Eliminacion') NOT NULL,
  KEY `id`(`id`),
  KEY `siisa` (`siisa`),
  KEY `responsable` (`responsable`),
  KEY `id_usuario` (`id_usuario`),
  KEY `dependencia` (`dependencia`),
  KEY `nombre` (`nombre`),
  KEY `localidad` (`localidad`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Centros Asistenciales';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_instituciones;

-- lo recreamos
CREATE TRIGGER edicion_centros_asistenciales
AFTER UPDATE ON centros_asistenciales
FOR EACH ROW
INSERT INTO auditoria_centros_asistenciales
    (id,
     nombre,
     siisa,
     localidad,
     direccion,
     codigo_postal,
     telefono,
     mail,
     responsable,
     id_usuario,
     dependencia,
     fecha_alta,
     comentarios,
     coordenadas,
     evento)
    VALUES
    (OLD.id,
     OLD.nombre,
     OLD.siisa,
     OLD.localidad,
     OLD.direccion,
     OLD.codigo_postal,
     OLD.telefono,
     OLD.mail,
     OLD.responsable,
     OLD.id_usuario,
     OLD.dependencia,
     OLD.fecha_alta,
     OLD.comentarios,
     OLD.coordenadas,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_instituciones;

-- lo recreamos
CREATE TRIGGER eliminacion_centros_asistenciales
AFTER DELETE ON centros_asistenciales
FOR EACH ROW
INSERT INTO auditoria_centros_asistenciales
    (id,
     nombre,
     siisa,
     localidad,
     direccion,
     codigo_postal,
     telefono,
     mail,
     responsable,
     id_usuario,
     dependencia,
     fecha_alta,
     comentarios,
     coordenadas,
     evento)
    VALUES
    (OLD.id,
     OLD.nombre,
     OLD.siisa,
     OLD.localidad,
     OLD.direccion,
     OLD.codigo_postal,
     OLD.telefono,
     OLD.mail,
     OLD.responsable,
     OLD.id_usuario,
     OLD.dependencia,
     OLD.fecha_alta,
     OLD.comentarios,
     OLD.coordenadas,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                    Protocolos o Certificados                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de protocolos
 id_informe entero(8) autonumérico clave del registro
 id_protocolo entero(8) clave del registro de pacientes
 informe blob contenido del resultado en pdf
 id_usuario entero(4) usuario que imprimió el registro
 fecha date fecha de impresión o entrega

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS protocolos;

-- la creamos
CREATE TABLE protocolos (
    id_informe int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    informe blob,
    id_usuario int(8) UNSIGNED NOT NULL,
    fecha_impresion date DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_informe),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Protocolos de pacientes';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_protocolos;

-- la creamos
CREATE TABLE auditoria_protocolos (
    id_informe int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    informe blob,
    id_usuario int(8) UNSIGNED NOT NULL,
    fecha_impresion date DEFAULT NULL,
    fecha date NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id_informe(id_informe),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Protocolos de pacientes';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_protocolos;

-- lo recreamos
CREATE TRIGGER edicion_protocolos
AFTER UPDATE ON protocolos
FOR EACH ROW
INSERT INTO auditoria_protocolos
    (id_informe,
     id_protocolo,
     informe,
     id_usuario,
     fecha_impresion,
     fecha,
     evento)
    VALUES
    (OLD.id_informe,
     OLD.id_protocolo,
     OLD.informe,
     OLD.id_usuario,
     OLD.fecha_impresion,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_protocolos;

-- lo recreamos
CREATE TRIGGER eliminacion_protocolos
AFTER DELETE ON protocolos
FOR EACH ROW
INSERT INTO auditoria_protocolos
    (id_informe,
     id_protocolo,
     informe,
     id_usuario,
     fecha_impresion,
     fecha,
     evento)
    VALUES
    (OLD.id_informe,
     OLD.id_protocolo,
     OLD.informe,
     OLD.id_usuario,
     OLD.fecha_impresion,
     OLD.fecha,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Tablas de No Conformidades                                     */
/*                                                                                          */
/********************************************************************************************/

/*

    Estructura de la tabla
    id int(6) clave del registro
    usuario int(4) clave del usuario que generó la no conformidad
    titulo varchar(100) descripción breve de la no conformidad
    fecha date fecha de emisión de la no conformidad
    detalle blob descripción de la no conformidad
    causa blob análisis de las causas
    respuesta date fecha de la respuesta
    accion blob detalle de las acciones correctivas
    ejecucion date fecha límite para la ejecución de la acción correctiva

*/

-- eliminamos si existe
DROP TABLE IF EXISTS noconformidad;

-- creamos la tabla
CREATE TABLE noconformidad(
    id int(6) unsigned NOT NULL AUTO_INCREMENT,
    usuario int(4) unsigned NOT NULL,
    titulo varchar(100) NOT NULL,
    fecha date DEFAULT NULL,
    detalle blob DEFAULT NULL,
    causa blob DEFAULT NULL,
    respuesta date DEFAULT NULL,
    accion blob DEFAULT NULL,
    ejecucion date DEFAULT NULL,
    PRIMARY KEY (id),
    KEY usuario(usuario),
    KEY fecha(fecha),
    KEY respuesta(respuesta),
    KEY ejecucion(ejecucion)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Incidencias y no conformidades';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_conformidades;

-- la recreamos
CREATE TABLE auditoria_conformidades(
    id int(6) unsigned NOT NULL ,
    usuario int(4) unsigned NOT NULL,
    titulo varchar(100) NOT NULL,
    fecha date DEFAULT NULL,
    detalle blob DEFAULT NULL,
    causa blob DEFAULT NULL,
    respuesta date DEFAULT NULL,
    accion blob DEFAULT NULL,
    ejecucion date DEFAULT NULL,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY usuario(usuario),
    KEY fecha(fecha),
    KEY respuesta(respuesta),
    KEY ejecucion(ejecucion)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de no conformidades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_conformidades;

-- lo recreamos
CREATE TRIGGER edicion_conformidades
AFTER UPDATE ON noconformidad
FOR EACH ROW
INSERT INTO auditoria_conformidades
    (id,
     usuario,
     fecha,
     titulo,
     detalle,
     causa,
     respuesta,
     accion,
     ejecucion,
     evento)
    VALUES
    (OLD.id,
     OLD.usuario,
     OLD.fecha,
     OLD.titulo,
     OLD.detalle,
     OLD.causa,
     OLD.respuesta,
     OLD.accion,
     OLD.ejecucion,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_conformidades;

-- lo recreamos
CREATE TRIGGER eliminacion_conformidades
AFTER DELETE ON noconformidad
FOR EACH ROW
INSERT INTO auditoria_conformidades
    (id,
     usuario,
     fecha,
     titulo,
     detalle,
     causa,
     respuesta,
     accion,
     ejecucion,
     evento)
    VALUES
    (OLD.id,
     OLD.usuario,
     OLD.fecha,
     OLD.titulo,
     OLD.detalle,
     OLD.causa,
     OLD.respuesta,
     OLD.accion,
     OLD.ejecucion,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Tablas de Control de Stock                                     */
/*                                                                                          */
/********************************************************************************************/

/*

  Vamos a crear tablas para
  1. Items y su descripción
  2. Entradas al depósito
  3. Salidas del Depósito
  4. Inventario en existencia (que se actualiza en tiempo real)
  5. La tabla de fuentes de financiamiento (diccionario)

  Se crean triggers para los eventos y el inventario es controlado directamente por los
  triggers (un ingreso al depósito incrementará el inventario existente y una salida
  lo actualizará) esto permite simplificar el código (el inventario solo se consultará)
  y evitar errores de diseño

*/


/********************************************************************************************/
/*                                                                                          */
/*                               Items del Depósito                                         */
/*                                                                                          */
/********************************************************************************************/

/*
   id entero(4) autonumérico
   id_laboratorio entero(6) laboratorio que utiliza este item
   descripcion varchar(200)
   valor decimal valor de la unidad
   codigosop varchar(20) código del sistema de la apn
   imagen blob imagen del artículo
   critico int(4) valor mínimo crítico
   fecha date
   id_usuario int(4) usuario que editó el registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS items ;

-- la creamos
CREATE TABLE items (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    descripcion varchar(200) NOT NULL,
    valor decimal(10,2) UNSIGNED DEFAULT NULL,
    codigosop varchar(20) DEFAULT NULL,
    imagen blob DEFAULT NULL,
    critico int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY laboratorio(id_laboratorio),
    KEY descripcion(descripcion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Elementos del inventario';

-- eliminamos la tabla
DROP TABLE IF EXISTS auditoria_items ;

-- la creamos
CREATE TABLE auditoria_items (
    id int(4) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    descripcion varchar(200) NOT NULL,
    valor decimal(10,2) UNSIGNED DEFAULT NULL,
    codigosop varchar(20) DEFAULT NULL,
    critico int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    KEY id(id),
    KEY laboratorio(id_laboratorio),
    KEY descripcion(descripcion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los items';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_items;

-- lo recreamos
CREATE TRIGGER edicion_items
AFTER UPDATE ON items
FOR EACH ROW
INSERT INTO auditoria_items
    (id,
     id_laboratorio,
     descripcion,
     valor,
     codigosop,
     critico,
     fecha,
     evento,
     id_usuario)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.descripcion,
     OLD.valor,
     OLD.codigosop,
     OLD.critico,
     OLD.fecha,
     "Edicion",
     OLD.id_usuario);

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_items;

-- lo recreamos
CREATE TRIGGER eliminacion_items
AFTER DELETE ON items
FOR EACH ROW
INSERT INTO auditoria_items
    (id,
     id_laboratorio,
     descripcion,
     valor,
     codigosop,
     critico,
     fecha,
     evento,
     id_usuario)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.descripcion,
     OLD.valor,
     OLD.codigosop,
     OLD.critico,
     OLD.fecha,
     "Eliminacion",
     OLD.id_usuario);


/********************************************************************************************/
/*                                                                                          */
/*                               Entradas del Depósito                                      */
/*                                                                                          */
/********************************************************************************************/

/*

    Ingresos al depósito
    id entero(6) clave del registro
    item entero(6) clave del item
    id_laboratorio entero(6) clave del laboratorio
    remito varchar(20) número o identificación del remito de entrada
    cantidad entero(5) número de unidades
    importe decimal(10,2) importe de la facutura
    vencimiento fecha de vencimiento del producto
    lote varchar(20) número de lote del artículo
    ubicacion varchar(30) ubicación física del artículo
    fecha date fecha de entrada al depósito
    id_usuario int(4) usuario que ingresó el registro
    comentarios text observaciones

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS ingresos;

-- la recreamos
CREATE TABLE ingresos (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    item int(6) UNSIGNED NOT NULL,
    importe decimal(10,2) UNSIGNED DEFAULT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_financiamiento int(1) UNSIGNED NOT NULL,
    remito varchar(20) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    vencimiento date DEFAULT NULL,
    lote varchar(20) DEFAULT NULL,
    ubicacion varchar(30) DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY id_financiamiento(id_financiamiento),
    KEY remito(remito),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Ingresos al Depósito';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_ingresos;

-- la recreamos
CREATE TABLE auditoria_ingresos (
    id int(6) UNSIGNED NOT NULL,
    item int(6) UNSIGNED NOT NULL,
    importe decimal(10,2) UNSIGNED DEFAULT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_financiamiento int(1) UNSIGNED NOT NULL,
    remito varchar(20) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    vencimiento date DEFAULT NULL,
    lote varchar(20) DEFAULT NULL,
    ubicacion varchar(30) DEFAULT NULL,
    fecha date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    comentarios text DEFAULT NULL,
    KEY id(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY id_financiamiento(id_financiamiento),
    KEY remito(remito),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Ingresos al Depósito';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_ingresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER edicion_ingresos
AFTER UPDATE ON ingresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_ingresos
        (id,
        item,
        importe,
        id_laboratorio,
        id_financiamiento,
        remito,
        cantidad,
        vencimiento,
        lote,
        ubicacion,
        fecha,
        id_usuario,
        evento,
        comentarios)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.importe,
        OLD.id_laboratorio,
        OLD.id_financiamiento,
        OLD.remito,
        OLD.cantidad,
        OLD.vencimiento,
        OLD.lote,
        OLD.ubicacion,
        OLD.fecha,
        OLD.id_usuario,
        "Edicion",
        OLD.comentarios);

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia + NEW.cantidad - OLD.cantidad,
                                    inventario.valor = (inventario.existencia + NEW.cantidad - OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
        inventario.item = NEW.item;

END; //

-- cambiamos el delimitador
DELIMITER ;

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ingresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER eliminacion_ingresos
AFTER DELETE ON ingresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_ingresos
        (id,
        item,
        importe,
        id_laboratorio,
        id_financiamiento,
        remito,
        cantidad,
        vencimiento,
        lote,
        ubicacion,
        fecha,
        id_usuario,
        evento,
        comentarios)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.importe,
        OLD.id_laboratorio,
        OLD.id_financiamiento,
        OLD.remito,
        OLD.cantidad,
        OLD.vencimiento,
        OLD.lote,
        OLD.ubicacion,
        OLD.fecha,
        OLD.id_usuario,
        "Eliminacion",
        OLD.comentarios);

    -- actualizamos el inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia - OLD.cantidad,
                                    inventario.valor = (inventario.existencia - OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = OLD.id_laboratorio AND
        inventario.item = OLD.item;

END; //

-- actualizamos el delimitador
DELIMITER ;


/********************************************************************************************/
/*                                                                                          */
/*                               Salidas del Depósito                                       */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla con la información de salidas de material del depósito
    id entero(6) clave del registro
    item int(6) clave del item
    id_laboratorio entero(6) clave del laboratorio
    remito varchar(20) remito de salida
    cantidad int(5) numero de unidades salientes
    fecha date fecha de la operación
    id_entrego int(4) id del usuario que entregó el material
    id_recibio int(4) id del usuario que recibió el material
    comentarios text

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS egresos;

-- la recreamos
CREATE TABLE egresos (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    item int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    remito int(6) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_entrego int(4) UNSIGNED NOT NULL,
    id_recibio int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY remito(remito),
    KEY id_entrego(id_entrego),
    KEY id_recibio(id_recibio)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Egresos del Depósito';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_egresos;

CREATE TABLE auditoria_egresos (
    id int(6) UNSIGNED NOT NULL,
    item int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    remito int(6) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    fecha date DEFAULT NULL,
    id_entrego int(4) UNSIGNED NOT NULL,
    id_recibio int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY remito(remito),
    KEY id_entrego(id_entrego),
    KEY id_recibio(id_recibio)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Egresos del Depósito';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_egresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER edicion_egresos
AFTER UPDATE ON egresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_egresos
        (id,
        item,
        id_laboratorio,
        remito,
        cantidad,
        fecha,
        id_entrego,
        id_recibio,
        comentarios,
        evento)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.id_laboratorio,
        OLD.remito,
        OLD.cantidad,
        OLD.fecha,
        OLD.id_entrego,
        OLD.id_recibio,
        OLD.comentarios,
        "Edicion");

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia - NEW.cantidad + OLD.cantidad,
                                    inventario.valor = (inventario.existencia - NEW.cantidad + OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
        inventario.item = NEW.item;

END; //

-- actualizamos el delimitador
DELIMITER ;

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_egresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER eliminacion_egresos
AFTER DELETE ON egresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_egresos
        (id,
        item,
        id_laboratorio,
        remito,
        cantidad,
        fecha,
        id_entrego,
        id_recibio,
        comentarios,
        evento)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.id_laboratorio,
        OLD.remito,
        OLD.cantidad,
        OLD.fecha,
        OLD.id_entrego,
        OLD.id_recibio,
        OLD.comentarios,
        "Eliminacion");

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia + OLD.cantidad,
                                    inventario.valor = (inventario.existencia + OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = OLD.id_laboratorio AND
          inventario.item = OLD.item;

END; //

-- actualizamos el delimitador
DELIMITER ;


/********************************************************************************************/
/*                                                                                          */
/*                                      Inventario                                          */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de inventario, contiene el resumen y a través de la auditoría el historial de
    elementos en depósito
    id entero(6) clave del registro
    id_laboratorio entero(6) clave del laboratorio
    valor decimal(10,2) valor en existencia (calculado al momento de actualizar la tabla)
    item entero(6) clave del item
    existencia entero(5) número de unidades en existencia

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS inventario;

-- la creamos
CREATE TABLE inventario (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    valor decimal(10,2) DEFAULT NULL,
    item int(6) UNSIGNED NOT NULL,
    existencia int(5) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY id_laboratorio(id_laboratorio),
    KEY item(item)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Inventario en existencia';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_inventario;

-- la recreamos (la clave primaria aquí es el registro de la
-- transacción)
CREATE TABLE auditoria_inventario (
    id int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    valor decimal(10,2) DEFAULT NULL,
    item int(6) UNSIGNED NOT NULL,
    existencia int(5) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Ingreso", "Egreso", "Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_laboratorio(id_laboratorio),
    KEY item(item)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del Inventario';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_inventario;

-- lo recreamos
CREATE TRIGGER edicion_inventario
AFTER UPDATE ON inventario
FOR EACH ROW
INSERT INTO auditoria_inventario
    (id,
     id_laboratorio,
     valor,
     item,
     existencia,
     evento)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.valor,
     OLD.item,
     OLD.existencia,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_inventario;

-- lo recreamos
CREATE TRIGGER eliminacion_inventario
AFTER DELETE ON inventario
FOR EACH ROW
INSERT INTO auditoria_inventario
    (id,
     id_laboratorio,
     valor,
     item,
     existencia,
     evento)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.valor,
     OLD.item,
     OLD.existencia,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           La tabla de pedidos                                            */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de pedidos al depósito, esta tabla permite que los usuarios realicen pedidos de
    material al depósito y luego el usuario autorizado pueda ver la nómina de pedidos
    y aprobarlos (con lo que genera un egreso automáticamente) o rechazarlos
    id int(8) clave del registro
    id_item int(4) clave del item solicitado
    id_usuario int(4) clave del usuario que solicitó
    cantidad int(4) número de unidades solicitadas
    fecha date fecha de la solicitud

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS pedidos;

-- la recreamos
CREATE TABLE pedidos(
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_item int(4) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    cantidad int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_item(id_item),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Pedidos al depósito';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_pedidos;

-- la recreamos
CREATE TABLE auditoria_pedidos(
    id int(8) UNSIGNED NOT NULL,
    id_item int(4) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    cantidad int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_item(id_item),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de pedidos al depósito';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_pedidos;

-- lo recreamos
CREATE TRIGGER edicion_pedidos
AFTER UPDATE ON pedidos
FOR EACH ROW
INSERT INTO auditoria_pedidos
       (id,
        id_item,
        id_usuario,
        cantidad,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_item,
        OLD.id_usuario,
        OLD.cantidad,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_pedidos;

-- lo recreamos
CREATE TRIGGER eliminacion_pedidos
AFTER DELETE ON pedidos
FOR EACH ROW
INSERT INTO auditoria_pedidos
       (id,
        id_item,
        id_usuario,
        cantidad,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_item,
        OLD.id_usuario,
        OLD.cantidad,
        OLD.fecha_alta,
        "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Fuentes de Financiamiento                                      */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de fuentes de financiamiento, no es necesario que sea exclusiva del stock pero
    aquí lo utilizamos para indicar la fuente de financiamiento de los ingresos al
    depósito, este diccionario es individual para cada laboratorio
    id int(1) clave del registro
    fuente varchar(50) descripcion del financiamiento
    id_laboratorio int(6) clave del laboratorio
    id_usuario int(4) clave del usuario
    fecha_alta date fecha del registro

*/

-- la eliminamos si no existe
DROP TABLE IF EXISTS financiamiento;

-- la recreamos
CREATE TABLE financiamiento(
    id int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    fuente varchar(50) NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario con las fuentes de financiamiento';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_financiamiento;

-- la recreamos
CREATE TABLE auditoria_financiamiento(
    id int(1) UNSIGNED NOT NULL,
    fuente varchar(50) NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las fuentes de financiamiento';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_financiamiento;

-- lo recreamos
CREATE TRIGGER edicion_financiamiento
AFTER UPDATE ON financiamiento
FOR EACH ROW
INSERT INTO auditoria_financiamiento
       (id,
        fuente,
        id_laboratorio,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.fuente,
        OLD.id_laboratorio,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_financiamiento;

-- lo recreamos
CREATE TRIGGER eliminacion_financiamiento
AFTER DELETE ON financiamiento
FOR EACH ROW
INSERT INTO auditoria_financiamiento
       (id,
        fuente,
        id_laboratorio,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.fuente,
        OLD.id_laboratorio,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Eliminacion");


/*

    Aquí creamos los triggers que se van a encargar de controlar el inventario, usamos el
    disparador BEFORE (antes) para usar el otro evento ya que MySQL solo soporta un trigger
    por evento, el concepto es
    - Cuando se elimina una entrada al depósito, resta esa entrada del existente
    - Cuando se elimina una salida al depósito, suma esa salida al existente
    - Cuando se inserta una entrada al depósito, suma esa entrada al existente
    - Cuando se inserta una salida del depósito, resta esa entrada al existente
    - Cuando se edita una entrada, resta la entrada anterior y suma la nueva
    - Cuando se edita una salida, suma la salida anterior y resta la nueva
    Tendremos entonces seis triggers (tres para la tabla de entradas y tres para la tabla
    de salidas)

    Creamos también un trigger para la inserción de items en el diccionario que inicializa
    a cero la tabla de inventario, de tal forma nos aseguramos que todos los items del
    diccionario tienen su correspondencia en el inventario

*/

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS insercion_items;

-- lo recreamos desde cero
CREATE TRIGGER insercion_items
AFTER INSERT ON items
FOR EACH ROW
INSERT INTO inventario
    (id_laboratorio,
     valor,
     item,
     existencia)
    VALUES
    (NEW.id_laboratorio,
     NEW.valor,
     NEW.id,
     '0');

-- eliminamos el trigger de inserción de entradas al depósito
DROP TRIGGER IF EXISTS nueva_entrada_inventario;

-- lo recreamos
CREATE TRIGGER nueva_entrada_inventario
AFTER INSERT ON ingresos
FOR EACH ROW
UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                inventario.item = items.id
                             SET inventario.existencia = inventario.existencia + NEW.cantidad,
                                 inventario.valor = (inventario.existencia + NEW.cantidad) * items.valor
WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
      inventario.item = NEW.item;

-- eliminamos el trigger de inserción de salidas al depósito
DROP TRIGGER IF EXISTS nueva_salida_inventario;

-- lo recreamos
CREATE TRIGGER nueva_salida_inventario
AFTER INSERT ON egresos
FOR EACH ROW
UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                inventario.item = items.id
                             SET inventario.existencia = inventario.existencia - NEW.cantidad,
                                 inventario.valor = (inventario.existencia - NEW.cantidad) * items.valor
WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
      inventario.item = NEW.item;


/***********************************************************************************************/
/*                                                                                             */
/*                              Freezers y Heladeras                                           */
/*                                                                                             */
/***********************************************************************************************/

/*

    Tabla que contiene la información sobre las heladeras a controlar
    id int(6) clave del registro
    laboratorio int(6) clave del laboratorio de la heladera
    marca varchar(50) marca de la heladera
    ubicación varchar(100) descripción de donde se encuentra
    patrimonio varchar(100) cadena con la id de patrimonio
    temperatura int(3) temperatura que que debe estar (con signo)
    temperatura2 int(3) otra temperatura por defecto (para heladeras de dos)
    tolerancia int(2) porcentaje de tolerancia en la temperatura
    usuario int(4) clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS heladeras;

-- la recreamos
CREATE TABLE heladeras (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    laboratorio int(6) UNSIGNED NOT NULL,
    marca varchar(50) NOT NULL,
    ubicacion varchar(100) DEFAULT NULL,
    patrimonio varchar(100) DEFAULT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    tolerancia int(2) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de heladeras';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_heladeras;

-- la recreamos
CREATE TABLE auditoria_heladeras (
    id int(6) UNSIGNED NOT NULL,
    laboratorio int(6) NOT NULL,
    marca varchar(50) NOT NULL,
    ubicacion varchar(100) DEFAULT NULL,
    patrimonio varchar(100) DEFAULT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    tolerancia int(2) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de auditoría de heladeras';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_heladeras;

-- lo recreamos
CREATE TRIGGER edicion_heladeras
AFTER UPDATE ON heladeras
FOR EACH ROW
INSERT INTO auditoria_heladeras
       (id,
        laboratorio,
        marca,
        ubicacion,
        patrimonio,
        temperatura,
        temperatura2,
        tolerancia,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.laboratorio,
        OLD.marca,
        OLD.ubicacion,
        OLD.patrimonio,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.tolerancia,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_heladeras;

CREATE TRIGGER eliminacion_heladeras
AFTER DELETE ON heladeras
FOR EACH ROW
INSERT INTO auditoria_heladeras
       (id,
        laboratorio,
        marca,
        ubicacion,
        patrimonio,
        temperatura,
        temperatura2,
        tolerancia,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.laboratorio,
        OLD.marca,
        OLD.ubicacion,
        OLD.patrimonio,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.tolerancia,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***********************************************************************************************/
/*                                                                                             */
/*                                     Temperaturas                                            */
/*                                                                                             */
/***********************************************************************************************/

/*

    Tabla con la información de las temperaturas de las heladeras
    id int(6) clave del registro
    heladera int(2) clave de la heladera
    fecha date fecha y hora del registro
    temperatura int(3) temperatura medida con signo
    temperatura2 int(3) segunda temperatura
    controlado int(4) clave del usuario

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS temperaturas;

-- la recreamos
CREATE TABLE temperaturas(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    heladera int(2) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    controlado int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY heladera(heladera),
    KEY controlado(controlado)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de temperaturas';

-- eliminamos la tabla de auditorías si existe
DROP TABLE IF EXISTS auditoria_temperaturas;

-- la recreamos
CREATE TABLE auditoria_temperaturas(
    id int(6) UNSIGNED NOT NULL,
    heladera int(2) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    controlado int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY heladera(heladera),
    KEY controlado(controlado)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditorìa de la tabla de temperaturas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_temperaturas;

-- lo recreamos
CREATE TRIGGER edicion_temperaturas
AFTER UPDATE ON temperaturas
FOR EACH ROW
INSERT INTO auditoria_temperaturas
       (id,
        heladera,
        fecha,
        temperatura,
        temperatura2,
        controlado,
        evento)
       VALUES
       (OLD.id,
        OLD.heladera,
        OLD.fecha,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.controlado,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_temperaturas;

-- lo recreamos
CREATE TRIGGER eliminacion_temperaturas
AFTER DELETE ON temperaturas
FOR EACH ROW
INSERT INTO auditoria_temperaturas
       (id,
        heladera,
        fecha,
        temperatura,
        temperatura2,
        controlado,
        evento)
       VALUES
       (OLD.id,
        OLD.heladera,
        OLD.fecha,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.controlado,
        "Eliminacion");


/***********************************************************************************************/
/*                                                                                             */
/*                                Vistas del Sistema                                           */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de usuarios si existe
DROP VIEW IF EXISTS v_usuarios;

-- la creamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_usuarios AS
    SELECT cce.responsables.id AS idusuario,
        UCASE(cce.responsables.nombre) AS nombreusuario,
        cce.responsables.password AS password,
        UCASE(cce.responsables.cargo) AS cargousuario,
        cce.responsables.institucion AS institucion,
        cce.responsables.telefono AS telefono,
        cce.responsables.e_mail AS e_mail,
        cce.responsables.direccion AS direccion,
        cce.responsables.codigo_postal AS codigo_postal,
        diagnostico.usuarios.laboratorio AS idlaboratorio,
        cce.laboratorios.nombre AS nombrelaboratorio,
        cce.responsables.usuario AS usuario,
        cce.responsables.coordenadas AS coordenadas,
        cce.responsables.activo AS activo,
        DATE_FORMAT(cce.responsables.fecha_alta, '%d/%d/%Y') AS fecha_alta,
        autorizo.usuario AS autorizo,
        diagnostico.usuarios.administrador AS administrador,
        cce.responsables.nivel_central AS nivelcentral,
        diagnostico.usuarios.personas AS personas,
        diagnostico.usuarios.congenito AS congenito,
        diagnostico.usuarios.resultados AS resultados,
        diagnostico.usuarios.muestras AS muestras,
        diagnostico.usuarios.entrevistas AS entrevistas,
        diagnostico.usuarios.auxiliares AS auxiliares,
        diagnostico.usuarios.protocolos AS protocolos,
        diagnostico.usuarios.stock AS stock,
        diagnostico.usuarios.usuarios AS usuarios,
        diccionarios.localidades.NOMLOC AS localidad,
        diccionarios.localidades.CODLOC AS codloc,
        diccionarios.provincias.NOM_PROV AS provincia,
        diccionarios.provincias.COD_PROV AS codprov,
        diccionarios.paises.NOMBRE AS pais,
        diccionarios.paises.ID AS idpais,
        diagnostico.usuarios.firma AS firma,
        cce.responsables.observaciones AS comentarios
    FROM cce.responsables LEFT JOIN diagnostico.usuarios ON cce.responsables.ID = diagnostico.usuarios.id
                          LEFT JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.ID
                          INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                          INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                          INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                          LEFT JOIN cce.responsables AS autorizo ON diagnostico.usuarios.autorizo = autorizo.id;

-- eliminamos la vista de marcas y técnicas si existe
DROP VIEW IF EXISTS v_tecnicas;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_tecnicas AS
    SELECT diagnostico.tecnicas.id AS idtecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombretecnica,
           diagnostico.marcas.id AS idmarca,
           diagnostico.marcas.marca AS marca
    FROM diagnostico.tecnicas INNER JOIN diagnostico.marcas ON diagnostico.tecnicas.id = diagnostico.marcas.tecnica
    ORDER BY diagnostico.tecnicas.tecnica ASC;

-- eliminamos la vista de inventario si existe
DROP VIEW IF EXISTS v_inventario;

-- creamos la vista desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_inventario AS
    SELECT diagnostico.items.id AS iditem,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.imagen AS imagen,
           diagnostico.items.critico AS critico,
           diagnostico.inventario.existencia AS existencia,
           diagnostico.inventario.valor AS importe,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.id AS idlaboratorio
    FROM diagnostico.items INNER JOIN diagnostico.inventario ON diagnostico.items.id = diagnostico.inventario.id
                           INNER JOIN cce.laboratorios ON diagnostico.inventario.id_laboratorio = cce.laboratorios.id
    ORDER BY diagnostico.items.descripcion ASC;

-- eliminamos la vista de ingresos si existe
DROP VIEW IF EXISTS v_ingresos;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_ingresos AS
    SELECT diagnostico.items.id AS iditem,
        diagnostico.items.descripcion AS descripcion,
        diagnostico.items.imagen AS imagen,
        diagnostico.items.codigosop AS codigosop,
        diagnostico.ingresos.id AS idingreso,
        diagnostico.ingresos.importe AS importe,
        diagnostico.ingresos.id_laboratorio AS idlaboratorio,
        diagnostico.ingresos.id_financiamiento AS id_financiamiento,
        diagnostico.financiamiento.fuente AS financiamiento,
        cce.laboratorios.nombre AS laboratorio,
        diagnostico.ingresos.remito AS remito,
        diagnostico.ingresos.cantidad AS cantidad,
        DATE_FORMAT(diagnostico.ingresos.vencimiento, '%d/%m/%Y') AS vencimiento,
        DATE_FORMAT(diagnostico.ingresos.fecha, '%d/%m/%Y') AS fecha_ingreso,
        diagnostico.ingresos.lote AS lote,
        diagnostico.ingresos.ubicacion AS ubicacion,
        cce.responsables.usuario AS usuario,
        cce.responsables.nombre AS nombre_usuario,
        cce.responsables.id AS idusuario,
        diagnostico.ingresos.comentarios AS comentarios
    FROM diagnostico.ingresos INNER JOIN diagnostico.items ON diagnostico.ingresos.item = diagnostico.items.id
                              INNER JOIN cce.laboratorios ON diagnostico.ingresos.id_laboratorio = cce.laboratorios.id
                              INNER JOIN cce.responsables ON diagnostico.ingresos.id_usuario = cce.responsables.id
                              INNER JOIN diagnostico.financiamiento ON diagnostico.ingresos.id_financiamiento = diagnostico.financiamiento.id
    ORDER BY diagnostico.items.descripcion ASC,
             diagnostico.ingresos.fecha ASC;

-- eliminamos la vista de egresos si existe
DROP VIEW IF EXISTS v_egresos;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_egresos AS
    SELECT diagnostico.items.id AS iditem,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.imagen AS imagen,
           diagnostico.egresos.id AS idegreso,
           diagnostico.egresos.id_laboratorio AS idlaboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.egresos.remito AS remito,
           diagnostico.egresos.cantidad AS cantidad,
           DATE_FORMAT(diagnostico.egresos.fecha, '%d/%m/%Y') AS fecha_egreso,
           cce.responsables.usuario AS entrego,
           cce.responsables.nombre AS nombre_entrego,
           cce.responsables.id AS id_entrego,
           recepcion.usuario AS recibio,
           recepcion.nombre AS nombre_recibio,
           recepcion.id AS idrecibio,
           diagnostico.egresos.comentarios AS comentarios
    FROM diagnostico.egresos INNER JOIN diagnostico.items ON diagnostico.egresos.item = diagnostico.items.id
                             INNER JOIN cce.laboratorios ON diagnostico.egresos.id_laboratorio = cce.laboratorios.id
                             INNER JOIN cce.responsables ON diagnostico.egresos.id_entrego = cce.responsables.id
                             INNER JOIN cce.responsables AS recepcion ON diagnostico.egresos.id_recibio = recepcion.id
    ORDER BY diagnostico.items.descripcion ASC,
             diagnostico.egresos.fecha ASC;

-- eliminamos la vista de jurisdicciones
DROP VIEW IF EXISTS v_jurisdicciones;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_jurisdicciones AS
    SELECT diccionarios.paises.id AS idpais,
           diccionarios.paises.nombre AS pais,
           diccionarios.provincias.cod_prov AS codprovincia,
           diccionarios.provincias.nom_prov AS provincia,
           diccionarios.provincias.poblacion AS poblacionprovincia,
           diccionarios.localidades.codloc AS codlocalidad,
           diccionarios.localidades.nomloc AS localidad,
           diccionarios.localidades.poblacion AS poblacionlocalidad
    FROM diccionarios.paises INNER JOIN diccionarios.provincias ON diccionarios.paises.id = diccionarios.provincias.pais
                INNER JOIN diccionarios.localidades ON diccionarios.provincias.cod_prov = diccionarios.localidades.codpcia
    ORDER BY diccionarios.paises.nombre,
             diccionarios.provincias.nom_prov,
             diccionarios.localidades.nomloc;

-- eliminamos la vista de items si existe
DROP VIEW IF EXISTS v_items;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_items AS
    SELECT diagnostico.items.id AS id_item,
           diagnostico.items.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.valor AS valor,
           diagnostico.items.imagen AS imagen,
           diagnostico.items.critico AS critico,
           DATE_FORMAT(diagnostico.items.fecha, '%d/%m/%Y') AS fecha,
           diagnostico.items.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario
    FROM diagnostico.items INNER JOIN cce.responsables ON diagnostico.items.id_usuario = cce.responsables.id
                           INNER JOIN cce.laboratorios ON diagnostico.items.id_laboratorio = cce.laboratorios.id;

-- eliminamos la vista de fuentes de financiamiento
DROP VIEW IF EXISTS v_financiamiento;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_financiamiento AS
    SELECT diagnostico.financiamiento.id AS id_financiamiento,
           diagnostico.financiamiento.fuente AS fuente,
           diagnostico.financiamiento.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.financiamiento.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.financiamiento.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.financiamiento INNER JOIN cce.laboratorios ON diagnostico.financiamiento.id_laboratorio = cce.laboratorios.id
                                    INNER JOIN cce.responsables ON diagnostico.financiamiento.id_usuario = cce.responsables.id
    ORDER BY diagnostico.financiamiento.fuente;

-- eliminamos la vista de pedidos si existe
DROP VIEW IF EXISTS v_pedidos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_pedidos AS
    SELECT diagnostico.pedidos.id AS id_pedido,
           diagnostico.pedidos.id_item AS id_item,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.imagen AS imagen,
           diagnostico.pedidos.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           diagnostico.pedidos.cantidad AS cantidad,
           diagnostico.inventario.existencia AS existencia,
           cce.laboratorios.id AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           DATE_FORMAT(diagnostico.pedidos.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.pedidos INNER JOIN diagnostico.items ON diagnostico.pedidos.id_item = diagnostico.items.id
                             INNER JOIN cce.responsables ON diagnostico.pedidos.id_usuario = cce.responsables.id
                             INNER JOIN diagnostico.inventario ON diagnostico.pedidos.id_item = diagnostico.inventario.item
                             INNER JOIN diagnostico.usuarios ON cce.responsables.id = diagnostico.usuarios.id
                             INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id
    ORDER BY diagnostico.pedidos.fecha_alta;

-- eliminamos la vista de laboratorios si existe
DROP VIEW IF EXISTS v_laboratorios;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_laboratorios AS
    SELECT cce.laboratorios.id AS id,
        UPPER(cce.laboratorios.nombre) AS laboratorio,
        UPPER(cce.laboratorios.responsable) AS responsable,
        cce.laboratorios.pais AS idpais,
        diccionarios.paises.nombre AS pais,
        cce.laboratorios.localidad AS codloc,
        diccionarios.localidades.nomloc AS localidad,
        diccionarios.provincias.nom_prov AS provincia,
        diccionarios.provincias.cod_prov AS codprov,
        cce.laboratorios.direccion AS direccion,
        cce.laboratorios.codigo_postal AS codigo_postal,
        cce.laboratorios.coordenadas AS coordenadas,
        cce.laboratorios.dependencia AS iddependencia,
        diccionarios.dependencias.dependencia AS dependencia,
        cce.laboratorios.e_mail AS email,
        DATE_FORMAT(cce.laboratorios.fecha_alta, '%d/%m/%Y') AS fecha_alta,
        cce.laboratorios.activo AS activo,
        cce.laboratorios.recibe_muestras_chagas AS recibe_muestras,
        cce.laboratorios.id_recibe AS id_recibe,
        usuarios.usuario AS nombre_recibe,
        cce.laboratorios.observaciones AS observaciones,
        cce.laboratorios.logo AS logo,
        cce.laboratorios.usuario AS id_usuario,
        cce.responsables.usuario AS usuario
    FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.pais = diccionarios.paises.id
                          INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                          INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                          LEFT JOIN cce.responsables AS usuarios ON cce.laboratorios.id_recibe = usuarios.id
                          INNER JOIN diccionarios.dependencias ON cce.laboratorios.dependencia = diccionarios.dependencias.id_dependencia
                          INNER JOIN cce.responsables ON cce.laboratorios.usuario = cce.responsables.id;

-- elimina la vista de entrevistas si existe
DROP VIEW IF EXISTS v_entrevistas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_entrevistas AS
    SELECT diagnostico.entrevistas.id_entrevista AS id_entrevista,
           diagnostico.entrevistas.id_protocolo AS id_protocolo,
           diagnostico.entrevistas.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.entrevistas.resultado AS resultado,
           diagnostico.entrevistas.actitud AS actitud,
           diagnostico.entrevistas.concurrio AS concurrio,
           diagnostico.entrevistas.tipo AS tipo_entrevista,
           DATE_FORMAT(diagnostico.entrevistas.fecha, '%d/%m/%Y') AS fecha,
           diagnostico.entrevistas.comentarios AS comentarios,
           diagnostico.entrevistas.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario
    FROM diagnostico.entrevistas INNER JOIN cce.responsables ON diagnostico.entrevistas.id_usuario = cce.responsables.id
                                 INNER JOIN cce.laboratorios ON diagnostico.entrevistas.id_laboratorio = cce.laboratorios.id;

-- eliminamos la consulta de congenito
DROP VIEW IF EXISTS v_congenito;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_congenito AS
    SELECT diagnostico.congenito.id AS id_congenito,
           diagnostico.congenito.id_protocolo AS id_protocolo,
           diagnostico.congenito.id_madre AS id_madre,
           diagnostico.congenito.id_sivila AS id_sivila,
           DATE_FORMAT(diagnostico.congenito.reportado, '%d/%m/%Y') AS reportado,
           diagnostico.congenito.parto AS parto,
           diagnostico.congenito.peso AS peso,
           diagnostico.congenito.prematuro AS prematuro,
           diagnostico.congenito.institucion AS id_institucion,
           diagnostico.centros_asistenciales.nombre AS institucion,
           diagnostico.congenito.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.congenito.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.congenito.comentarios AS comentarios
    FROM diagnostico.congenito INNER JOIN cce.responsables ON diagnostico.congenito.id_usuario = cce.responsables.id
                               INNER JOIN diagnostico.centros_asistenciales ON diagnostico.congenito.institucion = diagnostico.centros_asistenciales.id;

-- eliminamos la vista de pacientes
DROP VIEW IF EXISTS v_personas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_personas AS
    SELECT diagnostico.personas.protocolo AS protocolo,
           diagnostico.personas.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.personas.historia_clinica AS historia_clinica,
           diagnostico.personas.apellido AS apellido,
           diagnostico.personas.nombre AS nombre,
           diagnostico.personas.documento AS documento,
           diagnostico.personas.tipo_documento AS id_documento,
           diccionarios.tipo_documento.des_abreviada AS tipo_documento,
           DATE_FORMAT(diagnostico.personas.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacimiento,
           diagnostico.personas.edad AS edad,
           diagnostico.personas.sexo AS id_sexo,
           diccionarios.sexos.sexo AS sexo,
           diagnostico.personas.estado_civil AS id_estado_civil,
           diccionarios.estado_civil.estado_civil AS estado_civil,
           diagnostico.personas.hijos AS hijos,
           diagnostico.personas.direccion AS direccion,
           diagnostico.personas.telefono AS telefono,
           diagnostico.personas.celular AS celular,
           diagnostico.personas.compania AS id_compania,
           diccionarios.celulares.compania AS compania,
           diagnostico.personas.mail AS mail,
           diagnostico.personas.localidad_nacimiento AS id_localidad_nacimiento,
           localidad_nacimiento.nomloc AS localidad_nacimiento,
           jurisdiccion_nacimiento.cod_prov AS id_jurisdiccion_nacimiento,
           jurisdiccion_nacimiento.nom_prov AS jurisdiccion_nacimiento,
           pais_nacimiento.id AS id_pais_nacimiento,
           pais_nacimiento.nombre AS pais_nacimiento,
           diagnostico.personas.coordenadas_nacimiento AS coordenadas_nacimiento,
           diagnostico.personas.localidad_residencia AS id_localidad_residencia,
           localidad_residencia.nomloc AS localidad_residencia,
           jurisdiccion_residencia.cod_prov AS id_jurisdiccion_residencia,
           jurisdiccion_residencia.nom_prov AS jurisdiccion_residencia,
           pais_residencia.id AS id_pais_residencia,
           pais_residencia.nombre AS pais_residencia,
           diagnostico.personas.coordenadas_residencia AS coordenadas_residencia,
           diagnostico.personas.localidad_madre AS id_localidad_madre,
           localidad_madre.nomloc AS localidad_madre,
           jurisdiccion_madre.cod_prov AS id_jurisdiccion_madre,
           jurisdiccion_madre.nom_prov AS jurisdiccion_madre,
           pais_madre.id AS id_pais_madre,
           pais_madre.nombre AS pais_madre,
           diagnostico.personas.madre_positiva AS madre_positiva,
           diagnostico.personas.ocupacion AS ocupacion,
           diagnostico.personas.obra_social AS obra_social,
           diagnostico.personas.motivo AS id_motivo,
           diagnostico.motivos.motivo AS motivo,
           diagnostico.personas.derivacion AS derivacion,
           diagnostico.personas.tratamiento AS tratamiento,
           diagnostico.personas.duracion AS duracion,
           diagnostico.personas.terminado AS terminado,
           diagnostico.personas.usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.personas.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.personas.comentarios AS comentarios
    FROM diagnostico.personas INNER JOIN cce.responsables ON diagnostico.personas.usuario = cce.responsables.id
                              INNER JOIN diccionarios.localidades AS localidad_nacimiento ON diagnostico.personas.localidad_nacimiento = localidad_nacimiento.codloc
                              INNER JOIN diccionarios.provincias AS jurisdiccion_nacimiento ON localidad_nacimiento.codpcia = jurisdiccion_nacimiento.cod_prov
                              INNER JOIN diccionarios.paises AS pais_nacimiento ON jurisdiccion_nacimiento.pais = pais_nacimiento.id
                              INNER JOIN diccionarios.localidades AS localidad_residencia ON diagnostico.personas.localidad_residencia = localidad_residencia.codloc
                              INNER JOIN diccionarios.provincias AS jurisdiccion_residencia ON localidad_residencia.codpcia = jurisdiccion_residencia.cod_prov
                              INNER JOIN diccionarios.paises AS pais_residencia ON jurisdiccion_residencia.pais = pais_residencia.id
                              INNER JOIN diccionarios.localidades AS localidad_madre ON diagnostico.personas.localidad_madre = localidad_madre.codloc
                              INNER JOIN diccionarios.provincias AS jurisdiccion_madre ON localidad_madre.codpcia = jurisdiccion_madre.cod_prov
                              INNER JOIN diccionarios.paises AS pais_madre ON jurisdiccion_madre.pais = pais_madre.id
                              INNER JOIN cce.laboratorios ON diagnostico.personas.id_laboratorio = cce.laboratorios.id
                              INNER JOIN diccionarios.tipo_documento ON diagnostico.personas.tipo_documento = diccionarios.tipo_documento.id_documento
                              INNER JOIN diccionarios.sexos ON diagnostico.personas.sexo = diccionarios.sexos.id
                              INNER JOIN diccionarios.estado_civil ON diagnostico.personas.estado_civil = diccionarios.estado_civil.id_estado
                              INNER JOIN diccionarios.celulares ON diagnostico.personas.compania = diccionarios.celulares.id
                              INNER JOIN diagnostico.motivos ON diagnostico.personas.motivo = diagnostico.motivos.id_motivo;

-- eliminamos la vista de marcas si existe
DROP VIEW IF EXISTS v_marcas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_marcas AS
    SELECT diagnostico.marcas.id AS id_marca,
           diagnostico.marcas.marca AS marca,
           diagnostico.marcas.tecnica AS id_tecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre_tecnica,
           DATE_FORMAT(diagnostico.marcas.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.marcas.usuario AS id_usuario,
           cce.responsables.usuario AS usuario
    FROM diagnostico.marcas INNER JOIN cce.responsables ON diagnostico.marcas.usuario = cce.responsables.id
                            INNER JOIN diagnostico.tecnicas ON diagnostico.marcas.tecnica = diagnostico.tecnicas.id;

-- eliminamos la vista de transplantes
DROP VIEW IF EXISTS v_transplantes;

-- la recreamos incluyendo el laboratorio y la clave para luego
-- poder determinar que laboratorio ingresó el registro (y por
-- tanto, puede eliminarlo)
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_transplantes AS
    SELECT diagnostico.transplantes.id AS id_transplante,
           diagnostico.transplantes.id_protocolo AS protocolo,
           diagnostico.transplantes.organo AS id_organo,
           diagnostico.organos.organo AS organo,
           diagnostico.transplantes.positivo AS positivo,
           DATE_FORMAT(diagnostico.transplantes.fecha_transplante, '%d/%m/%Y') AS fecha_transplante,
           diagnostico.transplantes.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.id AS id_laboratorio,
           DATE_FORMAT(diagnostico.transplantes.fecha, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.transplantes INNER JOIN cce.responsables ON diagnostico.transplantes.id_usuario = cce.responsables.id
                                  INNER JOIN diagnostico.organos ON diagnostico.transplantes.organo = diagnostico.organos.id
                                  INNER JOIN diagnostico.usuarios ON cce.responsables.id = diagnostico.usuarios.id
                                  INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id;

-- eliminamos la vista de transfusiones
DROP VIEW IF EXISTS v_transfusiones;

-- la recreamos (agregamos la clave y el nombre del laboratorio
-- del usuario para identificar quien denunció la transfusión)
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_transfusiones AS
    SELECT diagnostico.transfusiones.id AS id_transfusion,
           diagnostico.transfusiones.id_protocolo AS id_protocolo,
           DATE_FORMAT(diagnostico.transfusiones.fecha_transfusion, '%d/%m/%Y') AS fecha_transfusion,
           diagnostico.transfusiones.motivo AS motivo,
           diagnostico.transfusiones.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.id AS id_laboratorio,
           DATE_FORMAT(diagnostico.transfusiones.fecha, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.transfusiones INNER JOIN cce.responsables ON diagnostico.transfusiones.id_usuario = cce.responsables.id
                                   INNER JOIN diagnostico.usuarios ON cce.responsables.id = diagnostico.usuarios.id
                                   INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id;

-- eliminamos la vista de centros asistenciales
DROP VIEW IF EXISTS v_instituciones;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_instituciones AS
    SELECT diagnostico.centros_asistenciales.id AS id_centro,
           UPPER(diagnostico.centros_asistenciales.nombre) AS nombre_centro,
           diagnostico.centros_asistenciales.localidad AS cod_loc,
           diccionarios.localidades.nomloc AS localidad,
           diccionarios.provincias.cod_prov AS cod_prov,
           diccionarios.provincias.nom_prov AS provincia,
           diccionarios.paises.id AS id_pais,
           diccionarios.paises.nombre AS pais,
           diagnostico.centros_asistenciales.direccion AS direccion,
           diagnostico.centros_asistenciales.codigo_postal AS codigo_postal,
           diagnostico.centros_asistenciales.telefono AS telefono,
           diagnostico.centros_asistenciales.mail AS mail,
           UPPER(diagnostico.centros_asistenciales.responsable) AS responsable,
           diagnostico.centros_asistenciales.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           diagnostico.centros_asistenciales.dependencia AS id_dependencia,
           diccionarios.dependencias.dependencia AS dependencia,
           DATE_FORMAT(diagnostico.centros_asistenciales.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.centros_asistenciales.comentarios AS comentarios,
           diagnostico.centros_asistenciales.coordenadas AS coordenadas
    FROM diagnostico.centros_asistenciales INNER JOIN cce.responsables ON diagnostico.centros_asistenciales.id_usuario = cce.responsables.id
                                           INNER JOIN diccionarios.localidades ON diagnostico.centros_asistenciales.localidad = diccionarios.localidades.codloc
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.provincias.id
                                           INNER JOIN diccionarios.dependencias ON diagnostico.centros_asistenciales.dependencia = diccionarios.dependencias.id_dependencia;

-- eliminamos la vista de marcas
DROP VIEW IF EXISTS v_marcas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_marcas AS
    SELECT diagnostico.marcas.id AS id_marca,
           diagnostico.marcas.marca AS marca,
           diagnostico.marcas.tecnica AS id_tecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre,
           diagnostico.marcas.usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.marcas.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.marcas INNER JOIN diagnostico.tecnicas ON diagnostico.marcas.tecnica = diagnostico.tecnicas.id
                            INNER JOIN cce.responsables ON diagnostico.marcas.usuario = cce.responsables.id;

-- eliminamos la vista de técnicas
DROP VIEW IF EXISTS v_tecnicas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_tecnicas AS
    SELECT diagnostico.tecnicas.id AS id_tecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre,
           diagnostico.tecnicas.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.tecnicas.fecha, '%d/%m/%Y') AS fecha
    FROM diagnostico.tecnicas INNER JOIN cce.responsables ON diagnostico.tecnicas.id_usuario = cce.responsables.id;

-- eliminamos la vista de valores
DROP VIEW IF EXISTS v_valores;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_valores AS
    SELECT diagnostico.valores_tecnicas.id AS id_valor,
           diagnostico.valores_tecnicas.id_tecnica AS id_tecnica,
           diagnostico.valores_tecnicas.valor AS valor,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre,
           diagnostico.valores_tecnicas.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.valores_tecnicas.fecha, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.valores_tecnicas INNER JOIN diagnostico.tecnicas ON diagnostico.valores_tecnicas.id_tecnica = diagnostico.tecnicas.id
                                      INNER JOIN cce.responsables ON diagnostico.valores_tecnicas.id_usuario = cce.responsables.id;

-- eliminamos la vista del diccionario de enfermedades
DROP VIEW IF EXISTS v_enfermedades;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_enfermedades AS
    SELECT diagnostico.enfermedades.id AS id_enfermedad,
           diagnostico.enfermedades.enfermedad AS enfermedad,
           diagnostico.enfermedades.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.enfermedades.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.enfermedades INNER JOIN cce.responsables ON diagnostico.enfermedades.id_usuario = cce.responsables.id
    ORDER BY diagnostico.enfermedades.enfermedad;

-- eliminamos la vista de otras enfermedades
DROP VIEW IF EXISTS v_otras_enfermedades;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_otras_enfermedades AS
    SELECT diagnostico.otras_enfermedades.id AS id_registro,
           diagnostico.otras_enfermedades.id_protocolo AS protocolo,
           diagnostico.personas.historia_clinica AS historia_clinica,
           diagnostico.personas.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           CONCAT(diagnostico.personas.apellido, ', ', diagnostico.personas.nombre) AS nombre,
           diccionarios.tipo_documento.des_abreviada AS tipo_documento,
           diagnostico.personas.documento AS documento,
           DATE_FORMAT(diagnostico.personas.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacimiento,
           diagnostico.personas.madre_positiva AS madre_positiva,
           diagnostico.personas.tratamiento AS tratamiento,
           diagnostico.personas.duracion AS duracion,
           diagnostico.personas.terminado AS terminado,
           diagnostico.otras_enfermedades.id_enfermedad AS id_enfermedad,
           diagnostico.enfermedades.enfermedad AS enfermedad,
           diagnostico.otras_enfermedades.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.otras_enfermedades.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.otras_enfermedades INNER JOIN cce.responsables ON diagnostico.otras_enfermedades.id_usuario = cce.responsables.id
                                        INNER JOIN diagnostico.enfermedades ON diagnostico.otras_enfermedades.id_enfermedad = diagnostico.enfermedades.id
                                        INNER JOIN diagnostico.personas ON diagnostico.otras_enfermedades.id_protocolo = diagnostico.personas.protocolo
                                        INNER JOIN diccionarios.tipo_documento ON diagnostico.personas.tipo_documento = diccionarios.tipo_documento.id_documento
                                        INNER JOIN cce.laboratorios ON diagnostico.personas.id_laboratorio = cce.laboratorios.id;

-- eliminamos la vista de no conformidades
DROP VIEW IF EXISTS v_conformidades;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_conformidades AS
    SELECT diagnostico.noconformidad.id AS id,
           diagnostico.noconformidad.usuario AS idusuario,
           cce.responsables.usuario AS usuario,
           cce.responsables.nombre AS nombre,
           cce.laboratorios.id AS idlaboratorio,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.responsable AS jefelaboratorio,
           cce.laboratorios.logo AS logo,
           DATE_FORMAT(diagnostico.noconformidad.fecha, '%d/%m/%Y') AS fecha,
           diagnostico.noconformidad.titulo AS titulo,
           diagnostico.noconformidad.detalle AS detalle,
           diagnostico.noconformidad.causa AS causa,
           DATE_FORMAT(diagnostico.noconformidad.respuesta, '%d/%m/%Y') AS respuesta,
           diagnostico.noconformidad.accion AS accion,
           DATE_FORMAT(diagnostico.noconformidad.ejecucion, '%d/%m/%Y') AS ejecucion
    FROM diagnostico.noconformidad INNER JOIN cce.responsables ON diagnostico.noconformidad.usuario = cce.responsables.id
                                   INNER JOIN diagnostico.usuarios ON diagnostico.noconformidad.usuario = diagnostico.usuarios.id
                                   INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id;

-- eliminamos la vista de temperaturas y heladeras
DROP VIEW IF EXISTS v_heladeras;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_heladeras AS
    SELECT diagnostico.heladeras.id AS id_heladera,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.heladeras.laboratorio AS idlaboratorio,
           UCASE(diagnostico.heladeras.marca) AS marca,
           diagnostico.heladeras.ubicacion AS ubicacion,
           diagnostico.heladeras.patrimonio AS patrimonio,
           diagnostico.heladeras.temperatura AS temperatura,
           diagnostico.heladeras.temperatura2 AS temperatura2,
           diagnostico.heladeras.tolerancia AS tolerancia,
           diagnostico.heladeras.usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.heladeras.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.temperaturas.id AS id_registro,
           DATE_FORMAT(diagnostico.temperaturas.fecha, '%d/%m/%Y') AS fecha_lectura,
           DATE_FORMAT(diagnostico.temperaturas.fecha, '%H:%i') AS hora_lectura,
           diagnostico.temperaturas.temperatura AS valor_lectura,
           diagnostico.temperaturas.temperatura2 AS valor_lectura2,
           diagnostico.temperaturas.controlado AS id_controlado,
           controladores.usuario AS controlado
    FROM diagnostico.heladeras INNER JOIN cce.responsables ON diagnostico.heladeras.usuario = cce.responsables.id
                               LEFT JOIN cce.responsables AS controladores ON controladores.id = cce.responsables.id
                               LEFT JOIN diagnostico.temperaturas ON diagnostico.heladeras.id = diagnostico.temperaturas.heladera
                               INNER JOIN cce.laboratorios ON diagnostico.heladeras.laboratorio = cce.laboratorios.id;

-- eliminamos la vista de muestras si existe
DROP VIEW IF EXISTS v_muestras;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_muestras AS
    SELECT diagnostico.muestras.id_muestra AS idmuestra,
           diagnostico.muestras.id_protocolo AS id_protocolo,
           diagnostico.personas.protocolo AS protocolo,
           diagnostico.muestras.id_laboratorio AS idlaboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.muestras.id_genero AS idgenero,
           cce.responsables.usuario AS genero,
           DATE_FORMAT(diagnostico.muestras.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.muestras.id_tomo AS idtomo,
           extraccion.usuario AS tomo,
           DATE_FORMAT(diagnostico.muestras.fecha_toma, '%d/%m/%Y') AS fecha_toma,
           CONCAT(diagnostico.personas.apellido, ", ", diagnostico.personas.nombre) AS nombre,
           diagnostico.personas.documento AS documento,
           diccionarios.sexos.sexo AS sexo,
           diagnostico.muestras.comentarios AS comentarios
    FROM diagnostico.muestras INNER JOIN diagnostico.personas ON diagnostico.muestras.id_protocolo = diagnostico.personas.protocolo
                              INNER JOIN cce.laboratorios ON diagnostico.muestras.id_laboratorio = cce.laboratorios.id
                              INNER JOIN cce.responsables ON diagnostico.muestras.id_genero = cce.responsables.id
                              INNER JOIN diccionarios.sexos ON diagnostico.personas.sexo = diccionarios.sexos.id
                              LEFT JOIN cce.responsables AS extraccion ON diagnostico.muestras.id_tomo = extraccion.id;
