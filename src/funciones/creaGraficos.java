/*

    Nombre: creaGraficos
    Fecha: 02/05/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que aporta los métodos para implementar distintos
                 gráficos

 */

// definición del paquete
package funciones;

// importamos las librerías
import java.awt.image.BufferedImage;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

// definición de la clase
public class creaGraficos {
 
 /**
  * @param datos array con los datos 
  * @return un objeto imagen
  * Método que crea un gráfico de barras con los datos de participación
  * de los laboratorios, recibe como parámetro el array con la información
  */
 public BufferedImage graficoPanelesEnviadosProvinciales(String[][] datos){

  // definimos la fuente de datos
  DefaultCategoryDataset dataset = new DefaultCategoryDataset();
  
  // agregamos los laboratorios participantes
  dataset.setValue(Double.parseDouble(datos[0][0]), "Laboratorios", "");
  
  // agregamos los paneles enviados
  dataset.setValue(Double.parseDouble(datos[0][1]), "Muestras Enviadas", "");
  
  // agregamos los paneles procesados verificando que no este vacío 
  if (datos[0][2] != null){
   dataset.setValue(Double.parseDouble(datos[0][2]), "Muestras Procesadas", "");
  }
   
  // creamos el gráfico
     JFreeChart chart = ChartFactory.createBarChart3D("", 
                                                "",
                                                "N° Paneles", 
                                                dataset, 
                                                PlotOrientation.VERTICAL, 
                                                true,
                                                true, 
                                                false);

     // creamos el buffer de imagen
     BufferedImage Imagen = chart.createBufferedImage(400, 300);

     // retornamos la imagen
     return Imagen;
     
 }

 /**
  * @param datos vector con los datos 
  * @return un objeto imagen
  * Método que crea un gráfico de barras con los datos de participación
  * de los laboratorios, recibe como parámetro el array con la información
  */
 public BufferedImage graficoPanelesEnviados(String[][] datos){

  // definimos la fuente de datos
  DefaultCategoryDataset dataset = new DefaultCategoryDataset();
  
  // recorremos el vector
  for (int i = 0; i < datos.length -1; i++){

   // agregamos los paneles enviados
   dataset.setValue(Double.parseDouble(datos[i][2]), "Muestras Enviadas", datos[i][0]);
   
   // agregamos los paneles procesados verificando que no este vacío 
   if (datos[i][3] != null){
    dataset.setValue(Double.parseDouble(datos[i][3]), "Muestras Procesadas", datos[i][0]);
   }
    
  }

  // creamos el gráfico
     JFreeChart chart = ChartFactory.createBarChart3D("", 
                                                "",
                                                "", 
                                                dataset, 
                                                PlotOrientation.HORIZONTAL, 
                                                true,
                                                true, 
                                                false);
  
     // creamos el buffer de imagen
     BufferedImage Imagen = chart.createBufferedImage(450, 580);

     // retornamos la imagen
     return Imagen;
     
 }
 
 /**
  * @param datos vector con los datos a representar
  * @return un objeto imagen
  * Método que presenta el gráfico de barras con las determinaciones 
  * realizadas y las correctas por jurisdicción
  */
 public BufferedImage graficaConcordancia(String[][] datos){

  // definimos la fuente de datos
  DefaultCategoryDataset dataset = new DefaultCategoryDataset();
  
  // recorremos el array no hasta el final porque la última 
  // fila contiene los totales
  for (int i=0; i < datos.length -1; i++){
  
   // agregamos los paneles procesados
   dataset.setValue(Double.parseDouble(datos[i][1]), "Procesadas", datos[i][0]);
   
   // agregamos los paneles correctos verificando que no sea nulo
   if (datos[i][3] != null){
    dataset.setValue(Double.parseDouble(datos[i][2]), "Correctas", datos[i][0]);
   }
   
  }
  
  // creamos el gráfico
     JFreeChart chart = ChartFactory.createBarChart3D("", 
                                                "",
                                                "", 
                                                dataset, 
                                                PlotOrientation.HORIZONTAL, 
                                                true,
                                                true, 
                                                false);

     // creamos el buffer de imagen
     BufferedImage Imagen = chart.createBufferedImage(450, 580);

     // retornamos la imagen
     return Imagen;
  
 }
 
 /**
  * @param Datos un vector con los datos a graficar
  * @return un objeto imagen
  * Método que recibe como parámetro el vector con el análisis histórico
  * y crea un gráfico de líneas (asume que en la primera columna se 
  * encuentra el título de la serie y en la tercera columna la 
  * eficiencia)
  */
 public BufferedImage graficaHistoria(String[][] Datos){
  
  // creamos el objeto
  DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
  
  // recorremos el vector agregando las series
  for (int i = 0; i < Datos.length; i++){
   
   // lo agregamos al dataset
   dataset.addValue(Double.parseDouble(Datos[i][3]), "Concordancia", Datos[i][0]);
      
  }

  // creamos el gráfico
     JFreeChart chart = ChartFactory.createLineChart("", 
                                               "",
                                               "", 
                                               dataset, 
                                               PlotOrientation.VERTICAL,
                                               true,
                                               true, 
                                               false);

     // creamos el buffer de imagen
     BufferedImage Imagen = chart.createBufferedImage(400, 300);

     // retornamos la imagen
     return Imagen;
  
 }
 
 /** 
  * @param datos array con los datos de los desvíos
  * @return un objeto imagen
  * Método que recibe como parámetro la tabla de eficiencia por jurisdicción 
  * y retorna un gráfico de líneas con la eficiencia de cada una
  */
 public BufferedImage graficaDesvios(String[][] datos){

  // creamos el objeto
  DefaultCategoryDataset dataset = new DefaultCategoryDataset( );
  
  // recorremos el vector agregando las series menos la última
  // fila que es la que tiene los totales
  for (int i = 0; i < datos.length -1; i++){
   
   // lo agregamos al dataset
   dataset.addValue(Double.parseDouble(datos[i][3]), "Concordancia", datos[i][0]);
      
  }

  // creamos el gráfico
     JFreeChart chart = ChartFactory.createLineChart("", 
                                               "",
                                               "", 
                                               dataset, 
                                               PlotOrientation.HORIZONTAL,
                                               true,
                                               true, 
                                               false);

     // creamos el buffer de imagen
     BufferedImage Imagen = chart.createBufferedImage(450, 580);

     // retornamos la imagen
     return Imagen;
  
 }
 
 /**
  * @param datos vector con los datos a presentar
  * @return imagen con el gráfico de barras
  * Método que recibe como parámetro un array con los datos de eficiencia
  * por técnica, presenta un gráfico de barras con los valores de cada una
  */
 public BufferedImage graficaConcordanciaTecnica(String[][] datos){
 
  // definimos la fuente de datos
  DefaultCategoryDataset dataset = new DefaultCategoryDataset();
  
  // recorremos el array
  for (int i=0; i < datos.length; i++){
  
   // agregamos los valores de técnica y concordancia
   // verificando que no sean nulos
   if (datos[i][1] != null){
    dataset.setValue(Double.parseDouble(datos[i][1]), "Concordancia", datos[i][0]);
   }
      
  }
  
  // creamos el gráfico
     JFreeChart chart = ChartFactory.createBarChart3D("", 
                                                "",
                                                "", 
                                                dataset, 
                                                PlotOrientation.VERTICAL, 
                                                true,
                                                true, 
                                                false);

     // creamos el buffer de imagen
     BufferedImage Imagen = chart.createBufferedImage(450, 580);

     // retornamos la imagen
     return Imagen;
  
 }
 
 /**
  * @param Datos vector con los datos a presentar
  * @param Porcentaje boolean, si calcula los porcentajes
  * @return un objeto imagen
  * Método que recibe los datos en una matriz asumiendo que el primero
  * de los items es el label del sector y el segundo el valor, si 
  * recibe como segundo parámetro un valor verdadero, a los label
  * agrega el porcentaje
  */
 public BufferedImage graficoTorta(String[][] Datos, boolean Porcentaje){

  // definimos las variables
  int Sumatoria = 0;
  Utilidades Herramientas;
  Estadisticas Calculos = new Estadisticas();
  
  // instanciamos la clase de utilidades
  Herramientas = new Utilidades();
  
  // definimos el dataset para gráfico 3D
  DefaultPieDataset dataset = new DefaultPieDataset();
  
  // si tiene que calcular los porcentajes
  if (Porcentaje == true){
  
   // recorremos el vector calculando la sumatoria verificando que no
   // sea nulo el valor
   for (int i=0; i < Datos.length; i++){
  
    // verificamos que exista
    if (Integer.parseInt(Datos[i][1]) != 0){
     
     // sumamos el argumento
     Sumatoria += Integer.parseInt(Datos[i][1]);
    
    }
    
   }
   
  }
  
  // ahora recorremos la matriz 
  for (int i=0; i < Datos.length; i++){
   
   // si el valor existe y no es nulo
   if (Double.parseDouble(Datos[i][1]) != 0){

    // si tiene que presentar los porcentajes
    if (Porcentaje == true){
     
     // calcula el porcentaje
     String Valor = Calculos.Porcentaje(Sumatoria, Integer.parseInt(Datos[i][1]));
     String Titulo = Datos[i][0] + " " + Valor;
       
     // lo asignamos al dataset
     dataset.setValue(Titulo, Double.parseDouble(Datos[i][1]));
     
    // si no quiere presentar porcentajes
    } else {
     
     // lo asigmanos al dataset
     dataset.setValue(Datos[i][0], Double.parseDouble(Datos[i][1]));

    }
    
   }
   
  }

  // creamos el gráfico
  JFreeChart chart = ChartFactory.createPieChart(null, dataset, true, true, false);

  // creamos el buffer de imagen pasándole el ancho y el alto 
  // como parámetros
  BufferedImage Imagen = chart.createBufferedImage(400, 300);

  // retornamos la imagen
  return Imagen;
     
 }
  
}
