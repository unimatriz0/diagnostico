/*

    Nombre: Info
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase sencilla que presenta un cuadro de diálogo con 
                 información del sistema

 */

// definición del paquete
package funciones;

// importación de librerías
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JOptionPane;

/**
 * @author Lic. Claudio Invernizzi
 */

// definición de la clase
public class Info {
    
    // declaración de variables
    protected String nombre_equipo;
    protected String usuario;
    protected String procesador;
    protected String so;
    protected String texto;
    
    // constructor de la clase
    public Info(){

        // obtenemos los valores
        nombre_equipo = System.getenv("COMPUTERNAME");
        usuario = System.getProperty("user.name");
        procesador = System.getenv("PROCESSOR_IDENTIFIER");
        so = System.getProperty("os.name");
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int sr = Toolkit.getDefaultToolkit().getScreenResolution();

        // ahora armamos el texto
        texto = "Sistema de Gestión de Pasantías\n"
              + "Instituto Nacional de Parasitología\n"
              + "Usuario: " + usuario + "\n"
              + "Procesador: " + procesador + "\n"
              + "Sistema Operativo: " + so + "\n"
              + "Pantalla: " + d.getWidth() + "x" + d.getHeight() + "\n"
              + "Resolución: " + sr + "dpi.\n"
              + "Autor: Lic. Claudio Invernizzi";
        
        // presentamos el mensaje
        JOptionPane.showMessageDialog(null, texto, "Información del Sistema",JOptionPane.INFORMATION_MESSAGE);
        
    }
    
}
