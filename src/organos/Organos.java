/*

    Nombre: Organos
    Fecha: 18/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que contiene los métodos para operar sobre la
                 tabla de organos transplantados

*/

// definición del paquete
package organos;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definiciòn de la clase
 *
 */
public class Organos {

    // declaraciòn de variables
    protected int IdOrgano;                   // clave del registro
    protected String Organo;                  // nombre del organo
    protected String Usuario;                 // nombre del usuario
    protected String FechaAlta;               // fecha de alta del registro

    // el puntero a la base de datos
    protected Conexion Enlace;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Organos(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initOrganos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initOrganos(){

        // inicializamos las variables
        this.IdOrgano = 0;
        this.Organo = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setIdOrgano(int idorgano){
        this.IdOrgano = idorgano;
    }
    public void setOrgano(String organo){
        this.Organo = organo;
    }

    // métodos de retorno de valores
    public int getIdOrgano(){
        return this.IdOrgano;
    }
    public String getOrgano(){
        return this.Organo;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param organo - nombre del organo
     * @return clave - entero con la clave del registro
     * Mètodo que recibe como paràmetro el nombre de un organo
     * y retorna la clave del registro
     */
    public int getClaveOrgano(String organo){

        // declaración de variables
        int claveOrgano = 0;
        String Consulta;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.organos.id AS clave " +
                   "FROM diagnostico.organos " +
                   "WHERE diagnostico.organos.organo = '" + organo + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            if (Resultado.next()){
                claveOrgano = Resultado.getInt("clave");
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos la clave
        return claveOrgano;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un órgano
     * y retorna el nombre del mismo
     */
    public String getNombreOrgano(int clave){

        // declaración de variables
        String Organo = "";
        String Consulta;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.organos.organo AS organo " +
                   "FROM diagnostico.organos " +
                   "WHERE diagnostico.organos.id = '" + clave + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            if (Resultado.next()){
                Organo = Resultado.getString("organo");
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos la clave
        return Organo;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return nomina - resultset con el listado de organos
     * Mètodo que retorna la nòmina de organos registrados
     * en el sistema
     */
    public ResultSet nominaOrganos(){

        // declaración de variables
        String Consulta;
        ResultSet listaOrganos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.organos.id AS id, " +
                   "       diagnostico.organos.organo AS organo " +
                   "FROM diagnostico.organos " +
                   "ORDER BY diagnostico.organos.organo;";
        listaOrganos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return listaOrganos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return IdEnfermedad - clave del registro
     * Mètodo que ejecuta la consulta de ediciòn o inserciòn
     * segùn corresponda y retorna la id del registro
     */
    public int grabaOrgano(){

        // si està insertando
        if (this.IdOrgano == 0){
            this.nuevoOrgano();
        } else {
            this.editaOrgano();
        }

        // retorna la id
        return this.IdOrgano;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo protegido que ejecuta la consulta de inserciòn
     */
    protected void nuevoOrgano(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "INSERT INTO diagnostico.organos "
                 + "       (organo, "
                 + "        id_usuario) "
                 + "       VALUES "
                 + "       (?,?); ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString (1, this.Organo);
            preparedStmt.setInt    (2, Seguridad.Id);

            // ejecutamos la consulta
            preparedStmt.execute();

            // obtenemos la id
            this.IdOrgano = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de ediciòn
     */
    protected void editaOrgano(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "UPDATE diagnostico.organos SET "
                 + "       organo = ?, "
                 + "       id_usuario = ? "
                 + "WHERE diagnostico.organos.id = ?; ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString (1, this.Organo);
            preparedStmt.setInt    (2, Seguridad.Id);
            preparedStmt.setInt    (3, this.IdOrgano);

            // ejecutamos la consulta
            preparedStmt.execute();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param organo - nombre a verificar
     * @return Correcto - boolean true si puede insertar
     * Método pùblico que verifica no se encuentre declarada
     * una enfemredad, si el usuario puede insertar retorna
     * verdadero
     */
    public boolean validaOrgano(String organo){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.organos.id) AS registros "
                 + "FROM diagnostico.organos "
                 + "WHERE diagnostico.organos.organo = '" + organo + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave del registro
     * ejecuta la consulta y asigna los valores en las variables
     * de clase
     */
    public void getDatosOrgano(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.organos.id AS id, " +
                   "       diagnostico.organos.organo AS organo, " +
                   "       cce.responsables.usuario AS usuario, " +
                   "       DATE_FORMAT(diagnostico.organos.fecha, '%d/%m/%Y') AS fecha " +
                   "FROM diagnostico.organos INNER JOIN cce.responsables ON diagnostico.organos.id_usuario = cce.responsables.id;";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos en la variable de clase
            this.IdOrgano = Resultado.getInt("id");
            this.Organo = Resultado.getString("organo");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * @return boolean - resultado de la operación
     * Método utilizado para verificar que un órgano no tenga
     * hijos (pacientes asignados) antes de eliminar el registro
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.transplantes.id) AS registros "
                 + "FROM diagnostico.transplantes "
                 + "WHERE diagnostico.transplantes.organo = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que ejecuta la consulta de eliminación en la
     * tabla de órganos
     */
    public void borraOrgano(int clave){

        // definición de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.organos " +
                   "WHERE diagnostico.organos.id = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }
}
