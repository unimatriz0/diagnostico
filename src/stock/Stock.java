/*
 * Nombre: stock
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 31/07/2017
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnóstico
 * Comentarios: Clase que controla las operaciones de la base de datos de
 *              control de stock
 */

// definición del paquete
package stock;

// inclusión de librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Blob;
import java.awt.Image;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import dbApi.Conexion;
import seguridad.Seguridad;
import java.util.logging.Level;
import java.util.logging.Logger;

// definición de la clase
public class Stock {

    // definición de variables de clase
    protected Conexion Enlace;

    // de la tabla de items
    protected int IdItem;             // clave del item
    protected int IdLabItem;          // clave del laboratorio
    protected String Item;            // descripción del item
    protected float Valor;            // valor del item
    protected String CodigoSop;       // código sop del item
    protected FileInputStream Imagen; // foto del item
    protected int LongImagen;         // tamaño de la imagen
    protected int Critico;            // número crítico del stock
    protected Image Foto;             // imagen almacenada en la base
    protected String FechaAlta;       // fecha de alta del item
    protected String Usuario;         // nombre del usuario

    // constructor de la clase
    public Stock(){

        // nos conectamos a la base
        this.Enlace = new Conexion();

        // inicializamos las variables de los items
        this.IdItem = 0;
        this.IdLabItem = 0;
        this.Item = "";
        this.Valor = 0;
        this.CodigoSop = "";
        this.Imagen = null;
        this.LongImagen = 0;
        this.Foto = null;
        this.Critico = 0;
        this.FechaAlta = "";
        this.Usuario = "";

    }

    // métodos de asignación de variables

    // del diccionario de items
    public void setIdItem(int iditem){
        this.IdItem = iditem;
    }
    public void setIdLabItem(int idlaboratorio){
        this.IdLabItem = idlaboratorio;
    }
    public void setItem(String item){
        this.Item = item;
    }
    public void setValor(float valor){
        this.Valor = valor;
    }
    public void setCodigoSop(String codigosop){
        this.CodigoSop = codigosop;
    }
    public void setImagen(FileInputStream imagen){
        this.Imagen = imagen;
    }
    public void setLongitud(int longitud){
        this.LongImagen = longitud;
    }
    public void setCritico(int critico){
        this.Critico = critico;
    }

    // métodos de obtención de variables

    // del diccionario de items
    public int getIdItem(){
        return this.IdItem;
    }
    public int getIdLabItem(){
        return this.IdLabItem;
    }
    public String getItem(){
        return this.Item;
    }
    public Image getImagen(){
        return this.Foto;
    }
    public float getValor(){
        return this.Valor;
    }
    public String getCodigoSop(){
        return this.CodigoSop;
    }
    public int getCritico(){
        return this.Critico;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getUsuario(){
        return this.Usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param descripcion string con el nombre de un item
     * @return boolean
     * Método que recibe como parámetro la descripción de un item
     * y retorna verdadero si existe, usado para evitar el ingreso
     * de items duplicados
     */
    public boolean verificaDescripcion(String descripcion){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(*) AS registros " +
                   "FROM diagnostico.items " +
                   "WHERE diagnostico.items.descripcion = '" + descripcion + "' AND " +
                   "      diagnostico.items.id_laboratorio = '" + Seguridad.Laboratorio + "';";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // si hubo registros
            if(Resultado.getInt("registros") == 0){
                return false;
            } else {
                return true;
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
            return true;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iditem clave del registro
     * Método que recibe como parámetro la clave de un item y asigna en las
     * variables de clase los valores de la base
     */
    public void getDatosItem(int iditem){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Blob Archivo;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.items.id AS iditem, " +
                   "       diagnostico.items.id_laboratorio AS id_laboratorio, " +
                   "       diagnostico.items.descripcion AS descripcion, " +
                   "       diagnostico.items.valor AS valor, " +
                   "       diagnostico.items.codigosop AS codigosop, " +
                   "       diagnostico.items.imagen AS imagen, " +
                   "       diagnostico.items.critico AS critico, " +
                   "       DATE_FORMAT(diagnostico.items.fecha, '%d/%m/%Y') AS fecha_alta, " +
                   "       cce.responsables.usuario AS usuario " +
                   "FROM diagnostico.items INNER JOIN cce.responsables ON diagnostico.items.id_usuario = cce.responsables.id " +
                   "WHERE diagnostico.items.id = '" + iditem + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos a los valores de clase
            this.IdItem = Resultado.getInt("iditem");
            this.IdLabItem = Resultado.getInt("id_laboratorio");
            this.Item = Resultado.getString("descripcion");
            this.Valor = Resultado.getFloat("valor");
            this.CodigoSop = Resultado.getString("codigosop");
            this.Critico = Resultado.getInt("critico");
            this.FechaAlta = Resultado.getString("fecha_alta");
            this.Usuario = Resultado.getString("usuario");

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("imagen") != null){
                Archivo = Resultado.getBlob("imagen");
                this.Foto = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            }

        // si no hay registros o hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del item
     * Método que graba en la base de datos los valores de las variables de
     * clase del item actual, retorna la clave del registro afectado
     */
    public int grabaItem(){

        // si está dando altas
        if (this.IdItem == 0){
            this.insertaItem();
        } else {
            this.editaItem();
        }

        /*
         * si el usuario seleccionó una imagen la actualizamos independientemente
         * esto lo hacemos así porque en una edición puede no haber seleccionado
         * una imagen y así evitamos pisarla con una cadena nula, o también en
         * un alta no es un campo obligatorio
         */
        if (this.Imagen != null){
            this.grabaImagen();
        }

        // retornamos la id
        return this.IdItem;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que actualiza la imagen en la base
     */
    protected void grabaImagen(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // actualizamos en el registro
        Consulta = "UPDATE diagnostico.items SET " +
                   "       imagen = ? " +
                   "WHERE diagnostico.items.id = ?;";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setBinaryStream(1,  this.Imagen, this.LongImagen);
            psInsertar.setInt(2, this.IdItem);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inserta en la base de datos un nuevo item
     */
    protected void insertaItem(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta de inserción
        Consulta = "INSERT INTO diagnostico.items " +
                   "            (id_laboratorio, " +
                   "             descripcion, " +
                   "             valor, " +
                   "             codigosop, " +
                   "             critico, " +
                   "             id_usuario) " +
                   "             VALUES " +
                   "            (?, ?, ?, ?, ?, ?);";

        try {

            // asignamos la consulta a la conexión
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, Seguridad.Laboratorio);
            psInsertar.setString(2, this.Item);
            psInsertar.setFloat(3, this.Valor);
            psInsertar.setString(4, this.CodigoSop);
            psInsertar.setInt(5, this.Critico);
            psInsertar.setInt(6, Seguridad.Id);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

            // obtenemos la id del registro
            this.IdItem = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de actualización de un item
     */
    protected void editaItem(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta de edición
        Consulta = "UPDATE diagnostico.items SET " +
                   "       id_laboratorio = ?, " +
                   "       descripcion = ?, " +
                   "       valor = ?, " +
                   "       codigosop = ?, " +
                   "       critico = ?, " +
                   "       id_usuario = ? " +
                   "WHERE diagnostico.items.id = ?;";

        try {

            // asignamos la consulta a la conexión
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, Seguridad.Laboratorio);
            psInsertar.setString(2, this.Item);
            psInsertar.setFloat(3, this.Valor);
            psInsertar.setString(4, this.CodigoSop);
            psInsertar.setInt(5, this.Critico);
            psInsertar.setInt(6, Seguridad.Id);
            psInsertar.setInt(7, this.IdItem);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con los items del diccionario
     * Método que retorna la nómina de items
     */
    public ResultSet getNominaItems(){

        // declaración de variables
        ResultSet Nomina;
        String Consulta;

        // compone la consulta
        Consulta = "SELECT diagnostico.items.id AS iditem, " +
                   "       diagnostico.items.descripcion AS descripcion, " +
                   "       diagnostico.items.valor AS valor, " +
                   "       diagnostico.items.codigosop AS codigosop, " +
                   "       diagnostico.items.critico AS critico, " +
                   "       cce.responsables.usuario AS usuario " +
                   "FROM diagnostico.items INNER JOIN cce.responsables ON diagnostico.items.id_usuario = cce.responsables.id " +
                   "WHERE diagnostico.items.id_laboratorio = '" + Seguridad.Laboratorio + "' " +
                   "ORDER BY diagnostico.items.descripcion; ";
        Nomina = this.Enlace.Consultar(Consulta);

        // retornamos la nómina
        return Nomina;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param item string con parte del nombre
     * @return vector con la nómina de items encontrados
     * Método que recibe como parámetro parte del nombre de un item,
     * retorna el vector con los items coincidentes para ese
     * laboratorio
     */
    public ResultSet buscaItems(String item){

        // declaración de variables
        ResultSet Nomina;
        String Consulta;

        // compone la consulta
        Consulta = "SELECT diagnostico.items.id AS iditem, " +
                   "       diagnostico.items.descripcion AS descripcion, " +
                   "       diagnostico.items.valor AS valor, " +
                   "       diagnostico.items.critico AS critico, " +
                   "       cce.responsables.usuario AS usuario " +
                   "FROM diagnostico.items INNER JOIN cce.responsables ON diagnostico.items.id_usuario = cce.responsables.id " +
                   "WHERE diagnostico.items.id_laboratorio = '" + Seguridad.Laboratorio + "' AND " +
                   "      diagnostico.items.descripcion LIKE '%" + item + "%' " +
                   "ORDER BY diagnostico.items.descripcion; ";
        Nomina = this.Enlace.Consultar(Consulta);

        // retornamos la nómina
        return Nomina;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param item
     * @return clave
     * Método que recibe como parámetro la cadena con la descripción de
     * un item, retorna la clave de ese item para el laboratorio activo
     */
    public int getClaveItem(String item){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int clave = 0;

        // componemos la consulta
        Consulta = "SELECT diagnostico.items.id AS iditem " +
                   "FROM diagnostico.items " +
                   "WHERE diagnostico.items.descripcion = '" + item + "'; ";

        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // si hay registros
            if (Resultado.next()){
                clave = Resultado.getInt("iditem");
            } else {
                clave = 0;
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos la clave
        return clave;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la existencia
     * Método que retorna el estado actual del inventario, aquí siempre devolvemos el
     * resultado completo de la base
     */
    public ResultSet getInventario(){

        // declaración de variables
        String Consulta;
        ResultSet Inventario;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_inventario.iditem AS iditem, " +
                   "       diagnostico.v_inventario.descripcion AS item, " +
        		   "       diagnostico.v_inventario.codigosop AS codigosop, " +
                   "       diagnostico.v_inventario.critico AS critico, " +
                   "       diagnostico.v_inventario.existencia AS existencia, " +
                   "       diagnostico.v_inventario.importe AS importe " +
                   "FROM diagnostico.v_inventario " +
                   "WHERE diagnostico.v_inventario.idlaboratorio = '" + Seguridad.Laboratorio + "' " +
                   "ORDER BY diagnostico.v_inventario.descripcion; ";
        Inventario = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Inventario;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iditem clave con el item
     * @return entero con la cantidad disponible
     * Método que recibe como parámetro la id de un item y
     * retorna el número de elementos existentes en el depósito
     * para el laboratorio del usuario actual
     */
    public int getDisponible(int iditem){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int Elementos = 0;

        // componemos la consulta
        Consulta = "SELECT diagnostico.inventario.existencia AS disponible " +
                   "FROM diagnostico.inventario " +
                   "WHERE diagnostico.inventario.item = '" + iditem + "' AND " +
                   "      diagnostico.inventario.id_laboratorio = '" + Seguridad.Laboratorio + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            Elementos = Resultado.getInt("disponible");

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos el disponible
        return Elementos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param item la clave de un item
     * @param cantidad la cantidad a entregar
     * @return si hay stock
     * Método que recibe como parámetros la clave de un item y
     * la cantidad, verifica si existen unidades suficientes
     * en stock para realizar la entrega
     */
    public boolean verificaExistentes(int item, int cantidad){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        boolean existentes = false;

        // componemos la consulta
        Consulta = "SELECT diagnostico.inventario.existencia AS inventario " +
                   "FROM diagnostico.inventario " +
                   "WHERE diagnostico.inventario.item = '" + item + "' AND " +
                   "      diagnostico.inventario.id_laboratorio = '" + Seguridad.Laboratorio + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // si hay registros
            if (Resultado.next()){

                // verifica la existencia
                if (Resultado.getInt("inventario") < cantidad){
                    existentes = false;
                } else {
                    existentes = true;
                }

            // si no encontró
            } else {
                existentes = false;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }
        // retornamos
        return existentes;

    }

}
