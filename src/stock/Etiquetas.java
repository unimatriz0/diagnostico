/*

    Nombre: etiquetas
    Fecha: 18/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que genera la etiqueda del item

 */

// definición del paquete
package stock;

//importamos las librerías
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.*;
import seguridad.Seguridad;

// definición de la clase
public class Etiquetas {

	// declaración de variables de clase
	protected int IdItem;                // clave del registro
	protected String Descripcion;        // descripcion del item
	protected String CodigoSop;          // código sop del elemento
    protected int Critico;               // número crítico del stock
    protected Image Foto;                // imagen almacenada en la base
    protected String FechaAlta;          // fecha de alta del item
    protected String Usuario;            // nombre del usuario
	protected Document documento;        // el documento pdf
    protected PdfWriter docWriter;       // el puntero del documento
    protected PdfContentByte cb;         // puntero al contenido

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param iditem
	 * Constructor de la clase, recibe como parámetro la
	 * clave de la heladera a imprimir
	 */
	public Etiquetas (int iditem){

		// instanciamos la clase y obtenemos los datos del registro
        Stock Item = new Stock();
        Item.getDatosItem(iditem);

        // asignamos en las variables de clase
        this.IdItem = Item.getIdItem();
        this.Descripcion = Item.getItem();
        this.CodigoSop = Item.getCodigoSop();
        this.Critico = Item.getCritico();
        // this.Foto = Item.getImagen();
        this.FechaAlta = Item.getFechaAlta();
        this.Usuario = Item.getUsuario();

        // creamos el documento
        this.documento = new Document(PageSize.A4);

        // creamos el archivo
        try {
            this.docWriter = PdfWriter.getInstance(this.documento, new FileOutputStream("etiqueta.pdf"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // establecemos las propiedades del documento
        this.documento.addAuthor("Lic. Claudio Invernizzi");
        this.documento.addCreationDate();
        this.documento.addCreator("http://fatalachaben.info.tm");

        // los márgenes van left - right - top - bottom en puntos
        // 72 puntos es 1 pulgada 2,5 cm (superior lo dejamos en
        // 10 porque el encabezado de página ocupa espacio
        this.documento.setMargins(90, 36, 10, 72);

        // abrimos el documento
        this.documento.open();

        // imprimimos el registro
        try {
			this.imprimeFicha();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

        // Cerramos el documento
        this.documento.close();

        // lo enviamos a la impresora directamente
        try {
            File path = new File ("etiqueta.pdf");
            Desktop.getDesktop().print(path);
        }catch (IOException ex) {
            ex.printStackTrace();
        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @throws DocumentException
	 * Método que imprime los datos de la ficha del item
	 */
	protected void imprimeFicha() throws DocumentException{

		// presenta el encabezado
		this.encabezado();

        // agregamos el título
        Paragraph Parrafo = new Paragraph("Registro de Artículos",
                                FontFactory.getFont("arial", 14));
                                Parrafo.setAlignment(Element.ALIGN_CENTER);
        this.documento.add(Parrafo);

        // la clave de la heladera
        Parrafo = new Paragraph("ID: " + Integer.toString(this.IdItem),
                      FontFactory.getFont("arial", 12));
                      Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la marca
        Parrafo = new Paragraph("Descripcion: " + this.Descripcion,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la ubicación
        Parrafo = new Paragraph("CodigoSop: " + this.CodigoSop,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la primer temperatura
        Parrafo = new Paragraph("Critico: " + Integer.toString(this.Critico),
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // el usuario y la fecha de alta
        Parrafo = new Paragraph("Usuario: " + this.Usuario,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que define una tabla en la que presenta alineados
	 * el logo y los códigos de barras
	 */
	protected void encabezado(){

        // insertamos la tabla para los logos
        PdfPTable Tabla = new PdfPTable(3);

        // fijamos el ancho de la tabla y de las columnas
        try {
            Tabla.setWidths(new int[]{92, 150, 191});
            Tabla.setTotalWidth(580);
            Tabla.setLockedWidth(true);

        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // agregamos la primer columna
        PdfPCell Celda = new PdfPCell();
        Celda.setVerticalAlignment(Element.ALIGN_CENTER);
        Celda.setBorder(0);

        try {

            // obtenemos la imagen y la escalamos
            Image logoFatala = Image.getInstance("Graficos/logo_fatala.jpg");
            logoFatala.scaleToFit(92f, 67f);
            logoFatala.setAlignment(Chunk.ALIGN_MIDDLE);
            Celda.addElement(logoFatala);

        } catch (BadElementException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // agregamos la celda a la tabla
        Tabla.addCell(Celda);

		// instanciamos el puntero al documento
		PdfContentByte cb = this.docWriter.getDirectContent();
		Barcode39 barcode39 = new Barcode39();
		String clave = Integer.toString(this.IdItem) + "-" + Integer.toString(Seguridad.Laboratorio);

		// creamos el código y lo convertimos a imagen
		barcode39.setCode(clave);
		Image code39Image = barcode39.createImageWithBarcode(cb, null, null);

        Celda = new PdfPCell();
        Celda.setVerticalAlignment(Element.ALIGN_CENTER);
        Celda.setBorder(0);
        Celda.addElement(code39Image);
        Tabla.addCell(Celda);

        // ahora agregamos la tercer columna
        Celda = new PdfPCell();
        Celda.setVerticalAlignment(Element.ALIGN_CENTER);
        Celda.setBorder(0);

		// inicializamos las variables
		Image codeQrImage = null;
		BarcodeQRCode barcodeQRCode = new BarcodeQRCode(clave, 1000, 1000, null);

		// creamos la imagen
		try {
			codeQrImage = barcodeQRCode.getImage();
		} catch (BadElementException e) {
			e.printStackTrace();
		}

		// escalamos la imagen y la agregamos al documento
		codeQrImage.scaleAbsolute(100, 100);

        Celda.addElement(codeQrImage);

        // agregamos la celda a la tabla
        Tabla.addCell(Celda);

        try {
            // agregamos la tabla
            this.documento.add(Tabla);
        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

	}

}
