/*
 * Nombre: remitoIngreso
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 11/12/2017
 * Mail: cinvernizzi@gmail.com
 * Proyecto: Diagnóstico
 * Licencia: GPL
 * Comentarios: Clase que recibe como parámetro en el constructor el id
 *              del ingreso y genera el remito en formato pdf
 */

// definición del paquete
package stock;

// inclusión de librerías
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import funciones.Utilidades;
import seguridad.Seguridad;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

// definición del paquete
public class RemitoIngreso {
    
    // declaración de variables
    String Archivo;
    String Descripcion;
    int IdIngreso;
    Float Importe;
    String Laboratorio;
    String Remito;
    int Cantidad;
    String Vencimiento;
    String FechaIngreso;
    String Usuario;
    String Financiamiento;
    String Lote;
    String Ubicacion;
    String Comentarios;

    // las variables del pdf
    protected Document documento;            // el documento pdf
    protected FileOutputStream ficheroPdf;   // el archivo físico
    protected PdfWriter docWriter;           // el puntero del documento
    protected PdfContentByte cb;             // puntero al contenido
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idingreso
     * Constructor del paquete
     */
    public RemitoIngreso(int idingreso){
        
        // obtenemos los datos del remito
        this.datosRemito(idingreso);
        
        // obtenemos el archivo
        this.obtenerArchivo();
        
    }
    
    /**
     * @param idingreso
     * Método protegido que recibe como parámetro la id del ingreso
     * y asigna en las variables de clase los datos del remito
     */
    protected void datosRemito(int idingreso){
        
        // instanciamos la clase
        Ingresos Deposito = new Ingresos();
        Deposito.getDatosIngreso(idingreso);
        
        // asignamos en las variables de clase
        this.Cantidad = Deposito.getCantidad();
        this.Comentarios = Deposito.getComentarios();
        this.Descripcion = Deposito.getItem();
        this.FechaIngreso = Deposito.getFechaIngreso();
        this.Vencimiento = Deposito.getVencimiento();
        this.IdIngreso = Deposito.getIdIngreso();
        this.Importe = Deposito.getImporte();
        this.Laboratorio = Deposito.getLaboratorio();
        this.Remito = Deposito.getRemito();
        this.Financiamiento = Deposito.getFinanciamiento();
        this.Lote = Deposito.getLote();
        this.Ubicacion = Deposito.getUbicacion();
        this.Usuario = Deposito.getUsuario();

    }
    
    /**
     * Método protegido que obtiene el nombre del archivo a grabar
     */
    protected void obtenerArchivo(){

        // obtenemos el nombre del archivo a grabar
        Utilidades Herramientas = new Utilidades();
        this.Archivo = Herramientas.grabaArchivo();
        
        // si canceló o hubo un error
        if (this.Archivo == null){
            
            // simplemente retornamos
            return;
        
        // si tenemos el nombre del archivo
        } else {
            
            // generamos el documento
            this.generarRemito();
            
        }
        
    }
    
    /**
     * Método protegido que a partir de las variables de clase
     * genera el remito de ingreso
     */
    protected void generarRemito(){

        // creamos el documento
        this.documento = new Document(PageSize.A4);

        // creamos el archivo
        try {
            this.docWriter = PdfWriter.getInstance(this.documento, new FileOutputStream(this.Archivo));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RemitoIngreso.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(RemitoIngreso.class.getName()).log(Level.SEVERE, null, ex);
        }

        // establecemos las propiedades del documento
        this.documento.addAuthor("Lic. Claudio Invernizzi");
        this.documento.addCreationDate();
        this.documento.addCreator("http://fatalachaben.info.tm");

        // los márgenes van left - right - top - bottom en puntos
        // 72 puntos es 1 pulgada 2,5 cm (superior lo dejamos en 
        // 10 porque el encabezado de página ocupa espacio
        this.documento.setMargins(90, 36, 10, 72);

        // abrimos el documento
        documento.open();            

        // imprimimos el encabezado
        this.encabezado();
        
        // agregamos el título
        Paragraph Parrafo = new Paragraph("Ingreso de Elementos al Depósito",
                                          FontFactory.getFont("arial", 14));
        Parrafo.setAlignment(Element.ALIGN_CENTER);
        try {
            this.documento.add(Parrafo);
        } catch (DocumentException ex) {
            Logger.getLogger(RemitoIngreso.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // ahora agregamos los datos del remito
        try {
			this.imprimirCuerpo();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
        // cerramos el documento
        this.documento.close();
        
    }
 
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @throws DocumentException
     * Método que imprime los datos del remito de ingreso
     */
    protected void imprimirCuerpo() throws DocumentException{
    	
    	// presenta el número de remito
        Paragraph Parrafo = new Paragraph("Remito Nro: " + this.Remito,
                                          FontFactory.getFont("arial", 12));
        Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);
        
    	// presenta la descripción del item
        Parrafo = new Paragraph("Descripción: " + this.Descripcion,
				                FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta la cantidad ingresada
        Parrafo = new Paragraph("Cantidad: " + Integer.toString(this.Cantidad),
     		                	FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta el importe
        Parrafo = new Paragraph("Importe: " + Float.toString(this.Importe),
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta la fecha de ingreso
        Parrafo = new Paragraph("Fecha Entrada: " + this.FechaIngreso,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta la fecha de vencimiento
        Parrafo = new Paragraph("Vencimiento: " + this.Vencimiento,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta la fuente de financiamiento
        Parrafo = new Paragraph("Financiamiento: " + this.Financiamiento,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta el número de lote
        Parrafo = new Paragraph("Lote: " + this.Lote,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta la ubicación
        Parrafo = new Paragraph("Ubicación: " + this.Ubicacion,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    	// presenta los comentarios
        Parrafo = new Paragraph(this.Comentarios,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
    	
    }
    
	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que define una tabla en la que presenta alineados
	 * el logo y los códigos de barras
	 */
	protected void encabezado(){

        // insertamos la tabla para los logos
        PdfPTable Tabla = new PdfPTable(3);

        // fijamos el ancho de la tabla y de las columnas
        try {
            Tabla.setWidths(new int[]{92, 150, 191});
            Tabla.setTotalWidth(580);
            Tabla.setLockedWidth(true);

        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // agregamos la primer columna
        PdfPCell Celda = new PdfPCell();
        Celda.setVerticalAlignment(Element.ALIGN_CENTER);
        Celda.setBorder(0);

        try {

            // obtenemos la imagen y la escalamos
            Image logoFatala = Image.getInstance("Graficos/logo_fatala.jpg");
            logoFatala.scaleToFit(92f, 67f);
            logoFatala.setAlignment(Chunk.ALIGN_MIDDLE);
            Celda.addElement(logoFatala);

        } catch (BadElementException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // agregamos la celda a la tabla
        Tabla.addCell(Celda);

		// instanciamos el puntero al documento
		PdfContentByte cb = this.docWriter.getDirectContent();
		Barcode39 barcode39 = new Barcode39();
		String clave = Integer.toString(this.IdIngreso) + "-" + Integer.toString(Seguridad.Laboratorio);

		// creamos el código y lo convertimos a imagen
		barcode39.setCode(clave);
		Image code39Image = barcode39.createImageWithBarcode(cb, null, null);

        Celda = new PdfPCell();
        Celda.setVerticalAlignment(Element.ALIGN_CENTER);
        Celda.setBorder(0);
        Celda.addElement(code39Image);
        Tabla.addCell(Celda);

        // ahora agregamos la tercer columna
        Celda = new PdfPCell();
        Celda.setVerticalAlignment(Element.ALIGN_CENTER);
        Celda.setBorder(0);

		// inicializamos las variables
		Image codeQrImage = null;
		BarcodeQRCode barcodeQRCode = new BarcodeQRCode(clave, 1000, 1000, null);

		// creamos la imagen
		try {
			codeQrImage = barcodeQRCode.getImage();
		} catch (BadElementException e) {
			e.printStackTrace();
		}

		// escalamos la imagen y la agregamos al documento
		codeQrImage.scaleAbsolute(100, 100);

        Celda.addElement(codeQrImage);

        // agregamos la celda a la tabla
        Tabla.addCell(Celda);

        try {
            // agregamos la tabla
            this.documento.add(Tabla);
        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

	}
    
}
