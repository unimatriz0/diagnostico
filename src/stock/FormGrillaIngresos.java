/*
 * Nombre: formGrillaIngresos
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 05/08/2017
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnostico
 * Comentarios: Procedimiento que arma la grilla de ingresos del stock
 *              y permite llamar el formulario de nuevo ingreso
 */

// definición del paquete
package stock;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import com.toedter.calendar.JDateChooser;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import funciones.Utilidades;
import funciones.RendererTabla;
import mensajes.Mensajes;
import seguridad.Seguridad;
import financiamiento.Financiamiento;
import funciones.ComboClave;

// definición de la clase
public class FormGrillaIngresos extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // definimos las variables
    protected JTable tIngresos;
    public JDateChooser dFechaInicio;
    public JDateChooser dFechaFin;
    protected ResultSet nominaIngresos;
    protected Ingresos Deposito;
    private JTextField tId;
    private JComboBox<Object> cDescripcion;
    private JTextField tRemito;
    private JSpinner sCantidad;
    private JDateChooser dFechaIngreso;
    private JDateChooser dVencimiento;
    private JTextField tRecibio;
    private JTextArea tComentarios;
    private JTextField tLote;
    private JTextField tUbicacion;
    private JComboBox<Object> cFinanciamiento;

    // definimos y configuramos el formulario
    public FormGrillaIngresos (Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

        // instanciamos el stock
        this.Deposito = new Ingresos();

        // fijamos el tamaño y el layout
        this.setBounds(100, 100, 889, 562);
        getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Entradas al Depósito");

        // inicializamos el formulario
        this.initFormIngresos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario
     */
    protected void initFormIngresos(){

        // la id del ingreso
        JLabel lId = new JLabel("ID:");
        lId.setBounds(10, 10, 26, 26);
        getContentPane().add(lId);
        this.tId = new JTextField();
        this.tId.setToolTipText("Clave del Registro");
        this.tId.setEditable(false);
        this.tId.setBounds(33, 10, 70, 26);
        getContentPane().add(this.tId);

        // la descripción con la posibilidad de editarlo
        JLabel lDescripcion = new JLabel("Descripción:");
        lDescripcion.setBounds(118, 10, 170, 26);
        getContentPane().add(lDescripcion);
        this.cDescripcion = new JComboBox<>();
        this.cDescripcion.setEditable(true);
        this.cDescripcion.setToolTipText("Seleccione el artículo de la lista");
        this.cDescripcion.setBounds(212, 10, 264, 26);
        getContentPane().add(this.cDescripcion);

        // cargamos los items en el combo
        this.cargaArticulos();
        
        // el número de remito
        JLabel lRemito = new JLabel("Remito: ");
        lRemito.setBounds(518, 10, 59, 26);
        getContentPane().add(lRemito);
        this.tRemito = new JTextField();
        this.tRemito.setToolTipText("Número de Remito de Ingreso");
        this.tRemito.setBounds(576, 10, 114, 26);
        getContentPane().add(this.tRemito);

        // el botón grabar
        JButton btnGrabar = new JButton("Grabar");
        btnGrabar.setToolTipText("Genera el remito de Entrada");
        btnGrabar.setBounds(760, 10, 115, 26);
        btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabaIngreso();
            }
        });
        getContentPane().add(btnGrabar);

        // la cantidad ingresada
        JLabel lCantidad = new JLabel("Cantidad:");
        lCantidad.setBounds(10, 45, 70, 26);
        getContentPane().add(lCantidad);
        this.sCantidad = new JSpinner();
        this.sCantidad.setBounds(91, 45, 59, 26);
        getContentPane().add(this.sCantidad);

        // la fecha de entrada al depósito
        JLabel lFechaIngreso = new JLabel("Fecha:");
        lFechaIngreso.setBounds(162, 45, 53, 26);
        getContentPane().add(lFechaIngreso);
        this.dFechaIngreso = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaIngreso.setToolTipText("Fecha de Entrada del Item");
        this.dFechaIngreso.setBounds(220, 45, 110, 26);
        getContentPane().add(this.dFechaIngreso);

        // la fecha de vencimiento
        JLabel lVencimiento = new JLabel("Vencimiento:");
        lVencimiento.setBounds(351, 45, 90, 26);
        getContentPane().add(lVencimiento);
        this.dVencimiento = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dVencimiento.setToolTipText("Indique la fecha de vencimiento del item");
        this.dVencimiento.setBounds(444, 45, 110, 26);
        getContentPane().add(this.dVencimiento);

        // el usuario que está dando la entrada
        JLabel lRecibio = new JLabel("Recibió:");
        lRecibio.setBounds(574, 45, 70, 26);
        getContentPane().add(lRecibio);
        this.tRecibio = new JTextField();
        this.tRecibio.setToolTipText("Usuario que recibió el elemento");
        this.tRecibio.setEditable(false);
        this.tRecibio.setBounds(630, 45, 114, 26);
        this.tRecibio.setText(Seguridad.Usuario);
        getContentPane().add(this.tRecibio);

        // el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setToolTipText("Cierra el formulario sin grabar");
        btnCancelar.setBounds(760, 45, 115, 26);
        btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoIngreso();
            }
        });
        getContentPane().add(btnCancelar);

        // la fuente de financiamiento
		// presenta el select
		JLabel lFinanciamiento = new JLabel("Financiamiento:");
		lFinanciamiento.setBounds(240, 80, 100, 26);
		getContentPane().add(lFinanciamiento);
		this.cFinanciamiento = new JComboBox<>();
		this.cFinanciamiento.setBounds(340, 80, 150, 26);
		this.cFinanciamiento.setToolTipText("Seleccione la fuente de financiamiento");
		getContentPane().add(this.cFinanciamiento);

        // el número de lote
        JLabel lLote = new JLabel("Lote: ");
        lLote.setBounds(500,80,100,26);
        getContentPane().add(lLote);
        this.tLote = new JTextField();
        this.tLote.setToolTipText("Número de lote del artículo");
        this.tLote.setBounds(535,80,100,26);
        getContentPane().add(this.tLote);

        // la ubicación
        JLabel lUbicacion = new JLabel("Ubicación:");
        lUbicacion.setBounds(645,80,100,26);
        getContentPane().add(lUbicacion);
        this.tUbicacion = new JTextField();
        this.tUbicacion.setBounds(715,80,160,26);
        this.tUbicacion.setToolTipText("Ubicación física del almacenamiento");
        getContentPane().add(this.tUbicacion);

        // el label de comentarios
        JLabel lComentarios = new JLabel("Comentarios y Obseraciones");
        lComentarios.setBounds(10, 80, 256, 26);
        getContentPane().add(lComentarios);
        this.tComentarios = new JTextArea();
        this.tComentarios.setToolTipText("Comentarios que considere necesarios");
        this.tComentarios.setBounds(10, 115, 480, 85);
        getContentPane().add(this.tComentarios);

        // define la fecha de inicio
        JLabel lInicio = new JLabel("Fecha Inicio:");
        lInicio.setBounds(500, 115, 103, 26);
        getContentPane().add(lInicio);
        this.dFechaInicio = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaInicio.setBounds(588, 115, 112, 26);
        this.dFechaInicio.setToolTipText("Fecha inicial del filtro");
        getContentPane().add(this.dFechaInicio);

        // presenta el botón nuevo egreso
        JButton btnNuevo = new JButton("Nuevo");
        btnNuevo.setBounds(760, 115, 115, 26);
        btnNuevo.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        btnNuevo.setToolTipText("Añadir un nuevo ingreso");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuevoIngreso();
            }
        });
        getContentPane().add(btnNuevo);

        // define la fecha de finalización
        JLabel lFechaFin = new JLabel("Fecha Fin:");
        lFechaFin.setBounds(500, 150, 77, 26);
        getContentPane().add(lFechaFin);
        this.dFechaFin = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaFin.setBounds(588, 150, 112, 26);
        this.dFechaFin.setToolTipText("Fecha final del filtro");
        getContentPane().add(this.dFechaFin);

        // presenta el botón exportar
        JButton btnFiltrar = new JButton("Exportar");
        btnFiltrar.setBounds(760, 150, 115, 26);
        btnFiltrar.setToolTipText("Pulse para exportar los ingresos");
        btnFiltrar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mestadistica.png")));
        btnFiltrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtraIngresos();
            }
        });
        getContentPane().add(btnFiltrar);

        // define la tabla y la agrega al scroll
        this.tIngresos = new JTable();
        this.tIngresos.setModel(new DefaultTableModel(
                        new Object[][] {
                                        { null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null },
                        },
                        new String[] {  "ID",
                                        "Item",
                                        "Importe",
                                        "Remito",
                                        "Cantidad",
                                        "Venc.",
                                        "Recibio",
                                        "Ver",
                                        "Imp",
                                        "El"
                        }) {
            Class[] columnTypes = new Class[] {Integer.class,
            		                           String.class,
            		                           Float.class,
            		                           String.class,
            		                           Integer.class,
            		                           String.class,
            		                           String.class,
            		                           Object.class,
            		                           Object.class,
            		                           Object.class };

            public Class getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
            }

            boolean[] columnEditables = new boolean[] {false,
                                                       false,
                                                       false,
                                                       false,
                                                       false,
                                                       false,
                                                       false,
                                                       false,
                                                       false,
                                                       false };

            public boolean isCellEditable(int row, int column) {
                return columnEditables[column];
            }
        });
        this.tIngresos.setToolTipText("Pulse para ver el detalle del remito");

        // fijamos el alto de las filas
        this.tIngresos.setRowHeight(25);

        // fijamos el ancho de las columnas
        this.tIngresos.getColumn("ID").setPreferredWidth(30);
        this.tIngresos.getColumn("ID").setMaxWidth(30);
        this.tIngresos.getColumn("Importe").setPreferredWidth(70);
        this.tIngresos.getColumn("Importe").setMaxWidth(70);
        this.tIngresos.getColumn("Remito").setPreferredWidth(70);
        this.tIngresos.getColumn("Remito").setMaxWidth(70);
        this.tIngresos.getColumn("Cantidad").setMaxWidth(50);
        this.tIngresos.getColumn("Cantidad").setPreferredWidth(50);
        this.tIngresos.getColumn("Venc.").setMaxWidth(85);
        this.tIngresos.getColumn("Venc.").setPreferredWidth(85);
        this.tIngresos.getColumn("Recibio").setMaxWidth(85);
        this.tIngresos.getColumn("Recibio").setPreferredWidth(85);
        this.tIngresos.getColumn("Ver").setMaxWidth(30);
        this.tIngresos.getColumn("Ver").setPreferredWidth(30);
        this.tIngresos.getColumn("Imp").setMaxWidth(30);
        this.tIngresos.getColumn("Imp").setPreferredWidth(30);
        this.tIngresos.getColumn("El").setMaxWidth(30);
        this.tIngresos.getColumn("El").setPreferredWidth(30);

        // define el scroll
        JScrollPane scrollEgresos = new JScrollPane(this.tIngresos);
        scrollEgresos.setViewportView(this.tIngresos);
        scrollEgresos.setBounds(10, 220, 865, 305);
        getContentPane().add(scrollEgresos);

        // fijamos el evento click
        this.tIngresos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tIngresosMouseClicked(evt);
            }

        });

        // cargamos las fuentes de financiamiento
        this.cargaFinanciamiento();

        // por defecto cargamos todos los ingresos
        this.cargaTodos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el combo el diccionario de artículos
     */
    protected void cargaArticulos() {
    	
    	// instanciamos la clase
    	Stock Articulos = new Stock();
    	ResultSet nominaArticulos = Articulos.getNominaItems();
    	
    	// agregamos el primer elemento en blanco
    	this.cDescripcion.addItem(new ComboClave(0,""));
    	
    	// recorremos el vector
    	try {
			while (nominaArticulos.next()) {
	
				// agregamos el elemento al combo
				this.cDescripcion.addItem(new ComboClave(nominaArticulos.getInt("iditem"), nominaArticulos.getString("descripcion")));
				
			}
		
		// si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    // evento llamado al pulsar el botón filtrar
    public void filtraIngresos() {

        // si marcó fecha de inicio y finalización
        if (this.dFechaInicio.getDate() != null && this.dFechaFin.getDate() != null) {

            // instanciamos las clases
            Utilidades Herramientas = new Utilidades();

            // convertimos las fechas a string
            String fechaInicio = Herramientas.fechaJDate(this.dFechaInicio);
            String fechaFin = Herramientas.fechaJDate(this.dFechaFin);

            // obtenemos el vector
            ResultSet nomina = this.Deposito.getIngresos(fechaInicio, fechaFin);

            // llamamos la hoja de cálculo
            new XLSIngresos(nomina);

        // si no marcó
        } else {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Seleccione un criterio", "Error", JOptionPane.ERROR_MESSAGE);
            return;

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo llamado en la carga del formulario que completa el
     * combo con las fuentes de financiamiento
     */
    protected void cargaFinanciamiento(){

    	// declaraciòn de variables
    	ResultSet Nomina;

    	// instanciamos la clase
    	Financiamiento Fuentes = new Financiamiento();

    	// obtenemos la nómina
    	Nomina = Fuentes.nominaFinanciamiento();

        try {

			// agregamos el primer elemento usamos la clase comboclave
			// para almacenar tanto la id como el texto
			this.cFinanciamiento.addItem(new ComboClave(0,""));

			// verificamos si está vacío
			if (!Nomina.next()){

				// presenta el mensaje y cierra el formulario
				JOptionPane.showMessageDialog(this, "Debe cargar fuentes de financiamiento", "Error", JOptionPane.ERROR_MESSAGE);
				return;

			}

            // nos desplazamos al inicio del resultset
            Nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (Nomina.next()){

                // agregamos el registro
				this.cFinanciamiento.addItem(new ComboClave(Nomina.getInt("id"), Nomina.getString("fuente")));

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al inicializar el formulario que carga
     * todos los ingresos
     */
    protected void cargaTodos(){

        // obtenemos el vector
        this.nominaIngresos = this.Deposito.getIngresos();

        // carga la grilla
        this.cargaItems();

    }

    /**
     * Método que a partir del resultset completa la grilla de items
     */
    protected void cargaItems(){

        // sobrecargamos el renderer de la tabla
        this.tIngresos.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) tIngresos.getModel();

        // hacemos la tabla se pueda ordenar
        tIngresos.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[10];

        try {

            // nos desplazamos al inicio del resultset
            this.nominaIngresos.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (this.nominaIngresos.next()){

                // fijamos los valores de la fila
                fila[0] = this.nominaIngresos.getInt("id_ingreso");
                fila[1] = this.nominaIngresos.getString("item");
                fila[2] = this.nominaIngresos.getFloat("importe");
                fila[3] = this.nominaIngresos.getString("remito");
                fila[4] = this.nominaIngresos.getInt("cantidad");
                fila[5] = this.nominaIngresos.getString("vencimiento");
                fila[6] = this.nominaIngresos.getString("usuario");
                fila[7] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[8] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
                fila[9] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    // método llamado al pulsar sobre la grilla
    private void tIngresosMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tIngresos.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tIngresos.rowAtPoint(evt.getPoint());
        int columna = this.tIngresos.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tIngresos.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // obtenemos la clave del item
            int clave = (int) modeloTabla.getValueAt(indice, 0);

            // si pulsó en editar
            if (columna == 7){

                // cargamos el registro
                this.getDatosIngreso(clave);

            // si pulsó en imprimir remito
            } else if (columna == 8){

                // imprimimos
                this.imprimeRemito(clave);

            // si pulsó en eliminar
            } else if (columna == 9){

                // eliminamos
                this.borraIngreso(clave);

            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario para un nuevo ingreso, también
     * llamado desde el botón cancelar
     */
    protected void nuevoIngreso(){

    	// inicializamos los campos
    	this.tId.setText("");
        ComboClave descripcion = new ComboClave(0, "");
        this.cDescripcion.setSelectedItem(descripcion);		
    	this.tRemito.setText("");
    	this.sCantidad.setValue(0);
    	ComboClave elemento = new ComboClave(0, "");
    	this.cFinanciamiento.setSelectedItem(elemento);

    	// instanciamos la clase de herramientas
    	Utilidades Herramientas = new Utilidades();

    	// fijamos las fechas de ingreso
    	this.dFechaIngreso.setDate(Herramientas.StringToDate(Herramientas.FechaActual()));

    	// la fecha de vencimiento
    	this.dVencimiento.setDate(Herramientas.StringToDate(Herramientas.FechaActual()));

    	// terminamos de inicializar los campos
    	this.tRecibio.setText(Seguridad.Usuario);
    	this.tLote.setText("");
    	this.tUbicacion.setText("");
    	this.tComentarios.setText("");

    	// fijamos el foco
    	this.cDescripcion.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de un ingreso
     * y presenta los datos del mismo en el formulario
     */
    protected void getDatosIngreso(int clave){

        // obtenemos los datos del registro
        this.Deposito.getDatosIngreso(clave);

        // agregamos los datos en el formulario
        this.tId.setText(Integer.toString(this.Deposito.getIdIngreso()));
        ComboClave descripcion = new ComboClave(this.Deposito.getIdItem(),this.Deposito.getItem());
        this.cDescripcion.setSelectedItem(descripcion);		
        this.tRemito.setText(this.Deposito.getRemito());
        this.sCantidad.setValue(this.Deposito.getCantidad());
        ComboClave elemento = new ComboClave(this.Deposito.getIdFinanciamiento(), this.Deposito.getFinanciamiento());
        this.cFinanciamiento.setSelectedItem(elemento);

        // instanciamos las herramientas
        Utilidades Herramientas = new Utilidades();

        // convertimos la fecha de entrada
        this.dFechaIngreso.setDate(Herramientas.StringToDate(this.Deposito.getFechaIngreso()));

        // convertimos la fecha de vencimiento
        this.dVencimiento.setDate(Herramientas.StringToDate(this.Deposito.getVencimiento()));

        // terminamos de cargar el formulario
        this.tLote.setText(this.Deposito.getLote());
        this.tUbicacion.setText(this.Deposito.getUbicacion());
        this.tRecibio.setText(this.Deposito.getUsuario());
        this.tComentarios.setText(this.Deposito.getComentarios());

        // fijamos el foco
        this.cDescripcion.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de un
     * ingreso e imprime el remito
     */
    protected void imprimeRemito(int clave){

    	// instanciamos la clase
    	new RemitoIngreso(clave);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de un ingreso
     * y luego de pedir confirmación lo elimina
     */
    protected void borraIngreso(int clave){

        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Está seguro que desea eliminar el ingreso?",
                                   "Entradas de Materiales",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // eliminamos el registro
            this.Deposito.borraIngreso(clave);

            // recargamos la grilla con todos los items
            this.cargaTodos();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón buscar que valida el
     * formulario y luego ejecuta la consulta
     */
    protected void grabaIngreso(){

    	// si está insertando
    	if (this.tId.getText().isEmpty()){
    		this.Deposito.setIdIngreso(0);
    	} else {
    		this.Deposito.setIdIngreso(Integer.parseInt(this.tId.getText()));
    	}

    	// verifica se halla seleccionado el artículo
		ComboClave articulo = (ComboClave) this.cDescripcion.getSelectedItem();
		if (articulo == null) {
			
            // presenta el mensaje
            JOptionPane.showMessageDialog(this, 
            		    "Seleccione el artículo a pedir", 
            		    "Error", 
            		    JOptionPane.ERROR_MESSAGE);
            this.cDescripcion.requestFocus();
            return;
			
        // verificamos que el id no sea 0
		} else if (articulo.getClave() == 0) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, 
            		    "Debe seleccionar un artículo", 
            		    "Error", 
            		    JOptionPane.ERROR_MESSAGE);
            this.cDescripcion.requestFocus();
            return;
			
        // si seleccionó
		} else {
    	
    		// seteamos en la clase
    		this.Deposito.setIdItem(articulo.getClave());

    	}

    	// si no ingresó el remito
    	if (this.tRemito.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique el remito de ingreso", "Error", JOptionPane.ERROR_MESSAGE);
            this.tRemito.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// fijamos en la clase
    		this.Deposito.setRemito(this.tRemito.getText());

    	}

    	// si no ingresó la cantidad
    	if (this.sCantidad.getValue().equals(0)){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique la cantidad ingresada", "Error", JOptionPane.ERROR_MESSAGE);
            this.sCantidad.requestFocus();
            return;

    	// si ingresó
    	} else {

    		// fijamos la cantidad
    		this.Deposito.setCantidad(Integer.parseInt(this.sCantidad.getValue().toString()));

    	}

    	// instanciamos las herramientas
    	Utilidades Herramientas = new Utilidades();

    	// si no ingresó la fecha de entrada
    	if (Herramientas.fechaJDate(this.dFechaIngreso) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique la fecha de entrada al depósito", "Error", JOptionPane.ERROR_MESSAGE);
            this.dFechaIngreso.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		this.Deposito.setFechaIngreso(Herramientas.fechaJDate(this.dFechaIngreso));

    	}

    	// si no ingresó la fecha de vencimiento
    	if (Herramientas.fechaJDate(this.dVencimiento) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique la fecha de vencimiento", "Error", JOptionPane.ERROR_MESSAGE);
            this.tRemito.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		this.Deposito.setVencimiento(Herramientas.fechaJDate(this.dFechaIngreso));

    	}

    	// si no ingresò la fuente de financiamiento
		ComboClave item = (ComboClave) this.cFinanciamiento.getSelectedItem();
		if (item == null){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe seleccionar la fuente de financiamiento", "Error", JOptionPane.ERROR_MESSAGE);
            this.cFinanciamiento.requestFocus();
            return;

        // si seleccionó
		} else {

			// asignamos en la clase
			this.Deposito.setIdFinanciamiento(item.getClave());

		}

    	// si no ingresó el lote
    	if (this.tLote.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique el número de lote", "Error", JOptionPane.ERROR_MESSAGE);
            this.tRemito.requestFocus();
            return;

    	// si ingresó
    	} else {

    		// asignmos en la clase
    		this.Deposito.setLote(this.tLote.getText());

    	}

    	// la ubicación y los comentarios los asignamos igual
    	this.Deposito.setUbicacion(this.tUbicacion.getText());
    	this.Deposito.setComentarios(this.tComentarios.getText());

    	// grabamos el registro
    	this.Deposito.grabaIngreso();

    	// presenta el mensaje
    	new Mensajes("Registro Grabado");

    	// recargamos la grilla
    	this.cargaTodos();

    	// limpiamos el formulario
    	this.nuevoIngreso();

    	// fijamos el foco
    	this.cDescripcion.requestFocus();

    }

}
