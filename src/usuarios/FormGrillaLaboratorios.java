/*

    Nombre: formGrillaLaboratorios
    Fecha: 28/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Metodo utilizado para presentar la nomina de laboratorios
                 y asignar la clave del mismo al usuario como responsable
                 de ese laboratorio

 */

// definición del paquete
package usuarios;

// importamos las librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 * @author Lic. Claudio Invernizzi
 */

// declaración de la clase
public class FormGrillaLaboratorios extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
	
    // Declaracion de variables del formulario
    private javax.swing.JScrollPane Scroll;
    private javax.swing.JTable tLaboratorios;

    // declaramos las variables que usamos para llamar
    // al formulario padre
    private FormUsuarios padre;

    /**
     * @param parent el formulario padre
     * @param modal valor que indica si será modal
     * Constructor cuando es llamado desde un dialog
     */
    public FormGrillaLaboratorios(javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);

        // inicializamos los componentes
        initComponents();

    }

    /**
     * @param formulario un objeto padre del formulario actual
     * Método que establece el formulario padre para luego poder
     * llamar las rutinas de presentación de datos
     */
    public void fijarPadre (FormUsuarios formulario){

        // asignamos a la variable local el objeto
        this.padre = formulario;

    }

    // evento que recibe como parámetro un resultset con los registros
    // encontrados y carga la grilla con los mismos
    public void muestraLaboratorios(ResultSet listado){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tLaboratorios.getModel();

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[4];

        try {

            // nos desplazamos al primer registro
            listado.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (listado.next()){

                // fijamos los valores de la fila
                fila[0] = listado.getInt("idlaboratorio");
                fila[1] = listado.getString("laboratorio");
                fila[2] = listado.getString("responsable");
                fila[3] = listado.getString("jurisdiccion");

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * Metodo que inicia los componentes del formulario
     */
    private void initComponents() {

        // definimos las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("laboratorios Encontrados");
        this.setLayout(null);
        this.setResizable(false);
        this.setBounds(250,250,770,460);

        // definimos la tabla y sus propiedades
        this.tLaboratorios = new javax.swing.JTable();
        this.tLaboratorios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Laboratorio", "Responsable", "Provincia"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        this.tLaboratorios.setToolTipText("Pulse sobre un registro para examinarlo");
        this.tLaboratorios.setColumnSelectionAllowed(true);
        this.tLaboratorios.getTableHeader().setReorderingAllowed(false);
        this.tLaboratorios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tLaboratoriosMouseClicked(evt);
            }
        });
        this.tLaboratorios.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        // definimos el scroll
        this.Scroll = new javax.swing.JScrollPane(this.tLaboratorios);
        this.Scroll.setViewportView(this.tLaboratorios);
        this.Scroll.setBounds(10,10,740,410);
        this.add(this.Scroll);

    }

    // evento llamado al pulsar con el mouse sobre una celda
    private void tLaboratoriosMouseClicked(java.awt.event.MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tLaboratorios.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tLaboratorios.rowAtPoint(evt.getPoint());
        int columna = this.tLaboratorios.columnAtPoint(evt.getPoint());

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos el nombre completo del laboratorio
            String nombreLaboratorio = (String) modeloTabla.getValueAt(fila, 1);

            // presentamos el valor en el campo y fijamos la clave
            this.padre.tLaboratorio.setText(nombreLaboratorio);
            this.padre.tClaveLaboratorio.setText((String) modeloTabla.getValueAt(fila, 0));

            // cerramos este formulario
            this.dispose();

        }

    }

}
