/*
 *
 * Nombre: formUsuarios
 * Fecha: 30/05/2017
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Proyecto: diagnostico
 * Licencia: GPL
 * Producido en: INP - Dr. Mario Fatala Chaben
 * Buenos Aires - Argentina
 * Comentarios: Formulario que permite el alta y edición de usuarios
 * autorizados del sistema
 *
 */

// definición del paquete
package usuarios;

// importamos las librerías
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import paises.Paises;
import localidades.Localidades;
import provincias.Provincias;
import funciones.Utilidades;
import laboratorios.Laboratorios;
import mapas.FormMapas;
import mensajes.Mensajes;
import seguridad.Seguridad;
import funciones.ComboClave;

// definición de la clase
public class FormUsuarios extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // declaración de variables
    private JTextField tId;
    private JTextField tIdDiagnostico;
    private JTextField tNombre;
    private JTextField tInstitucion;
    private JTextField tTelefono;
    private JTextField tMail;
    private JTextField tDireccion;
    private JTextField tCodigoPostal;
    private JComboBox<Object> cPais;
    private JComboBox<Object> cJurisdiccion;
    private JComboBox<Object> cLocalidad;
    private JCheckBox cResponsable;
    private JCheckBox cActivo;
    private JScrollPane scrollResponsables;
    private JButton btnLaboratorio;
    private JButton btnMapa;
    private JTextField tCoordenadas;
    private JTextArea tComentarios;
    private JTextField tCargo;
    public JTextField tLaboratorio;
    public JTextField tClaveLaboratorio;
    private JTextField tUsuario;
    private JTextField tAutorizo;
    private JTextField tFechaAlta;

    // definimos las variables de los checkbox
    private JCheckBox cAdministrador; // el de administrador
    private JCheckBox cPacientes; // el de personas / pacientes
    private JCheckBox cEntrevistas; // el de entrevistas
    private JCheckBox cResultados; // el de resultados
    private JCheckBox cCongenito; // el de chagas congénito
    private JCheckBox cProtocolos; // el de protocolos
    private JCheckBox cUsuarios; // el de usuarios del sistema
    private JCheckBox cMuestras; // el de toma de muestras
    private JCheckBox cStock; // el de stock
    private JCheckBox cAuxiliares; // el de tablas auxiliares
    private JCheckBox cNivelCentral; // el de nivel central

    // las variables de los botones
    private JButton btnGrabar;
    private JButton btnCancelar;

    // definimos las variables de clase
    protected Paises Naciones;
    protected Usuarios Responsables;
    protected Localidades Ciudades;
    protected Provincias Jurisdicciones;

    /**
     * Definición del formulario cuando no recibe parámetros
     */
    public FormUsuarios () {

        // llamamos al constructor del formulario
        this.initFormulario();

        // fijamos la fecha actual
        Utilidades Herramientas = new Utilidades();
        String Fecha = Herramientas.FechaActual();
        this.tFechaAlta.setText(Fecha);

        // fijamos el usuario que autorizó
        this.tAutorizo.setText(Seguridad.Usuario);

        // verifica el nivel de acceso
        this.fijarAcceso();

        // setea el foco en el primer elemento
        this.tNombre.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idUsuario entero con la clave del usuario
     * Definición del formulario cuando recibe la id de un usuario
     */
    public FormUsuarios (int idUsuario) {

        // llamamos al constructor del formulario
        this.initFormulario();

        // mostramos el registro
        this.muestraUsuario(idUsuario);

        // verifica el nivel de acceso
        this.fijarAcceso();

    }

    /*
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el fomulario
     */
    protected void initFormulario() {

        // inicializamos las variables de clase
    	this.Naciones = new Paises();
        this.Responsables = new Usuarios();
        this.Ciudades = new Localidades();
        this.Jurisdicciones = new Provincias();

        // fijamos el título y las propiedades
        setTitle("Usuarios Autorizados");
        setResizable(false);
        setModal(true);
        setBounds(100, 100, 837, 500);
        getContentPane().setLayout(null);

        // presentamos el título
        JLabel lTitulo = new JLabel("<html><b>Usuarios Autorizados del Sistema</b></html>");
        lTitulo.setBounds(10, 15, 645, 26);
        getContentPane().add(lTitulo);

        // el label de id del usuario
        JLabel lId = new JLabel("ID:");
        lId.setBounds(10, 52, 30, 26);
        getContentPane().add(lId);

        // el texto de id de la tabla de responsables
        this.tId = new JTextField();
        this.tId.setEditable(false);
        this.tId.setToolTipText("Clave del registro");
        this.tId.setBounds(30, 52, 55, 26);
        getContentPane().add(this.tId);

        // presenta oculta la clave de la tabla de
        // usuarios de diagnóstico
        this.tIdDiagnostico = new JTextField();
        this.tIdDiagnostico.setEditable(false);
        this.tIdDiagnostico.setVisible(false);

        // el nombre del responsable
        JLabel lNombre = new JLabel("Nombre:");
        lNombre.setBounds(95, 52, 60, 26);
        getContentPane().add(lNombre);

        // el texto del nombre
        this.tNombre = new JTextField();
        this.tNombre.setToolTipText("Nombre y Apellidos completos");
        this.tNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tNombreKeyPressed(evt);
            }
        });
        this.tNombre.setBounds(155, 52, 335, 26);
        getContentPane().add(this.tNombre);

        // el label de usuario
        JLabel lUsuario = new JLabel("Usuario:");
        lUsuario.setBounds(500, 52, 100, 26);
        getContentPane().add(lUsuario);

        // el texto de usuario
        this.tUsuario = new JTextField();
        this.tUsuario.setToolTipText("Nombre de usuario en la base");
        this.tUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tUsuarioKeyPressed(evt);
            }
        });
        this.tUsuario.setBounds(560, 52, 100, 26);
        getContentPane().add(this.tUsuario);

        // el label de institución
        JLabel lInstitucion = new JLabel("Institución:");
        lInstitucion.setBounds(10, 85, 100, 26);
        getContentPane().add(lInstitucion);

        // el nombre de la institución
        this.tInstitucion = new JTextField();
        this.tInstitucion.setToolTipText("Institución a la que pertenece");
        this.tInstitucion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tInstitucionKeyPressed(evt);
            }
        });
        this.tInstitucion.setBounds(95, 85, 395, 26);
        getContentPane().add(this.tInstitucion);

        // el label del teléfono
        JLabel lTelefono = new JLabel("Teléfono:");
        lTelefono.setBounds(500, 85, 70, 26);
        getContentPane().add(lTelefono);

        // el número de teléfono
        this.tTelefono = new JTextField();
        this.tTelefono.setToolTipText("Teléfono con prefijo de área");
        this.tTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tTelefonoKeyPressed(evt);
            }
        });
        this.tTelefono.setBounds(568, 85, 125, 26);
        getContentPane().add(this.tTelefono);

        // el label de cargo
        JLabel lCargo = new JLabel("Cargo:");
        lCargo.setBounds(10, 120, 55, 26);
        getContentPane().add(lCargo);

        // el texto del cargo
        this.tCargo = new JTextField();
        this.tCargo.setToolTipText("Cargo que desempeña en la institución");
        this.tCargo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tCargoKeyPressed(evt);
            }
        });
        this.tCargo.setBounds(57, 120, 365, 26);
        getContentPane().add(this.tCargo);

        // el label de mail
        JLabel lMail = new JLabel("E-Mail:");
        lMail.setBounds(435, 120, 55, 26);
        getContentPane().add(lMail);

        // el texto del mail
        this.tMail = new JTextField();
        this.tMail.setToolTipText("Dirección de correo electrónico");
        this.tMail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tMailKeyPressed(evt);
            }
        });
        this.tMail.setBounds(483, 120, 230, 26);
        getContentPane().add(this.tMail);

        // el label de dirección
        JLabel lDireccion = new JLabel("Dirección:");
        lDireccion.setBounds(10, 156, 85, 26);
        getContentPane().add(lDireccion);

        // el texto de dirección
        this.tDireccion = new JTextField();
        this.tDireccion.setToolTipText("Dirección postal completa");
        this.tDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tDireccionKeyPressed(evt);
            }
        });
        this.tDireccion.setBounds(85, 156, 365, 26);
        getContentPane().add(this.tDireccion);

        // el label del código postal
        JLabel lCodigoPostal = new JLabel("Código Postal:");
        lCodigoPostal.setBounds(465, 156, 114, 26);
        getContentPane().add(lCodigoPostal);

        // el texto del código postal
        this.tCodigoPostal = new JTextField();
        this.tCodigoPostal.setToolTipText("Código Postal correspondiente");
        this.tCodigoPostal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tCodigoPostalKeyPressed(evt);
            }
        });
        this.tCodigoPostal.setBounds(577, 156, 85, 26);
        getContentPane().add(this.tCodigoPostal);

        // el botón mapa
        this.btnMapa = new JButton();
        this.btnMapa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/tierra.jpeg")));
        this.btnMapa.setToolTipText("Pulse para ver el Mapa");
        this.btnMapa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMapaActionPerformed(evt);
            }
        });
        this.btnMapa.setBounds(670, 156, 26, 26);
        getContentPane().add(this.btnMapa);

        // el label de país
        JLabel lPais = new JLabel("País:");
        lPais.setBounds(10, 190, 50, 26);
        getContentPane().add(lPais);

        // el combo de país
        this.cPais = new JComboBox<>();
        this.cPais.setToolTipText("País donde radica el responsable");
        this.cPais.setBounds(45, 190, 180, 26);
        getContentPane().add(this.cPais);

        // el label de jurisdicción
        JLabel lJurisdiccion = new JLabel("Jurisdicción:");
        lJurisdiccion.setBounds(240, 190, 120, 26);
        getContentPane().add(lJurisdiccion);

        // el combo de jurisdicción
        this.cJurisdiccion = new JComboBox<>();
        this.cJurisdiccion.setToolTipText("Seleccione la jurisdicción de la lista");
        this.cJurisdiccion.setBounds(325, 190, 186, 26);
        getContentPane().add(this.cJurisdiccion);

        // el label de localidad
        JLabel lLocalidad = new JLabel("Localidad:");
        lLocalidad.setBounds(516, 190, 120, 26);
        getContentPane().add(lLocalidad);

        // el combo de localidad
        this.cLocalidad = new JComboBox<>();
        this.cLocalidad.setToolTipText("Seleccione la localidad de la lista");
        this.cLocalidad.setBounds(591, 190, 226, 26);
        getContentPane().add(this.cLocalidad);

        // el checkbox de responsable
        this.cResponsable = new javax.swing.JCheckBox("Responsable");
        this.cResponsable.setBounds(5, 225, 120, 26);
        this.cResponsable.setToolTipText("Marque si es responsable");
        getContentPane().add(this.cResponsable);

        // el checkbox de activo
        this.cActivo = new JCheckBox("Activo");
        cActivo.setBounds(130, 225, 80, 26);
        cActivo.setToolTipText("Marque si el usuario está activo");
        getContentPane().add(cActivo);

        // el checkbox de nivel central
        this.cNivelCentral = new JCheckBox("Nivel Central");
        this.cNivelCentral.setBounds(210, 225, 120, 26);
        this.cNivelCentral.setToolTipText("Marque si el usuario es de nivel central");
        getContentPane().add(this.cNivelCentral);

        // el label de laboratorio responsable
        JLabel lLaboratorio = new JLabel("Laboratorio:");
        lLaboratorio.setBounds(335, 225, 100, 26);
        getContentPane().add(lLaboratorio);

        // el texto de laboratorio responsable
        this.tLaboratorio = new JTextField();
        this.tLaboratorio.setToolTipText("Si es responsable de un laboratorio");
        this.tLaboratorio.setBounds(425, 225, 314, 26);
        getContentPane().add(this.tLaboratorio);

        // el botón buscar laboratorio
        this.btnLaboratorio = new JButton();
        this.btnLaboratorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/busqueda.png")));
        this.btnLaboratorio.setToolTipText("Pulse para buscar el laboratorio");
        this.btnLaboratorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLaboratorioActionPerformed(evt);
            }
        });
        this.btnLaboratorio.setBounds(751, 225, 25, 26);
        getContentPane().add(this.btnLaboratorio);

        // el texto oculto de las coordenadas
        this.tCoordenadas = new JTextField();
        this.tCoordenadas.setEditable(false);
        this.tCoordenadas.setVisible(false);
        getContentPane().add(this.tCoordenadas);

        // el título de las tablas autorizadas
        JLabel lTablasAutorizadas = new JLabel("<html><b>Tablas Autorizadas:</b></html>");
        lTablasAutorizadas.setBounds(12, 250, 176, 26);
        getContentPane().add(lTablasAutorizadas);

        // el checkbox de pacientes
        this.cPacientes = new JCheckBox("Personas / pacientes");
        this.cPacientes.setBounds(8, 274, 180, 26);
        this.cPacientes.setToolTipText("El usuario puede cargar pacientes");
        getContentPane().add(this.cPacientes);

        // el de entrevistas
        this.cEntrevistas = new JCheckBox("Entrevistas");
        this.cEntrevistas.setBounds(195, 274, 120, 26);
        this.cEntrevistas.setToolTipText("El usuario puede realizar entrevistas");
        getContentPane().add(this.cEntrevistas);

        // el de resultados
        this.cResultados = new JCheckBox("Resultados");
        this.cResultados.setBounds(317, 274, 129, 26);
        this.cResultados.setToolTipText("El usuario puede validar resultados");
        getContentPane().add(this.cResultados);

        // el de chagas congénito
        this.cCongenito = new JCheckBox("Chagas Congénito");
        this.cCongenito.setBounds(8, 294, 180, 26);
        this.cCongenito.setToolTipText("El usuario puede cargar Chagas Congénito");
        getContentPane().add(this.cCongenito);

        // el de protocolos
        this.cProtocolos = new JCheckBox("Protocolos");
        this.cProtocolos.setBounds(195, 294, 120, 26);
        this.cProtocolos.setToolTipText("El usuario puede imprimir protocolos");
        getContentPane().add(this.cProtocolos);

        // el de usuarios del sistema
        this.cUsuarios = new JCheckBox("Usuarios del Sistema");
        this.cUsuarios.setBounds(317, 294, 194, 26);
        this.cUsuarios.setToolTipText("El usuario puede dar de alta otros usuarios");
        getContentPane().add(this.cUsuarios);

        // el de toma de muestras
        this.cMuestras = new JCheckBox("Toma de Muestras");
        this.cMuestras.setBounds(8, 315, 180, 26);
        this.cMuestras.setToolTipText("El usuario puede tomar muestras");
        getContentPane().add(this.cMuestras);

        // el de stock
        this.cStock = new JCheckBox("stock");
        this.cStock.setBounds(195, 315, 120, 25);
        this.cStock.setToolTipText("El usuario puede controlar el stock");
        getContentPane().add(this.cStock);

        // el de tablas auxiliares
        this.cAuxiliares = new JCheckBox("Tablas Auxiliares");
        this.cAuxiliares.setBounds(317, 315, 159, 26);
        this.cAuxiliares.setToolTipText("El usuario puede modificar las tablas auxiliares");
        getContentPane().add(this.cAuxiliares);

        // presenta el checkbox de administrador
        this.cAdministrador = new JCheckBox("Administrador");
        this.cAdministrador.setToolTipText("Marque si el usuario es administrador");
        this.cAdministrador.setBounds(507, 274, 129, 26);
        getContentPane().add(this.cAdministrador);

        // el label de comentarios
        JLabel lComentarios = new JLabel("Comentarios y Observaciones");
        lComentarios.setBounds(10, 338, 320, 26);
        getContentPane().add(lComentarios);

        // el texto de los comentarios
        this.tComentarios = new JTextArea();
        this.tComentarios.setColumns(20);
        this.tComentarios.setRows(5);

        // el contenedor de los comentarios
        this.scrollResponsables = new JScrollPane();
        this.scrollResponsables.setBounds(10, 368, 634, 90);
        this.scrollResponsables.add(this.tComentarios);
        getContentPane().add(this.scrollResponsables);

        // el label de fecha de alta
        JLabel lFechaAlta = new JLabel("Fecha Alta:");
        lFechaAlta.setBounds(654, 274, 89, 26);
        getContentPane().add(lFechaAlta);

        // el texto de fecha de alta
        this.tFechaAlta = new JTextField();
        this.tFechaAlta.setBounds(738, 274, 78, 26);
        this.tFechaAlta.setEditable(false);
        this.tFechaAlta.setToolTipText("Fecha de alta del registro");
        getContentPane().add(this.tFechaAlta);

        // presentamos el label de autorizó
        JLabel lAutorizo = new JLabel("Autorizó:");
        lAutorizo.setBounds(635, 315, 70, 26);
        getContentPane().add(lAutorizo);

        // presentamos el texto de autorizó, solo lectura
        this.tAutorizo = new JTextField();
        this.tAutorizo.setEditable(false);
        this.tAutorizo.setBounds(704, 315, 114, 26);
        getContentPane().add(this.tAutorizo);
        this.tAutorizo.setToolTipText("Usuario que autorizó el ingreso, solo lectura");

        // el botón grabar y fijamos el evento para verificar el formulario y
        // grabar
        this.btnGrabar = new JButton("Grabar");
        btnGrabar.setBounds(666, 379, 125, 30);
        btnGrabar.setToolTipText("Pulse para grabar el registro");
        getContentPane().add(btnGrabar);
        btnGrabar.setIcon(new ImageIcon(FormUsuarios.class.getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabaUsuario();
            }
        });

        // el botón cancelar y fijamos el evento para cerrar el formulario
        this.btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(666, 416, 125, 30);
        btnCancelar.setToolTipText("Pulse para cerrar el formulario");
        btnCancelar.setIcon(new ImageIcon(FormUsuarios.class.getResource("/Graficos/msalida.png")));
        getContentPane().add(btnCancelar);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispose();
            }
        });

        // cargamos el combo de países
        this.cargaPaises();

        // fijamos el listener del combo de países
        this.cPais.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cPaisItemStateChanged(evt);
            }
        });       

        // agregamos el listener del combo de jurisdicción
        this.cJurisdiccion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cJurisdiccionItemStateChanged(evt);
            }
        });
        
    }

    // Método llamado en el evento onclick del botón grabar, verifica
    // los datos del formulario y graba el registro
    protected void grabaUsuario() {

        // instanciamos la clase de usuarios
        Usuarios Persona = new Usuarios();

        // definimos las variables
        ComboClave item;
        
        // si está editando asigna la clave
        if (!this.tId.getText().isEmpty()) {
            Persona.setId(Integer.parseInt(this.tId.getText()));
        }

        // verifica se halla ingresado el nombre
        if (this.tNombre.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Debe ingresar el nombre completo del usuario", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tNombre.requestFocus();
            return;

            // si seleccionó
        } else {

            // asigna en la clase
            Persona.setNombre(this.tNombre.getText());

        }

        // verifica exista el nombre de usuario
        if (this.tUsuario.getText().isEmpty()) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe ingresar el nombre de usuario", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tUsuario.requestFocus();
            return;

            // si asignó y está dando un alta
        } else if (this.tId.getText().isEmpty()) {

            // verifica que ese usuario no exista
            if (Persona.VerificaUsuario(this.tUsuario.getText())) {

                // presenta el mensaje y retorna
                JOptionPane.showMessageDialog(this,
                                "Ese nombre de usuario está en uso",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                this.tUsuario.requestFocus();
                return;

            }

            // asigna en la clase
            Persona.setUsuario(this.tUsuario.getText());

        }

        // verifica se halla ingresado la institución
        if (this.tInstitucion.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique la institución del del Usuario", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tInstitucion.requestFocus();
            return;

            // si seleccionó
        } else {

            // asigna en la clase
            Persona.setIdLaboratorio(Integer.parseInt(this.tClaveLaboratorio.getText()));

        }

        // verifica se halla ingresado el cargo
        if (this.tCargo.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Ingrese el cargo que ocupa el usuario", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tCargo.requestFocus();
            return;

            // si asignó
        } else {

            // asigna en la clase
            Persona.setCargo(this.tCargo.getText());

        }

        // verifica se halla ingresado el mail
        if (this.tMail.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Ingrese la dirección de E-Mail", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tMail.requestFocus();
            return;

            // si ingresó
        } else {

            // asigna en la clase
            Persona.setMail(this.tMail.getText());
        }

        // verifica que halla ingresado el teléfono
        if (this.tTelefono.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Ingrese el teléfono con prefijo de área", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tTelefono.requestFocus();
            return;

            // si ingresó
        } else {

            // asigna en la clase
            Persona.setTelefono(this.tTelefono.getText());

        }

        // verifica que halla ingresado la dirección
        if (this.tDireccion.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique la dirección postal", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tDireccion.requestFocus();
            return;

            // si asignó
        } else {

            // asigna en la clase
            Persona.setDireccion(this.tDireccion.getText());

        }

        // verifica que halla ingresado el código postal
        if (this.tCodigoPostal.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Indique el Código Postal", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tCodigoPostal.requestFocus();
            return;

            // si asignó
        } else {

            // asigna en la clase
            Persona.setCodigoPostal(this.tCodigoPostal.getText());

        }

        // verifica que halla seleccionado el país
        if (this.cPais.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Seleccione de la lista el País de Residencia", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.cPais.requestFocus();
            return;

            // si seleccionó
        } else {

            // obtenemos la clave del país y asignamos en la clase
        	item = (ComboClave) this.cPais.getSelectedItem();
            Persona.setIdPais(item.getClave());

        }

        // verifica que halla seleccionado la jurisdicción simplemente para
        // estar
        // seguros que disparó el evento y tiene el listado de localidades
        if (this.cJurisdiccion.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Seleccione de la lista la Jurisdicción", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.cJurisdiccion.requestFocus();
            return;

        }

        // verifica que halla seleccionado la localidad
        if (this.cLocalidad.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Seleccione de la lista la localidad", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.cLocalidad.requestFocus();
            return;

        // si seleccionó
        } else {

            // obtenemos la clave de la localidd y asignamos en la clase
        	item = (ComboClave) this.cLocalidad.getSelectedItem();
            Persona.setIdLocalidad(item.getCodigo());

        }

        // activamos el contador de checkbox
        boolean Correcto = false;

        // si marco pacientes
        if (this.cPacientes.isSelected()) {
            Persona.setPersonas("Si");
            Correcto = true;
        }

        // si marcó entrevistas
        if (this.cEntrevistas.isSelected()) {
            Persona.setEntrevistas("Si");
            Correcto = true;
        }

        // si marcó resultados
        if (this.cResultados.isSelected()) {
            Persona.setResultados("Si");
            Correcto = true;
        }

        // si marcó congénito
        if (this.cCongenito.isSelected()) {
            Persona.setCongenito("Si");
            Correcto = true;
        }

        // si marcó protocolos
        if (this.cProtocolos.isSelected()) {
            Persona.setProtocolos("Si");
            Correcto = true;
        }

        // si marcó usuarios
        if (this.cUsuarios.isSelected()) {
            Persona.setUsuarios("Si");
            Correcto = true;
        }

        // si marcó toma de muestras
        if (this.cMuestras.isSelected()) {
            Persona.setMuestras("Si");
            Correcto = true;
        }

        // si marcó stock
        if (this.cStock.isSelected()) {
            Persona.setStock("Si");
            Correcto = true;
        }

        // si marcó tablas auxiliares
        if (this.cAuxiliares.isSelected()) {
            Persona.setAuxiliares("Si");
            Correcto = true;
        }

        // si no marcó ninguna autorización
        if (!Correcto) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                            "Debe autorizar al usuario para editar al\n " + "menos una de las tablas del sistema",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
            return;

        }

        // si seleccionó administrador
        if (this.cAdministrador.isSelected()) {
            Persona.setAdministrador("Si");
        } else {
            Persona.setAdministrador("No");
        }

        // si seleccionó responsable
        if (this.cResponsable.isSelected()) {
            Persona.setResponsable("Si");
        } else {
            Persona.setResponsable("No");
        }

        // si seleccionó nivel central
        if (this.cNivelCentral.isSelected()) {
            Persona.setNivelCentral("Si");
        } else {
            Persona.setNivelCentral("No");
        }

        // verifica si corresponde que seleccione un laboratorio
        // si responsable no está marcado y no hay seleccionado
        // un laboratorio
        if (!this.cResponsable.isSelected() && this.tClaveLaboratorio.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                            "Indique que laboratorio puede editar el usuario", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tLaboratorio.requestFocus();
            return;

            // si está seleccionado un laboratorio
        } else if (!this.tClaveLaboratorio.getText().isEmpty()) {

            // asigna en la clase
            Persona.setIdLaboratorio(Integer.parseInt(this.tClaveLaboratorio.getText()));

        }

        // graba el registro
        int Clave = Persona.grabaUsuario();

        // fija la clave en el formulario
        this.tId.setText(Integer.toString(Clave));
        this.tIdDiagnostico.setText(Integer.toString(Clave));

        // presenta el mensaje
        new Mensajes("Registro Grabado ... ");

        // setea el foco
        this.tNombre.requestFocus();

    }

    // evento llamado al pulsar sobre la institución
    private void tInstitucionKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tTelefono.requestFocus();
        }

    }

    // evento llamado al pulsar sobre el teléfono
    private void tTelefonoKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tCargo.requestFocus();
        }

    }

    // evento llamado al pulsar sobre el código postal
    private void tCodigoPostalKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.cPais.requestFocus();
        }

    }

    // evento llamado al pulsar una tecla sobre el nombre del usuario
    protected void tNombreKeyPressed(java.awt.event.KeyEvent evt) {

        // si se pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            // si está insertando
            if (this.tId.getText().isEmpty()) {
                this.tUsuario.requestFocus();
            } else {
                this.tCargo.requestFocus();
            }
        }

    }

    // evento llamado al pulsar sobre la dirección
    private void tDireccionKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tCodigoPostal.requestFocus();
        }

    }

    // evento llamado al pulsar una tecla sobre el usuario
    protected void tUsuarioKeyPressed(java.awt.event.KeyEvent evt) {

        // si se pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tCargo.requestFocus();
        }

    }

    // evento llamado al pulsar sobre el mail
    private void tMailKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tDireccion.requestFocus();
        }

    }

    // evento llamado al pulsar una tecla sobre el cargo
    protected void tCargoKeyPressed(java.awt.event.KeyEvent evt) {

        // si se pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tLaboratorio.requestFocus();
        }

    }

    // evento llamado al cambiar el estado del combo de país
    private void cPaisItemStateChanged(java.awt.event.ItemEvent evt){
   
        // obtenemos el valor del combo
        ComboClave item = (ComboClave) this.cPais.getSelectedItem();
        
        // si es distinto de nulo
        if (item != null){
        
            // elimina los elementos de la jurisdicción
            this.cJurisdiccion.removeAllItems();
            
            // agrega el primer elemento en blanco
            this.cJurisdiccion.addItem(new ComboClave("0", ""));
            
			// obtenemos la clave del país
			int idpais = item.getClave();

			// obtenemos el vector de jurisdicciones
            ResultSet nominaJurisdicciones = this.Jurisdicciones.listaProvincias(idpais);
            
            try {
                
                // recorremos el vector
                while (nominaJurisdicciones.next()){

                    // agregamos el elemento al select
                    this.cJurisdiccion.addItem(new ComboClave(nominaJurisdicciones.getString("cod_prov"), nominaJurisdicciones.getString("nombre_provincia")));
                }
                
            // si hubo un error
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
        }
        
    }
    
    // evento llamado al cambiar el combo de jurisdicción
    private void cJurisdiccionItemStateChanged(java.awt.event.ItemEvent evt) {

        // obtenemos el valor del combo
        ComboClave item = (ComboClave) this.cJurisdiccion.getSelectedItem();
               
        // si es distingo de nulo
        if (item != null) {

        	// obtenemos la clave 
        	String jurisdiccion = item.getCodigo();
        	
            // elimina los elementos de la localidad
            this.cLocalidad.removeAllItems();

            // agrega el primer elemento en blanco
            this.cLocalidad.addItem(new ComboClave("0", ""));

            // obtenemos el vector
            ResultSet nominaLocalidades = this.Ciudades.listaLocalidades(jurisdiccion);

            try {

                // recorremos el vector
                while (nominaLocalidades.next()) {

                    // lo agregamos el combo
                    this.cLocalidad.addItem(new ComboClave(nominaLocalidades.getString("codloc"), nominaLocalidades.getString("localidad")));

                }

            // si hubo un error
            } catch (SQLException ex) {
                ex.printStackTrace();

            }

        }

    }

    // evento llamado al pulsar sobre el botón buscar laboratorio en
    // la edición de usuarios, llama la grilla y presenta los laboratorios
    // que cumplen con el criterio indicado
    private void btnLaboratorioActionPerformed(java.awt.event.ActionEvent evt) {

        // verifica que halla ingresado un texto a buscar
        if ("".equals(this.tLaboratorio.getText())) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Ingrese parte del nombre del laboratorio", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.cPais.requestFocus();
            return;

        }

        // instanciamos la clase de laboratorios y buscamos
        Laboratorios Laboratorio = new Laboratorios();

        // inicializamos la variable
        ResultSet Resultado;

        // obtenemos los laboratorios que cumplen con el criterio
        Resultado = Laboratorio.buscaLaboratorio(this.tLaboratorio.getText());

        try {

            // si no hubo resultados
            Resultado.last();

            if (Resultado.getRow() == 0) {

                // presenta el mensaje
                JOptionPane.showMessageDialog(this, "No hay laboratorios que cumplan el criterio", "Error",
                                JOptionPane.ERROR_MESSAGE);
                this.tLaboratorio.requestFocus();
                return;

            }

            // nos desplazamos al primer registro
            Resultado.beforeFirst();

            // si hubo un error
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // si hubo resultados instancia la clase de la grilla
        FormGrillaLaboratorios grillaLaboratorios = new FormGrillaLaboratorios(this, true);

        // fija el padre
        grillaLaboratorios.fijarPadre(this);

        // carga la grilla con el resultset
        grillaLaboratorios.muestraLaboratorios(Resultado);

        // muestra el formulario
        grillaLaboratorios.setVisible(true);

    }

    // evento llamado al pulsar el botón ver mapa
    private void btnMapaActionPerformed(java.awt.event.ActionEvent evt) {

        // verificamos si seleccionó país, localidad o provincia y dirección
        if (this.tCoordenadas.getText().equals("")) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "No hay declaradas coordenadas válidas", "Error",
                            JOptionPane.ERROR_MESSAGE);

            // si está cargado
        } else {

            // componemos la dirección
            String Coordenadas = this.tDireccion.getText() + " - " + this.cJurisdiccion.getSelectedItem().toString()
                            + " - " + this.cPais.getSelectedItem().toString();

            // instancia el formulario, carga el mapa y lo muestra
            FormMapas Mapa = new FormMapas(this, true);
            Mapa.setCoordenadas(Coordenadas);
            Mapa.mostrarMapa();
            Mapa.setVisible(true);

        }

    }

    // método que carga el combo de paises, llamado en el
    // constructor del formulario
    private void cargaPaises() {

        // declaración de variables
        ResultSet nominaPaises;

        // obtenemos el listado
        nominaPaises = Naciones.nominaPaises();

        // agregamos el primer elemento en clanbo
        this.cPais.addItem(new ComboClave(0, ""));

        try {

            // recorremos el vector
            while (nominaPaises.next()) {

                // agregamos el registro
                this.cPais.addItem(new ComboClave(nominaPaises.getInt("id"), nominaPaises.getString("pais")));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idUsuario entero con la clave del usuario 
     * Método que recibe como parámetro la clave de un usuario, consulta la
     * base y actualiza el formulario
     */
    public void muestraUsuario(int idUsuario) {

        // obtenemos los datos del usuario
        this.Responsables.getDatosUsuario(idUsuario);

        // ahora cargamos los controles

        // el texto de id de la tabla de responsables
        this.tId.setText(Integer.toString(this.Responsables.getIdUsuario()));

        // presenta oculta la clave de la tabla de usuarios de diagnóstico
        this.tIdDiagnostico.setText(Integer.toString(this.Responsables.getIdDiagnostico()));

        // el texto del nombre
        this.tNombre.setText(this.Responsables.getNombre());

        // el texto de usuario
        this.tUsuario.setText(this.Responsables.getUsuario());

        // el nombre de la institución
        this.tInstitucion.setText(this.Responsables.getInstitucion());

        // el número de teléfono
        this.tTelefono.setText(this.Responsables.getTelefono());

        // el texto del cargo
        this.tCargo.setText(this.Responsables.getCargo());

        // el texto del mail
        this.tMail.setText(this.Responsables.getEMail());

        // el texto de dirección
        this.tDireccion.setText(this.Responsables.getDireccion());

        // el texto del código postal
        this.tCodigoPostal.setText(this.Responsables.getCodigoPostal());

        // el botón mapa se asegura de encenderlo
        this.btnMapa.setVisible(true);

        // el combo de país
        this.cPais.setSelectedItem(this.Responsables.getPais());

        // el combo de jurisdicción
        this.cJurisdiccion.setSelectedItem(this.Responsables.getProvincia());

        // el combo de localidad
        this.cLocalidad.setSelectedItem(this.Responsables.getLocalidad());

        // el checkbox de responsable
        if (this.Responsables.getResponsable().equals("Si")) {
            this.cResponsable.setSelected(true);
        } else {
            this.cResponsable.setSelected(false);
        }

        // el checkbox de activo
        if (this.Responsables.getActivo().equals("Si")) {
            this.cActivo.setSelected(true);
        } else {
            this.cActivo.setSelected(false);
        }

        // el checkbox de nivel central
        if (this.Responsables.getNivelCentral().equals("Si")) {
            this.cNivelCentral.setSelected(true);
        } else {
            this.cNivelCentral.setSelected(false);
        }

        // el texto de laboratorio responsable
        this.tLaboratorio.setText(this.Responsables.getLaboratorio());

        // el texto oculto de las coordenadas
        this.tCoordenadas.setText(this.Responsables.getCoordenadas());

        // el checkbox de pacientes
        if (this.Responsables.getPersonas().equals("Si")) {
            this.cPacientes.setSelected(true);
        } else {
            this.cPacientes.setSelected(false);
        }

        // el de entrevistas
        if (this.Responsables.getEntrevistas().equals("Si")) {
            this.cEntrevistas.setSelected(true);
        } else {
            this.cEntrevistas.setSelected(false);
        }

        // el de resultados
        if (this.Responsables.getResultados().equals("Si")) {
            this.cResultados.setSelected(true);
        } else {
            this.cResultados.setSelected(false);
        }

        // el de chagas congénito
        if (this.Responsables.getCongenito().equals("Si")) {
            this.cCongenito.setSelected(true);
        } else {
            this.cCongenito.setSelected(true);
        }

        // el de protocolos
        if (this.Responsables.getProcolos().equals("Si")) {
            this.cProtocolos.setSelected(true);
        } else {
            this.cProtocolos.setSelected(false);
        }

        // el de usuarios del sistema
        if (this.Responsables.getUsuarios().equals("Si")) {
            this.cUsuarios.setSelected(true);
        } else {
            this.cUsuarios.setSelected(false);
        }

        // el de toma de muestras
        if (this.Responsables.getMuestras().equals("Si")) {
            this.cMuestras.setSelected(true);
        } else {
            this.cMuestras.setSelected(false);
        }

        // el de stock
        if (this.Responsables.getStock().equals("Si")) {
            this.cStock.setSelected(true);
        } else {
            this.cStock.setSelected(false);
        }

        // el de tablas auxiliares
        if (this.Responsables.getAuxiliares().equals("Si")) {
            this.cAuxiliares.setSelected(true);
        } else {
            this.cAuxiliares.setSelected(false);
        }

        // presenta el checkbox de administrador
        if (this.Responsables.getAdministrador().equals("Si")) {
            this.cAdministrador.setSelected(true);
        } else {
            this.cAdministrador.setSelected(false);
        }

        // el texto de los comentarios
        this.tComentarios.setText(this.Responsables.getObservaciones());

        // el texto de fecha de alta
        this.tFechaAlta.setText(this.Responsables.getFechaAlta());

        // presentamos el texto de autorizó, solo lectura
        this.tAutorizo.setText(this.Responsables.getAutorizo());

        // fijamos el foco en el primer elemento
        this.tNombre.requestFocus();

    }

    // método que oculta los controles según el nivel de acceso del usuario
    private void fijarAcceso() {

        // si no puede editar usuarios
        if (!"Si".equals(Seguridad.Usuarios)) {

            // oculta el botón grabar
            this.btnGrabar.setVisible(false);

        }

    }

}
