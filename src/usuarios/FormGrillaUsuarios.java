/*

    Nombre: formGrillaUsuarios
    Fecha: 21/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que presenta la grilla con los usuarios encontrados 
                 y luego al pulsar sobre una celda, carga en el formulario
                 padre los datos del registro

 */

// declaración del paquete
package usuarios;

// importamos las librerías
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 * @author Lic. Claudio Invernizzi
 */

// declaración de la clase
public class FormGrillaUsuarios extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
	
    // Declaracion de variables del formulario
    private javax.swing.JScrollPane Scroll;
    private javax.swing.JTable tUsuarios;
       
    /**
     * @param parent el formulario padre
     * @param modal valor que indica si será modal
     * Creates new form formGrillaUsuarios
     */
    public FormGrillaUsuarios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        
        // inicializamos el formulario
        initComponents();

    }
   
    // evento que recibe como parámetro un resultset con los registros 
    // encontrados y carga la grilla con los mismos
    public void muestraUsuarios(ResultSet listado){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tUsuarios.getModel();

        // limpiamos la tabla
        modeloTabla.setRowCount(0);
        
        // definimos el objeto de las filas
        Object [] fila = new Object[4];

        try {

            // nos desplazamos al inicio del resultset
            listado.beforeFirst();
            
            // iniciamos un bucle recorriendo el vector
            while (listado.next()){

                // fijamos los valores de la fila
                fila[0] = listado.getInt("idusuario");
                fila[1] = listado.getString("nombreusuario");
                fila[2] = listado.getString("institucion");
                fila[3] = listado.getString("provincia");                
                
                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }
        
    }

    /**
     * Metodo que instancia los componentes del formulario
     */
    private void initComponents() {

        // definimos las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Usuarios Encontrados");
        this.setLayout(null);
        this.setResizable(false);
        this.setBounds(250,250,750,420);                
        
        // definimos la tabla y sus propiedades
        this.tUsuarios = new javax.swing.JTable();
        this.tUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Institución", "Provincia"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        this.tUsuarios.setToolTipText("Pulse sobre un registro para verlo");
        this.tUsuarios.setColumnSelectionAllowed(true);
        this.tUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tUsuariosMouseClicked(evt);
            }
        });
        this.tUsuarios.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        // fijamos el ancho de las columnas
        this.tUsuarios.getColumn("ID").setPreferredWidth(75);
        this.tUsuarios.getColumn("ID").setMaxWidth(75);
        
        // definimos el contenedor
        this.Scroll = new javax.swing.JScrollPane(this.tUsuarios);      
        this.Scroll.setViewportView(this.tUsuarios);
        this.Scroll.setBounds(10,0,720,380);
        this.add(this.Scroll);

    }

    // evento llamado al pulsar sobre una celda en la grilla
    private void tUsuariosMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tUsuarios.getModel();            
        
        // obtenemos la fila y columna pulsados
        int fila = tUsuarios.rowAtPoint(evt.getPoint());
        int columna = tUsuarios.columnAtPoint(evt.getPoint());
        
        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id del usuario
            int idusuario = (int) modeloTabla.getValueAt(fila, 0);

            // llamamos al constructor del formulario
            FormUsuarios Usuario = new FormUsuarios(idusuario);
            Usuario.setVisible(true);
            
            // cerramos este formulario
            this.dispose();
            
        }
              
    }

}
