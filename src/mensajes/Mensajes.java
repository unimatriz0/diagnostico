/*

    Nombre: mensajes
    Fecha: 18/03/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que recibe como constructor un mensaje a presentar
                 e instancia el formulario

 */

// definición del paquete
package mensajes;

// importación de librerías
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * @author iceman
 */

// definición de la clase
public class Mensajes {
    
    // definición de las variables 
    FormMensajes Atencion;
    Timer Temporizador;
    
    /**
     * @param mensaje String con el mensaje a presentar
     * Constructor de la clase, recibe como parámetro la cadena de texto
     * con el mensaje a presentar
     */
    public Mensajes(String mensaje){

        // instanciamos el formulario y le pasamos el mensaje
        this.Atencion = new FormMensajes(null, false);
        this.Atencion.setMensaje(mensaje);
        this.Atencion.setVisible(true);

        Temporizador = new Timer(5000, new ActionListener() {
                           public void actionPerformed(ActionEvent ev) {

                               // cerramos el mensaje
                               cerrarMensaje();
                               
                           }});
        
        // iniciamos el timer
        Temporizador.start();
        
    }
    
    // método protegido que destruye el formulario
    public void cerrarMensaje(){
    
        // detiene el temporizador
        Temporizador.stop();
        
        // destruye el formulario
        this.Atencion.dispose();
        
    }
    
}
