/*

    Nombre: mensajes
    Fecha: 17/03/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Simple clase que presenta un mensaje y luego se oculta 
                 automaticamente

 */

// declaracion del paquete
package mensajes;

/**
 *@author iceman
 */
public class FormMensajes extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
	
    // declaracion de variables
    private javax.swing.JLabel tMensaje;
    
    /**
     * @param parent el formulario padre
     * @param modal verdadero o falso
     * Creamos el formulario
     */
    public FormMensajes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);

        // fijamos las propiedades
        this.setAlwaysOnTop(true);
        this.setAutoRequestFocus(false);
        this.setUndecorated(true);
        this.setResizable(false);
        this.setBounds(900, 50, 200, 50);
                
        // declaramos el label del mensaje
        this.tMensaje = new javax.swing.JLabel();
        this.tMensaje.setBounds(10,15,180,20);
        this.add(this.tMensaje);

    }
    
    
    /**
     * @param texto una cadena con el titulo
     * Metodo publico que fija el texto a mostrar
     */
    public void setMensaje(String texto){

        // fijamos el texto del mensaje
        this.tMensaje.setText(texto);
                
    }

}
