/*
 *
 * Nombre: FormPacientes
 * Fecha: 05/01/2019
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Proyecto: Diagnostico
 * Licencia: GPL
 * Producido en: INP - Dr. Mario Fatala Chaben
 * Buenos Aires - Argentina
 * Comentarios: Formulario de presentación de datos de los pacientes
 *
 */

// definimos el paquete
package pacientes;

// importamos la librería
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

// definición de la clase
public class FormPacientes extends JPanel {

	// establece el serial id
	private static final long serialVersionUID = 1L;

	// declaramos las variables
	protected JPanel DatosPacientes;
	
	// creamos el panel, recibe como parámetro el contendor
	public FormPacientes(JTabbedPane Tabulador) {

		// inicializamos el contenedor
		this.DatosPacientes = new JPanel();
		this.DatosPacientes.setBounds(0, 0, 945, 620);
		this.DatosPacientes.setLayout(null);
		this.DatosPacientes.setToolTipText("Datos de los Pacientes");

		 // agregamos el panel al tabulador
        Tabulador.addTab("Pacientes", 
                          new javax.swing.ImageIcon(getClass().getResource("/Graficos/mCapsule.png")), 
                          this.DatosPacientes, 
                          "Datos de los pacientes");
		
	}

}
