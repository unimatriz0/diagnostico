/*

    Nombre: marcas
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: serie de procedimientos que controlan el abm de las 
                 marcas reconocidas del sistema

 */

// definición del paquete
package marcas;

//importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

/**
 * @author Lic. Claudio Invernizzi
 */

// declaración de la clase
public class Marcas {

    // definición de variables
    protected Conexion Enlace;
    
    // definición de las variables de la base de datos
    protected int IdMarca;                // clave de la marca
    protected String Marca;               // nombre de la marca
    protected int IdTecnica;              // clave de la técnica
    protected String Tecnica;             // nombre de la técnica
    protected String Usuario;             // nombre del usuario
    protected String FechaAlta;           // fecha de alta del registro
    
    /** 
     * Constructor de la clase
     */
    public Marcas(){
    
        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initMarcas();
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initMarcas(){
    	
        // inicializamos las variables de clase
        this.IdMarca = 0;
        this.Marca = null;
        this.IdTecnica = 0;
        this.Tecnica = "";
        this.Usuario = "";
        this.FechaAlta = "";
    	
    }
    
    // métodos de asignación de variables
    public void setIdMarca(int idmarca){
        this.IdMarca = idmarca;
    }
    public void setIdTecnica(int idtecnica){
        this.IdTecnica = idtecnica;
    }
    public void setMarca(String marca){
        this.Marca = marca;
    }

    // métodos de retorno de valores
    public int getIdMarca(){
    	return this.IdMarca;
    }
    public String getMarca(){
    	return this.Marca;
    }
    public int getIdTecnica(){
    	return this.IdTecnica;
    }
    public String getTecnica(){
    	return this.Tecnica;
    }
    public String getUsuario(){
    	return this.Usuario;
    }
    public String getFechaAlta(){
    	return this.FechaAlta;
    }
    
    /** 
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtecnica clave de una técnica
     * @return resultset con la nómina de marcas de esa técnica
       Método que retorna el listado de las marcas, recibe como parámetro
       el nombre de la técnica, retorna la id de la marca, que
       es utilizada en el abm de marcas
     */
    public ResultSet nominaMarcas(int idtecnica){

        // declaración de variables
        String Consulta;
        ResultSet listaMarcas;
        
        // componemos la consulta
        Consulta = "SELECT diagnostico.v_marcas.id_marca AS idmarca, "
                 + "       diagnostico.v_marcas.marca AS marca "
                 + "FROM diagnostico.v_marcas "
                 + "WHERE diagnostico.v_marcas.id_tecnica = '" + idtecnica + "' "
                 + "ORDER BY diagnostico.v_marcas.marca;";

        // ejecutamos la consulta
        listaMarcas = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return listaMarcas;

    }

    /** 
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param tecnica cadena con la técnica
     * @param marca cadena con la marca
     * @return entero con la clave de la marca
     * método que retorna la clave de una marca, recibe como parámetro
     * la id de la técnica y la cadena con la marca
     */
    public int getClaveMarca(String tecnica, String marca){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int claveMarca = 0;
        
        // componemos la consulta
        Consulta = "SELECT diagnostico.v_marcas.id_marca AS id_marca "
                 + "FROM diagnostico.v_marcas "
                 + "WHERE diagnostico.v_marcas.tecnica = '" + tecnica + "' AND "
                 + "      diagnostico.v_marcas.marca = '" + marca + "'; ";

        // ejecutamos la consulta y obtenemos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            claveMarca = Resultado.getInt("id_marca");
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos la clave de la marca
        return claveMarca;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro afectado
     * Función que actualiza la base de datos de marcas
     */
    public int grabaMarca(){

    	// si está insertando
    	if (this.IdMarca == 0){
    		this.nuevaMarca();
    	} else {
    		this.editaMarca();
    	}
    	
    	// retornamos la clave
    	return this.IdMarca;
    	        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaMarca(){

    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
        // compone la consulta de inserción
        Consulta = "INSERT INTO diagnostico.marcas "
                + "        (MARCA, "
                + "         TECNICA,"
                + "         USUARIO)"
                + "        VALUES"
                + "        (?,?,?); ";

        // obtenemos la id del registro
        this.IdMarca = this.Enlace.UltimoInsertado();

    	try {
        	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Marca);
	        preparedStmt.setInt    (2, this.IdTecnica);
	        preparedStmt.setInt    (3, Seguridad.Id);
	        
	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	        // obtenemos la id
	        this.IdMarca = this.Enlace.UltimoInsertado();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaMarca(){

    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
        // compone la consulta de edición
        Consulta = "UPDATE diagnostico.marcas SET "
                + "        MARCA = ?, "
                + "        TECNICA = ?, "
                + "        USUARIO = ? "
                + " WHERE diagnostico.marcas.ID = ?; ";

    	try {
        	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Marca);
	        preparedStmt.setInt    (2, this.IdTecnica);
	        preparedStmt.setInt    (3, Seguridad.Id);
	        preparedStmt.setInt    (4, this.IdMarca);
	        
	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un registro y 
     * asigna en las variables de clase los valores del mismo
     */
    public void getDatosMarca(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        
        // componemos la consulta
        Consulta = "SELECT diagnostico.v_marcas.id_marca AS idmarca, " +
                   "       diagnostico.v_marcas.marca AS marca, " +
        		   "       diagnostico.v_marcas.id_tecnica AS id_tecnica, " +
                   "       diagnostico.v_marcas.tecnica AS tecnica, " +
        		   "       diagnostico.v_marcas.usuario AS usuario, " +
                   "       diagnostico.v_marcas.fecha_alta AS fecha_alta " +
        		   "FROM diagnostico.v_marcas " + 
                   "WHERE diagnostico.v_marcas.id_marca = '" + clave + "';";

        // ejecutamos la consulta y obtenemos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // lo asignamos a las variables de clase
            this.IdMarca = Resultado.getInt("idmarca");
            this.Marca = Resultado.getString("marca");
            this.IdTecnica = Resultado.getInt("id_tecnica");
            this.Tecnica = Resultado.getString("tecnica");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
    	
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param marca - cadena con la marca
     * @param tecnica - clave de la tecnica
     * @return boolean - el resultado de la operación
     * Método que verifica no se esté ingresando una marca
     * repetida
     */
    public boolean validaMarca(String marca, int tecnica){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;
        
        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.marcas.id) AS registros " +
        		   "FROM diagnostico.marcas " + 
                   "WHERE diagnostico.marcas.marca = '" + marca + "' AND " + 
        		   "      diagnostico.marcas.tecnica = '" + tecnica + "';";

        // ejecutamos la consulta y obtenemos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hubo registros
            if (Resultado.getInt("registros") == 0){
            	Correcto = true;
            } else {
            	Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
    	
        // retornamos el estado
        return Correcto;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que ejecuta la consulta de eliminación de una marca
     */
    public void borraMarca(int clave){
    	
    	// declaración de variables
    	String Consulta;
    	
    	// componemos y ejecutamos la consulta
    	Consulta = "DELETE FROM diagnostico.marcas " +
    	           "WHERE diagnostico.marcas.id = '" + clave + "';";
    	this.Enlace.Ejecutar(Consulta);
    	
    }
    
}
