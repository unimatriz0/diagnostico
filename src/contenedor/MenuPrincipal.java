/*

    Nombre: MenuPrincipal
    Fecha: 05/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que carga el menú principal de la aplicación 

 */

// definimos el paquete
package contenedor;

// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JPopupMenu.Separator;
import seguridad.Seguridad;

// declaración de la clase
public class MenuPrincipal {

	// definimos las variables
	private Acciones Opciones;
	
	// constructor de la clase que es el que genera el menú, recibe como 
	// parámetro el frame sobre el que se alica
	public MenuPrincipal(JFrame Contenedor) {
		
		// instanciamos las acciones del menú
		this.Opciones = new Acciones(Contenedor);
		
        // define la barra de menu
        JMenuBar barraMenu = new JMenuBar();

        // define el menu de usuarios
        JMenu mUsuarios = new JMenu("Usuarios");
        mUsuarios.setIcon(new ImageIcon(getClass().getResource("/Graficos/musuarios.png")));

        // el elemento nuevo usuario
        JMenuItem mNuevoUsuario = new JMenuItem("Nuevo Usuario");
        mNuevoUsuario.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        mNuevoUsuario.setToolTipText("Ingresa un nuevo usuario en la base");
        mNuevoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.nuevoUsuario();
            }

        });
        mUsuarios.add(mNuevoUsuario);

        // el elemento buscar usuario
        JMenuItem mBuscaUsuario = new JMenuItem("Buscar");
        mBuscaUsuario.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbinoculares.png")));
        mBuscaUsuario.setToolTipText("Busca un usuario en la base");
        mBuscaUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.buscarUsuario();
            }
        });
        mUsuarios.add(mBuscaUsuario);

        // el separador
        Separator jSeparator1 = new JPopupMenu.Separator();
        mUsuarios.add(jSeparator1);

        // el elemento nuevo password
        JMenuItem mPassword = new JMenuItem("Cambiar Contraseña");
        mPassword.setIcon(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
        mPassword.setToolTipText("Cambia su contraseña de ingreso");
        mPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.cambiarPassword();
            }
        });
        mUsuarios.add(mPassword);

        // agregamos otro separador
        Separator jSeparator5 = new JPopupMenu.Separator();
        mUsuarios.add(jSeparator5);

        // el menú configuración de la interfaz
        JMenuItem mTema = new JMenuItem("Tema de Interfaz");
        mTema.setIcon(new ImageIcon(getClass().getResource("/Graficos/minstrumentos1.png")));
        mTema.setToolTipText("Configurar la Apariencia");
        mTema.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.Configurar();
            }
        });
        mUsuarios.add(mTema);

        // agregamos otro separador
        Separator jSeparator2 = new JPopupMenu.Separator();
        mUsuarios.add(jSeparator2);

        // el menú salir
        JMenuItem mSalir = new JMenuItem("Salir");
        mSalir.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
        mSalir.setToolTipText("Salir de la Aplicación");
        mSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.Salir();
            }
        });
        mUsuarios.add(mSalir);

        // agregamos el menú a la barra
        barraMenu.add(mUsuarios);

        // definimos el menú de pacientes
        JMenu mPacientes = new JMenu("pacientes");
        mPacientes.setIcon(new ImageIcon(getClass().getResource("/Graficos/mCapsule.png")));

        // el submenú de nuevo paciente
        JMenuItem mNuevoPaciente = new JMenuItem("Nuevo");
        mNuevoPaciente.setToolTipText("Ingresa un nuevo Paciente en la Base");
        mNuevoPaciente.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        mPacientes.add(mNuevoPaciente);

        // el submenú de buscar paciente
        JMenuItem mBuscaPaciente = new JMenuItem("Buscar");
        mBuscaPaciente.setToolTipText("Busca un Paciente en la Base");
        mBuscaPaciente.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        mPacientes.add(mBuscaPaciente);

        // el submenú de congénito
        JMenuItem mCongenito = new JMenuItem("Congénito");
        mCongenito.setToolTipText("Chagas Congénito");
        mCongenito.setIcon(new ImageIcon(getClass().getResource("/Graficos/mEmbryo.png")));
        mPacientes.add(mCongenito);

        // el submenú de reportes
        JMenuItem mReportesPacientes = new JMenuItem("Reportes");
        mReportesPacientes.setToolTipText("Reportes de pacientes");
        mReportesPacientes.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdownload_server.png")));
        mPacientes.add(mReportesPacientes);

        // agregamos el menú a la barra
        barraMenu.add(mPacientes);

        // definimos el menú de determinaciones
        JMenu mDeterminaciones = new JMenu("Determinaciones");
        mDeterminaciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mblood.png")));

        // el submenú de nueva determinación
        JMenuItem mNuevaDeterminacion = new JMenuItem("Nueva");
        mNuevaDeterminacion.setToolTipText("Ingresa una Determinación en la Base");
        mNuevaDeterminacion.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        mDeterminaciones.add(mNuevaDeterminacion);

        // el submenú de buscar determinación
        JMenuItem mBuscarDeterminacion = new JMenuItem("Buscar");
        mBuscarDeterminacion.setToolTipText("Busca una determinación");
        mBuscarDeterminacion.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        mDeterminaciones.add(mBuscarDeterminacion);

        // el submenú de reportes
        JMenuItem mReportesDeterminaciones = new JMenuItem("Reportes");
        mReportesDeterminaciones.setToolTipText("Reportes de Determinaciones");
        mReportesDeterminaciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdownload_server.png")));
        mDeterminaciones.add(mReportesDeterminaciones);

        // agregamos el menú a la barra
        barraMenu.add(mDeterminaciones);

        // definimos el menú de laboratorios
        JMenu mLaboratorios = new JMenu("Laboratorios");
        mLaboratorios.setIcon(new ImageIcon(getClass().getResource("/Graficos/mSyringe.png")));

        // el submenú de nuevo laboratorio
        JMenuItem mNuevoLaboratorio = new JMenuItem("Nuevo");
        mNuevoLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        mNuevoLaboratorio.setToolTipText("Agrega un Laboratorio a la Base");
        mLaboratorios.add(mNuevoLaboratorio);

        // el submenú de buscar laboratorio
        JMenuItem mBuscarLaboratorio = new JMenuItem("Buscar");
        mBuscarLaboratorio.setToolTipText("Busca un Laboratorio en la Base");
        mBuscarLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        mLaboratorios.add(mBuscarLaboratorio);

        // el submenú de reportes
        JMenuItem mReportesLaboratorios = new JMenuItem("Reportes");
        mReportesLaboratorios.setToolTipText("Reportes de aboratorios");
        mReportesLaboratorios.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdownload_server.png")));
        mLaboratorios.add(mReportesLaboratorios);

        // agregamos el menú a la barra
        barraMenu.add(mLaboratorios);

        // definimos el menú de instituciones asistenciales
        JMenu mInstituciones = new JMenu("instituciones");
        mInstituciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mCaduceus.png")));

        // el submenú de nueva institución
        JMenuItem mNuevaInstitucion = new JMenuItem("Nueva");
        mNuevaInstitucion.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        mNuevaInstitucion.setToolTipText("Agrega una Institución a la Base");
        mInstituciones.add(mNuevaInstitucion);

        // el submenú de buscar institución
        JMenuItem mBuscarInstitucion = new JMenuItem("Buscar");
        mBuscarInstitucion.setToolTipText("Buscar una Institución Asistencial");
        mBuscarInstitucion.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        mInstituciones.add(mBuscarInstitucion);

        // el submenú de reportes
        JMenuItem mReportesInstituciones = new JMenuItem("Reportes");
        mReportesInstituciones.setToolTipText("Reportes de instituciones Asistenciales");
        mReportesInstituciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdownload_server.png")));
        mInstituciones.add(mReportesInstituciones);

        /// agregamos el menú a la barra
        barraMenu.add(mInstituciones);

        // definimos el menú de stock
        JMenu mStock = new JMenu("stock");
        mStock.setIcon(new ImageIcon(getClass().getResource("/Graficos/mestadistica.png")));

        // definimos el submenú de inventario
        JMenuItem mInventario = new JMenuItem("Inventario");
        mInventario.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbase-de-datos.png")));
        mInventario.setToolTipText("Existencias en el depósito");
        mInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verInventario();
            }
        });
        mStock.add(mInventario);

        // definimos el submenú de ingresos
        JMenuItem mIngresos = new JMenuItem("Ingresos");
        mIngresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmas.png")));
        mIngresos.setToolTipText("Ingresos al Depósito");
        mIngresos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verIngresos();
            }
        });
        mStock.add(mIngresos);

        // definimos el submenú de egresos
        JMenuItem mEgresos = new JMenuItem("Egresos");
        mEgresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmaquina-de-escribir.png")));
        mEgresos.setToolTipText("Egresos del Depósito");
        mEgresos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verEgresos();
            }
        });
        mStock.add(mEgresos);

        // definimos el submenú del diccionario
        JMenuItem mItems = new JMenuItem("Elementos");
        mItems.setIcon(new ImageIcon(getClass().getResource("/Graficos/majustes.png")));
        mItems.setToolTipText("Diccionario de elementos");
        mItems.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verElementos();
            }
        });
        mStock.add(mItems);

        // agregamos el stock a la barra
        barraMenu.add(mStock);

        // definimos el menú de configuración
        JMenu mConfiguracion = new JMenu("Configuración");
        mConfiguracion.setIcon(new ImageIcon(getClass().getResource("/Graficos/minstrumentos1.png")));

        // agregamos el submenú de técnicas
        JMenuItem mTecnicas = new JMenuItem("Técnicas");
        mTecnicas.setToolTipText("Diccionario de Técnicas Utilizadas");
        mTecnicas.setIcon(new ImageIcon(getClass().getResource("/Graficos/manalitica.png")));
        mTecnicas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verTecnicas();
            }
        });
        mConfiguracion.add(mTecnicas);

        // agregamos el submenú de marcas
        JMenuItem mMarcas = new JMenuItem("Marcas");
        mMarcas.setToolTipText("Diccionario de marcas de Reactivos");
        mMarcas.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador.png")));
        mMarcas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verMarcas();
            }
        });
        mConfiguracion.add(mMarcas);

        // agregamos el submenú de valores
        JMenuItem mValores = new JMenuItem("Valores");
        mValores.setToolTipText("Valores aceptados para cada Técnica");
        mValores.setIcon(new ImageIcon(getClass().getResource("/Graficos/manalitica1.png")));
        mValores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verValores();
            }
        });
        mConfiguracion.add(mValores);

        // agregamos el submenú de países
        JMenuItem mPaises = new JMenuItem("Países");
        mPaises.setToolTipText("Diccionario de Países");
        mPaises.setIcon(new ImageIcon(getClass().getResource("/Graficos/mubicacion.png")));
        mPaises.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verPaises();
            }
        });
        mConfiguracion.add(mPaises);

        // agregamos el submenú de provincias
        JMenuItem mJurisdicciones = new JMenuItem("Jurisdicciones");
        mJurisdicciones.setToolTipText("Diccionario de Jurisdicciones");
        mJurisdicciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador-de-posicion.png")));
        mJurisdicciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verJurisdicciones();
            }
        });
        mConfiguracion.add(mJurisdicciones);

        // agregamos el submenú de localidades
        JMenuItem mLocalidades = new JMenuItem("Localidades");
        mLocalidades.setToolTipText("Diccionario de localidades");
        mLocalidades.setIcon(new ImageIcon(getClass().getResource("/Graficos/mMapa.png")));
        mLocalidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verLocalidades();
            }
        });
        mConfiguracion.add(mLocalidades);

        // agregamos el submenú de dependencias
        JMenuItem mDependencias = new JMenuItem("Dependencias");
        mDependencias.setToolTipText("Tipos de dependencia institucional");
        mDependencias.setIcon(new ImageIcon(getClass().getResource("/Graficos/mHospital.png")));
        mDependencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verDependencias();
            }
        });
        mConfiguracion.add(mDependencias);

        // agregamos el submenú de fuentes de financiamiento
        JMenuItem mFinanciamiento = new JMenuItem("Financiamiento");
        mFinanciamiento.setToolTipText("Fuentes de financiamiento");
        mFinanciamiento.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbillete.png")));
        mFinanciamiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verFinanciamiento();
            }
        });
        mConfiguracion.add(mFinanciamiento);

        // agregamos el submenú de motivos de consulta
        JMenuItem mMotivos = new JMenuItem("Motivos de Consulta");
        mMotivos.setToolTipText("Diccionario de motivos de consulta");
        mMotivos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mnurse.png")));
        mMotivos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verMotivos();
            }
        });
        mConfiguracion.add(mMotivos);

        // agregamos el submenú de enfermedades
        JMenuItem mEnfermedades = new JMenuItem("Patologías");
        mEnfermedades.setToolTipText("Diccionario de patologías");
        mEnfermedades.setIcon(new ImageIcon(getClass().getResource("/Graficos/mAmbulancia.png")));
        mEnfermedades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verEnfermedades();
            }
        });
        mConfiguracion.add(mEnfermedades);

        // agregamos el submenú de organos
        JMenuItem mOrganos = new JMenuItem("Organos");
        mOrganos.setToolTipText("Diccionario de Organos de Transplante");
        mOrganos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mtransplante.png")));
        mOrganos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verOrganos();
            }
        });
        mConfiguracion.add(mOrganos);

        // agregamos el submenú de tipos de documentos
        JMenuItem mDocumentos = new JMenuItem("Documentos");
        mDocumentos.setToolTipText("Tipos de documentos registrados");
        mDocumentos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mMaletin.png")));
        mDocumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verDocumentos();
            }
        });
        mConfiguracion.add(mDocumentos);

        // agregamos el submenú de celulares
        JMenuItem mCelulares = new JMenuItem("Celulares");
        mCelulares.setToolTipText("Diccionario de Compañías de Celular");
        mCelulares.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdownload_server.png")));
        mCelulares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verCelulares();
            }
        });
        mConfiguracion.add(mCelulares);

        // agregamos el submenú de heladeras
        JMenuItem mHeladeras = new JMenuItem("Heladeras");
        mHeladeras.setToolTipText("Heladeras y Freezers del Laboratorio");
        mHeladeras.setIcon(new ImageIcon(getClass().getResource("/Graficos/mEscanerbarras.png")));
        mHeladeras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verHeladeras();
            }
        });
        mConfiguracion.add(mHeladeras);

        // agregamos el submenú de temperaturas
        JMenuItem mTemperaturas = new JMenuItem("Temperaturas");
        mTemperaturas.setToolTipText("Registro de temperaturas");
        mTemperaturas.setIcon(new ImageIcon(getClass().getResource("/Graficos/mTermometro.png")));
        mTemperaturas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.verTemperaturas();
            }
        });
        mConfiguracion.add(mTemperaturas);

        // agregamos el menú a la barra
        barraMenu.add(mConfiguracion);

        // agregamos la barra al formulario
        Contenedor.setJMenuBar(barraMenu);

        // si no es de nivel central
        if (!"Si".equals(Seguridad.NivelCentral)){
            mConfiguracion.setEnabled(false);
        }

        // si no puede controlar stock
        if (!"Si".equals(Seguridad.Stock)){
            mIngresos.setEnabled(false);
            mEgresos.setEnabled(false);
            mItems.setEnabled(false);
        }

        // si no está autorizado
        if (!"Si".equals(Seguridad.Usuarios)) {
            mNuevoUsuario.setEnabled(false);
        }

	
	}
	
}
