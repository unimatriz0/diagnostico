/*

    Nombre: FormContenedor
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que arma la pantalla principal con el menú y el 
                 tabulador

 */

// definición del paquete
package contenedor;

// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import Instituciones.FormInstituciones;
import laboratorios.FormLaboratorios;
import determinaciones.FormDeterminaciones;
import pacientes.FormPacientes;
import javax.swing.JTabbedPane;

// definición de la clase
public class FormContenedor extends JFrame {

	// agregamos el serial id
	private static final long serialVersionUID = 9178063416534321878L;

	// declaramos las variables
	JTabbedPane tabDatos;
	FormInstituciones Hospitales;
	FormLaboratorios Laboratorios;
	FormPacientes Pacientes;
	FormDeterminaciones Determinaciones;
	
	
	/**
     * Constructor de la clase
     */
    public FormContenedor () {

        // establecemos las propiedades del formulario
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 20, 950, 700);
        this.setTitle("Sistema de Laboratorio y Seguimiento");
        this.setLayout(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/Graficos/logo_fatala.png")).getImage());

        // cargamos el menú del sistema
        new MenuPrincipal(this);

        // cargamos la barra de herramientas
        new BarraHerramientas(this);

        // definimos el tabulador y lo posicionamos
        this.tabDatos = new JTabbedPane();
        this.tabDatos.setBounds(0,30,945,620);
        
        // cargamos el formulario de pacientes
        this.Pacientes = new FormPacientes(this.tabDatos);
        
        // cargamos el formulario de toma de muestras
        
        // cargamos el formulario de determinaciones
        this.Determinaciones = new FormDeterminaciones(this.tabDatos);
        
        // cargamos el formulario de instituciones asistenciales
        this.Hospitales = new FormInstituciones(this.tabDatos);
        
        // cargamos el formulario de laboratorios
        this.Laboratorios = new FormLaboratorios(this.tabDatos);
        
        // agregamos el tabulador
        getContentPane().add(this.tabDatos);
        
    }

}
