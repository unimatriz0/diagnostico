/*

    Nombre: BarraHerramientas
    Fecha: 05/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que carga la barra de herramientas en el frame 

 */

// definición del paquete
package contenedor;

//importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import seguridad.Seguridad;

// declaración de la clase
public class BarraHerramientas {

	// definimos las variables de clase
	private Acciones Opciones;
	
	// constructor de la clase
	public BarraHerramientas(JFrame Contenedor) {
		
		// instanciamos las acciones
		this.Opciones = new Acciones(Contenedor);
		
        JToolBar Herramientas = new JToolBar();
        Herramientas.setRollover(true);

        // el boton nuevo usuario
        JButton btnNuevoUsuario = new JButton();
        btnNuevoUsuario.setIcon(new ImageIcon(getClass().getResource("/Graficos/usuarios.png")));
        btnNuevoUsuario.setToolTipText("Agrega un nuevo usuario");
        btnNuevoUsuario.setFocusable(false);
        btnNuevoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.nuevoUsuario();
            }
        });
        Herramientas.add(btnNuevoUsuario);

        // agregamos el botón buscar paciente
        JButton btnBuscarPaciente = new JButton();
        btnBuscarPaciente.setIcon(new ImageIcon(getClass().getResource("/Graficos/Capsule.png")));
        btnBuscarPaciente.setToolTipText("Busca un paciente en la base");
        btnBuscarPaciente.setFocusable(false);
        Herramientas.add(btnBuscarPaciente);

        // agregamos el botón determinaciones
        JButton btnDeterminaciones = new JButton();
        btnDeterminaciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/blood.png")));
        btnDeterminaciones.setToolTipText("Determinaciones de pacientes");
        btnDeterminaciones.setFocusable(false);
        Herramientas.add(btnDeterminaciones);

        // agregamos el botón de chagas congénito
        JButton btnCongenito = new JButton();
        btnCongenito.setIcon(new ImageIcon(getClass().getResource("/Graficos/Embryo.png")));
        btnCongenito.setToolTipText("Chagas Congénito");
        btnCongenito.setFocusable(false);
        Herramientas.add(btnCongenito);

        // agregamos el botón buscar laboratorio
        JButton btnBuscarLaboratorio = new JButton();
        btnBuscarLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/Syringe.png")));
        btnBuscarLaboratorio.setToolTipText("Busca un Laboratorio en la base");
        btnBuscarLaboratorio.setFocusable(false);
        Herramientas.add(btnBuscarLaboratorio);

        // agregamos el botón buscar institución
        JButton btnBuscarInstitucion = new JButton();
        btnBuscarInstitucion.setIcon(new ImageIcon(getClass().getResource("/Graficos/Caduceus.png")));
        btnBuscarInstitucion.setToolTipText("Busca una Institución Asistencial");
        btnBuscarInstitucion.setFocusable(false);
        Herramientas.add(btnBuscarInstitucion);

        // el botón inventario
        JButton btnInventario = new JButton();
        btnInventario.setToolTipText("Muestra la existencia en Depósito");
        btnInventario.setIcon(new ImageIcon(getClass().getResource("/Graficos/base-de-datos.png")));
        btnInventario.setFocusable(false);
        Herramientas.add(btnInventario);

        // el botón salir
        JButton btnSalir = new JButton();
        btnSalir.setToolTipText("Salir de la Aplicación");
        btnSalir.setIcon(new ImageIcon(getClass().getResource("/Graficos/salida.png")));
        btnSalir.setFocusable(false);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Opciones.Salir();
            }
        });
        Herramientas.add(btnSalir);

        // fijamos el tamaño, la posición y agregamos la barra al formulario
        Herramientas.setBounds(0, 0, 900, 30);
        Contenedor.add(Herramientas);

        // si no puede agregar usuarios
        if (!"Si".equals(Seguridad.Usuarios)) {
            btnNuevoUsuario.setEnabled(false);
        }
		
	}
	
}
