/*

    Nombre: Acciones
    Fecha: 05/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que provee los métodos de los eventos del menú y 
                 de la barra de herramientas

 */

// declaración del paquete
package contenedor;

// importamos las librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import celulares.FormCelulares;
import dbApi.formConfig;
import dependencias.FormDependencias;
import documentos.FormDocumentos;
import enfermedades.FormEnfermedades;
import financiamiento.FormFinanciamiento;
import heladeras.FormHeladeras;
import localidades.FormLocalidades;
import marcas.FormMarcas;
import motivos.FormMotivos;
import organos.FormOrganos;
import paises.FormPaises;
import provincias.FormProvincias;
import seguridad.FormNuevoPass;
import stock.FormGrillaEgresos;
import stock.FormGrillaIngresos;
import stock.FormGrillaInventario;
import stock.FormGrillaItems;
import tecnicas.FormTecnicas;
import temperaturas.FormTemperaturas;
import usuarios.Usuarios;
import usuarios.FormGrillaUsuarios;
import usuarios.FormUsuarios;
import valores.FormValores;

// importamos las librerías
public class Acciones {

	// declaración de variables de clase
	JFrame Formulario;
	
	// el constructor recibe como parámetro el frame
	public Acciones(JFrame Contenedor) {
	
		// asignamos en la variable de clase
		this.Formulario = Contenedor;
		
	}
	
    // método llamado al pulsar el botón salir
    public void Salir(){

        // directamente eliminamos el formulario
        this.Formulario.dispose();

    }

    // método llamado al pulsar sobre cambiar contraseña
    public void cambiarPassword() {

        // instanciamos el formulario y lo mostramos
        FormNuevoPass nuevoPassword = new FormNuevoPass(this.Formulario, true);
        nuevoPassword.setVisible(true);

    }

    // método llamado al pulsar sobre nuevo usuario
    public void nuevoUsuario() {

        // instanciamos el formulario y lo mostramos
        FormUsuarios altaUsuario = new FormUsuarios();
        altaUsuario.setVisible(true);

    }

    // método llamado al pulsar sobre buscar usuario
    public void buscarUsuario() {

        // pedimos el texto a buscar
        String texto = JOptionPane.showInputDialog(this,
                        "Ingrese el texto a buscar, el sistema explorará en el\n"
                                        + "nombre, la institución, la localidad y la jurisdicción");

        // si pulsó aceptar
        if (texto != null) {

            // instanciamos la clase de usuarios
            Usuarios Responsables = new Usuarios();
            ResultSet listaUsuarios = Responsables.buscaUsuario(texto);

            try {

                // nos movemos al último registro
                listaUsuarios.last();

                // si no hubo resultados
                if (listaUsuarios.getRow() == 0) {

                    // presenta el mensaje
                    JOptionPane.showMessageDialog(this.Formulario,
                                    "No hay Usuarios que cumplan con el\n"
                                                    + "criterio ingresado ",
                                    "Error", JOptionPane.ERROR_MESSAGE);

                    // si hay registros
                } else {

                    // instancia la grilla
                    FormGrillaUsuarios grillaUsuarios = new FormGrillaUsuarios(this.Formulario, true);

                    // presenta los datos
                    grillaUsuarios.muestraUsuarios(listaUsuarios);

                    // lo mostramos
                    grillaUsuarios.setVisible(true);

                }

                // si hubo un error
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }

        }

    }

    // mostramos el formulario de técnicas
    public void verTecnicas(){

        // simplemente lo mostramos
        FormTecnicas Tecnicas = new FormTecnicas(this.Formulario,true);
        Tecnicas.setVisible(true);

    }

    // mostramos el formulario de valores
    public void verValores(){

        // simplemente lo mostramos
        FormValores Valores = new FormValores(this.Formulario, true);
        Valores.setVisible(true);

    }

    // mostramos el formulario de paises
    public void verPaises() {

        // simplemente lo mostramos
        FormPaises Paises = new FormPaises(this.Formulario, true);
        Paises.setVisible(true);

    }

    // mostramos el formulario de jurisdicciones
    public void verJurisdicciones() {

        // simplemente lo mostramos
        FormProvincias Provincias = new FormProvincias(this.Formulario, true);
        Provincias.setVisible(true);

    }

    // mostramos el formulario de localidades
    public void verLocalidades() {

        // simplemente lo mostramos
        FormLocalidades Localidades = new FormLocalidades(this.Formulario, true);
        Localidades.setVisible(true);

    }

    // mostramos el formulario de heladeras
    public void verHeladeras(){
    	FormHeladeras Heladeras = new FormHeladeras(this.Formulario, true);
    	Heladeras.setVisible(true);
    }

    // mostramos el formulario de dependencias
    public void verDependencias() {
        FormDependencias Dependencias = new FormDependencias(this.Formulario, true);
        Dependencias.setVisible(true);
    }

    // mostramos el formulario de celulares
    public void verCelulares() {
        FormCelulares Celulares = new FormCelulares(this.Formulario, true);
        Celulares.setVisible(true);
    }

    // mostramos el formulario de enfermedades
    public void verMotivos() {
        FormMotivos Motivos = new FormMotivos(this.Formulario, true);
        Motivos.setVisible(true);
    }

    // mostramos el formulario de enfermedades
    public void verEnfermedades() {
        FormEnfermedades Enfermedades = new FormEnfermedades(this.Formulario, true);
        Enfermedades.setVisible(true);
    }

    // mostramos el formulario de órganos
    public void verOrganos() {
        FormOrganos Organos = new FormOrganos(this.Formulario, true);
        Organos.setVisible(true);
    }

    // mostramos el formulario de enfermedades
    public void verFinanciamiento() {
        FormFinanciamiento Financiamiento = new FormFinanciamiento(this.Formulario, true);
        Financiamiento.setVisible(true);
    }

    // mostramos el formulario de tipos de documento
    public void verDocumentos(){
        FormDocumentos Documentos = new FormDocumentos(this.Formulario, true);
        Documentos.setVisible(true);
    }

    // mostramos el formulario de temperaturas
    public void verTemperaturas(){
        FormTemperaturas Temperaturas = new FormTemperaturas(this.Formulario, true);
        Temperaturas.setVisible(true);
    }

    // mostramos el formulario de marcas
    public void verMarcas(){
        FormMarcas Marcas = new FormMarcas(this.Formulario, true);
        Marcas.setVisible(true);
    }

    // mostramos el formulario de items del stock
    public void verElementos(){
        FormGrillaItems Items = new FormGrillaItems(this.Formulario, true);
        Items.setVisible(true);
    }

    // mostramos el formulario de egresos
    public void verEgresos(){
        FormGrillaEgresos Egresos = new FormGrillaEgresos(this.Formulario, true);
        Egresos.setVisible(true);
    }

    // mostramos el formulario de ingresos
    public void verIngresos(){
        FormGrillaIngresos Ingresos = new FormGrillaIngresos(this.Formulario, true);
        Ingresos.setVisible(true);
    }

    // mostramos el formulario de inventario
    public void verInventario(){
        FormGrillaInventario Inventario = new FormGrillaInventario(this.Formulario, true);
        Inventario.setVisible(true);
    }

    // cargamos el formulario de configuración
    public void Configurar(){
        formConfig Configuracion = new formConfig(this.Formulario, true);
        Configuracion.setVisible(true);
    }
	
}
