/*

    Nombre: valores
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: métodos que controlan el abm y listados de la tabla de
                 valores de las tècnicas  diagnósticas

 */

// declaraciòn del paquete
package valores;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Declaraciòn de la clase
 */
public class Valores {

    // variables de la tabla de valores aceptados
    protected int IdValor;                  // clave del registro
    protected int IdTecnica;                // clave de la técnica
    protected String Tecnica;               // nombre de la técnica
    protected String Valor;                 // valor aceptado
    protected String Usuario;               // nombre del usuario
    protected String FechaAlta;             // fecha de alta del registro

    // definición de variables
    protected Conexion Enlace;

    /**
     * Constructor de la clase
     */
    public Valores(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initValores();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initValores(){

        // inicializamos las variables de clase
        this.IdValor = 0;
        this.IdTecnica = 0;
        this.Tecnica = "";
        this.Valor = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // mètodos de asignaciòn de valores
    public void setIdValor(int idvalor){
        this.IdValor = idvalor;
    }
    public void setIdTecnica(int idtecnica){
        this.IdTecnica = idtecnica;
    }
    public void setValor(String valor){
        this.Valor = valor;
    }

    // métodos de retorno de valores
    public int getIdValor(){
        return this.IdValor;
    }
    public int getIdTecnica(){
        return this.IdTecnica;
    }
    public String getTecnica(){
        return this.Tecnica;
    }
    public String getValor(){
        return this.Valor;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param tecnica clave de la técnica
     * @return vector con los valores válidos
     * Método que recibe la clave de una técnica y retorna el array
     * de valores aceptados
     */
    public ResultSet nominaValores(int tecnica){

        // definición de variables
        String Consulta;
        ResultSet listaValores;

        // armamos la consulta
        Consulta = "SELECT diagnostico.v_valores.valor AS valor_tecnica, "
                 + "       diagnostico.v_valores.id_valor AS id_valor "
                 + "FROM diagnostico.v_valores "
                 + "WHERE diagnostico.v_valores.id_tecnica = '" + tecnica + "';";

        // ejecutamos la consulta y retornamos el array
        listaValores = this.Enlace.Consultar(Consulta);
        return listaValores;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro afectado
     * Método que graba los datos del valor aceptado de la
     * técnica en la base de datos
     */
    public int grabaValores(){

        // si no recibió la clave
        if (this.IdValor == 0){
            this.nuevoValor();
        } else {
            this.editaValor();
        }

        // retornamos la id
        return this.IdValor;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo protegido llamado al insertar un nuevo registro
     */
    protected void nuevoValor(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de inserción
        Consulta = "INSERT INTO diagnostico.valores_tecnicas "
                 + "       (id_tecnica, "
                 + "        valor, "
                 + "        id_usuario) "
                 + "        VALUES "
                 + "        (?, ?, ?);";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.IdTecnica);
	        preparedStmt.setString (2, this.Valor);
	        preparedStmt.setInt    (3, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	        // obtenemos la id del registro
	        this.IdValor = this.Enlace.UltimoInsertado();


	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo protegido llamado al editar el registro
     */
    protected void editaValor(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "UPDATE diagnostico.valores_tecnicas SET "
                 + "       id_tecnica = ?, "
                 + "       valor = ?, "
                 + "       id_usuario = ? "
                 + "WHERE diagnostico.valores_tecnicas.id = ?;";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.IdTecnica);
	        preparedStmt.setString (2, this.Valor);
	        preparedStmt.setInt    (3, Seguridad.Id);
	        preparedStmt.setInt    (4, this.IdValor);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtecnica - clave de la tècnica
     * @param valor - valor a verificar
     * @return Correcto - boolean indicando si està repetido
     * Mètodo pùblico que recibe como paràmetro la clave de la tècnica
     * y el valor a verificar, retorna falso si ya se encuentra
     * declarado
     */
    public boolean validaValor(int idtecnica, String valor){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.v_valores.id_valor) AS registros "
                 + "FROM diagnostico.v_valores "
                 + "WHERE diagnostico.v_valores.id_tecnica = '" + idtecnica + "' AND "
                 + "      diagnostico.v_valores.valor = '" + valor + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y asigna los datos en las variables de clase
     */
    public void getDatosValor(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_valores.id_valor AS idvalor, " +
                   "       diagnostico.v_valores.id_tecnica AS idtecnica, " +
                   "       diagnostico.v_valores.valor AS valor, " +
                   "       diagnostico.v_valores.tecnica AS tecnica, " +
                   "       diagnostico.v_valores.usuario AS usuario, " +
                   "       diagnostico.v_valores.fecha_alta AS fecha_alta " +
                   "FROM diagnostico.v_valores " +
                   "WHERE diagnostico.v_valores.id_valor = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos los valores del registro
            this.IdValor = Resultado.getInt("idvalor");
            this.IdTecnica = Resultado.getInt("idtecnica");
            this.Valor = Resultado.getString("valor");
            this.Tecnica = Resultado.getString("tecnica");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que ejecuta la consulta de eliminación
     */
    public void borraValor(int clave){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.valores_tecnicas " +
                   "WHERE diagnostico.valores_tecnicas.id = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

}
