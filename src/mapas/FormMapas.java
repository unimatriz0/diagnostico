/*

    Nombre: formMapas
    Fecha: 09/02/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase implementa el formulario y muestra los mapas de las 
                 coordenadas recibidas

 */

// declaración del paquete
package mapas;

// importamos las librerías
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import funciones.Utilidades;
import maps.java.StaticMaps;
import maps.java.StaticMaps.Maptype;

/**
 *
 * @author Lic. Claudio Invernizzi
 */
public class FormMapas extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
	
    // declaración de variables
    protected String Coordenadas;            // coordenadas a representar
    protected Maptype TipoMapa;              // modo de representación
    protected int Zoom;                      // acercamiento de la imagen
    protected Image Mapa;                    // imagen del mapa

    // Declaracion de variables del formulario
    private JButton btnGrabar;
    private JComboBox <String>cTipoMapa;
    private JLabel lMapa;
    private JSpinner sZoom;
    
    /**
     * @author Lic. Claudio Invernizzi
     * @param parent el formulario padre cuando es un frame
     * @param modal true o false
     * constructor de la clase cuando se llama desde un frame
     */
    public FormMapas(java.awt.Frame parent, boolean modal) {
    	
    	// establecemos el padre
        super(parent, modal);

        // iniciamos los componentes
        initComponents();
        
    }

    /**
     * @author Lic. Claudio Invernizzi
     * @param diagnostico el formulario padre cuando es panel
     * constructor de la clase cuando se llama desde un frame
     */
    public FormMapas(JPanel diagnostico) {

        // iniciamos los componentes
        initComponents();
        
    }
    
    /**
     * @author Lic. Claudio Invernizzi
     * @param parent el formulario padre cuando es un dialog
     * @param modal true o false
     * constructor de la clase cuando se llama desde un dialog
     */
    public FormMapas(java.awt.Dialog parent, boolean modal) {
        super(parent, modal);

        // iniciamos los componentes
        initComponents();
        
    }    

    /**
     * @author Lic. Claudio Invernizzi
     * Metodo que instancia los componentes del formulario
     */
    private void initComponents() {

        // definimos las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Ubicacion Geografica");
        this.setLayout(null);
        this.setResizable(false);
        this.setBounds(250,200,750,500);                

        // el label de tipo de mapa
        JLabel lTipo = new JLabel("Tipo de Mapa:");
        lTipo.setBounds(10,6,100,26);
        this.add(lTipo);
        
        // el combo con el tipo de mapa
        this.cTipoMapa = new javax.swing.JComboBox<>();
        this.cTipoMapa.setBounds(120,6,140,26);
        this.add(this.cTipoMapa);
        this.cTipoMapa.addItem("Carreteras");
        this.cTipoMapa.addItem("Satélite");
        this.cTipoMapa.addItem("Relieve");
        this.cTipoMapa.addItem("Híbrido");
        this.cTipoMapa.setToolTipText("Seleccione el tipo de mapa a visualizar");
        this.cTipoMapa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cTipoMapaItemStateChanged(evt);
            }
        });

        // el label de zoom
        JLabel lZoom = new JLabel("Zoom:");
        lZoom.setBounds(270,6,75,26);
        this.add(lZoom);
        
        // el spinner con el zoom
        this.sZoom = new javax.swing.JSpinner();
        this.sZoom.setBounds(320,6,40,26);
        this.add(this.sZoom);
        this.sZoom.setModel(new javax.swing.SpinnerNumberModel(12, 1, 21, 1));
        this.sZoom.setToolTipText("Indique el acercamiento de la imagen");
        this.sZoom.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sZoomStateChanged(evt);
            }
        });

        // el boton grabar
        this.btnGrabar = new javax.swing.JButton("Grabar");
        this.btnGrabar.setBounds(390,6,120,26);
        this.btnGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));        
        this.add(this.btnGrabar);
        this.btnGrabar.setToolTipText("Graba el mapa a un archivo");
        this.btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        // el label con el mapa
        this.lMapa = new JLabel();
        this.lMapa.setBounds(5,40,1000,415);
        this.add(this.lMapa);
                
        // inicializamos las variables
        this.Coordenadas = "";
        this.TipoMapa = StaticMaps.Maptype.terrain;
        this.Zoom = 9;
                
    }

    // evento llamado al cambiar el spin de zoom
    private void sZoomStateChanged(javax.swing.event.ChangeEvent evt) {

        // actualizamos la variable de clase y redibujamos
        this.Zoom = Integer.parseInt(this.sZoom.getValue().toString());
        this.mostrarMapa();
        
    }

    // evento llamado al cambiar el combo de tipo de mapa
    private void cTipoMapaItemStateChanged(java.awt.event.ItemEvent evt) {

        // según el estado del combo actualizamos la variable de clase
        // y redibujamos
        switch (this.cTipoMapa.getSelectedItem().toString()){
            
            // si es carreteras
            case "Carreteras":
                this.TipoMapa = StaticMaps.Maptype.roadmap;
                break;
                
            // si es relieve
            case "Relieve":
                this.TipoMapa = StaticMaps.Maptype.terrain;
                break;
                
            // si es satélite
            case "Satélite":
                this.TipoMapa = StaticMaps.Maptype.satellite;
                break;
                
            // si es híbrido
            case "Híbrido":
                this.TipoMapa = StaticMaps.Maptype.hybrid;
                break;
                
        }
        
        // redibujamos
        this.mostrarMapa();
        
    }

    // evento disparado al pulsar el botón grabar
    private void btnGrabarActionPerformed(java.awt.event.ActionEvent evt) {

        // obtenemos el archivo a grabar
        Utilidades Herramientas = new Utilidades();
        String Archivo = Herramientas.grabaArchivo(this);
        
        // si seleccionó archivo
        if (Archivo != null){
        
            try {
              ImageIO.write((RenderedImage) this.Mapa, "png", new File(Archivo));
            } catch (IOException e) {
                System.out.println("Error de escritura");
            }
            
        }
        
    }

    /**
     * @author Lic. Claudio Invernizzi
     * @param coordenadas string con las coordenadas iniciales a mostrar
     * Método que asigna a la variable de clase las coordenadas
     */
    public void setCoordenadas(String coordenadas){
    
        // fijamos el valor en la variable de clase
        this.Coordenadas = coordenadas;
        
    }
    
    // método público que muestra el mapa en el diálogo
    public void mostrarMapa(){

        // definimos el mapa
        StaticMaps ObjStatMap=new StaticMaps();
        
        try {
            
            // obtenemos la imagen
            this.Mapa = ObjStatMap.getStaticMap(this.Coordenadas, 
                                   this.Zoom,
                                   new Dimension(840,500),
                                   1, StaticMaps.Format.png, 
                                   this.TipoMapa);
            
            // convertimos la imagen obtenida y redibujamos el label
            ImageIcon icono = new ImageIcon(this.Mapa);
            this.lMapa.setIcon(icono);
            this.lMapa.repaint();
            
        } catch (Exception e) {
            System.out.println("mapas estáticos");
        }
        
    }
    
}
