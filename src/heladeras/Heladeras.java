/*

    Nombre: Heladeras
    Fecha: 20/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Métodos que controlan las operaciones sobre la tabla 
                 de heladeras / freezers

 */

// declaración del paquete
package heladeras;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Declaración de la clase
 */
public class Heladeras {

    // declaración de variables
    protected int IdHeladera;                      // clave del registro
    protected int Laboratorio;                     // clave del laboratorio
    protected String Marca;                        // marca de la heladera
    protected String Ubicacion;                    // descripción de la ubicación
    protected String Patrimonio;                   // nro. de registro de patrimonio
    protected int Temperatura1;                    // primer valor de temperatura
    protected int Temperatura2;                    // para heladeras de dos temperaturas
    protected int Tolerancia;                      // rango de tolerancia en grados
    protected int IdUsuario;                       // clave del usuario
    protected String Usuario;                      // nombre del usuario
    protected String FechaAlta;                    // fecha de alta del registro

    // el puntero a la base
    protected Conexion Enlace;
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Heladeras(){
    
        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las varriables
        this.initHeladeras();
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initHeladeras(){
        
        // inicializamos los valores
        this.IdHeladera = 0;
        this.Laboratorio = Seguridad.Laboratorio;
        this.Marca = "";
        this.Ubicacion = "";
        this.Patrimonio = "";
        this.Temperatura1 = 0;
        this.Temperatura2 = 0;
        this.Tolerancia = 0;
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.FechaAlta = "";
        
    }
    
    // métodos de asignación de valores
    public void setIdHeladera(int idheladera){
        this.IdHeladera = idheladera;
    }
    public void setMarca(String marca){
        this.Marca = marca;
    }
    public void setUbicacion(String ubicacion){
        this.Ubicacion = ubicacion;
    }
    public void setPatrimonio(String patrimonio){
        this.Patrimonio = patrimonio;
    }
    public void setTemperatura1(int temperatura){
        this.Temperatura1 = temperatura;
    }
    public void setTemperatura2(int temperatura){
        this.Temperatura2 = temperatura;
    }
    public void setTolerancia(int tolerancia){
        this.Tolerancia = tolerancia;
    }
    
    // métodos de retorno de valores
    public int getIdHeladera(){
        return this.IdHeladera;
    }
    public int getLaboratorio(){
    	return this.Laboratorio;
    }
    public String getMarca(){
        return this.Marca;
    }
    public String getUbicacion(){
        return this.Ubicacion;
    }
    public String getPatrimonio(){
        return this.Patrimonio;
    }
    public int getTemperatura1(){
        return this.Temperatura1;
    }
    public int getTemperatura2(){
        return this.Temperatura2;
    }
    public int getTolerancia(){
        return this.Tolerancia;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera - clave de la heladera
     * Método que recibe como parámetro la clave de una heladera
     * y asigna en las variables de clase los valores del registro
     */
    public void getDatosHeladera(int idheladera){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_heladeras.id_heladera AS id, "
                 + "       diagnostico.v_heladeras.marca AS marca, "
                 + "       diagnostico.v_heladeras.ubicacion AS ubicacion, "
                 + "       diagnostico.v_heladeras.patrimonio AS patrimonio, "
                 + "       diagnostico.v_heladeras.temperatura AS temperatura1, "
                 + "       diagnostico.v_heladeras.temperatura2 AS temperatura2, "
                 + "       diagnostico.v_heladeras.tolerancia AS tolerancia, "
                 + "       diagnostico.v_heladeras.usuario AS usuario, "
                 + "       diagnostico.v_heladeras.fecha_alta AS fecha_alta "
                 + "FROM diagnostico.v_heladeras "
                 + "WHERE diagnostico.v_heladeras.id_heladera = '" + idheladera + "' "
                 + "LIMIT 1; "; 

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // asignamos los valores
            this.IdHeladera = Resultado.getInt("id");
            this.Marca = Resultado.getString("marca");
            this.Ubicacion = Resultado.getString("ubicacion");
            this.Patrimonio = Resultado.getString("patrimonio");
            this.Temperatura1 = Resultado.getInt("temperatura1");
            this.Temperatura2 = Resultado.getInt("temperatura2");
            this.Tolerancia = Resultado.getInt("tolerancia");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return nomina - vector con el listado de heladeras
     * Método que retorna el vector con la nómina de heladeras
     * para el laboratorio activo
     */
    public ResultSet nominaHeladeras(){

        // declaración de variables
        String Consulta;
        ResultSet nomina;
        
        // componemos la consulta
        Consulta = "SELECT diagnostico.heladeras.id AS id, "
                 + "       diagnostico.heladeras.marca AS marca,"
                 + "       diagnostico.heladeras.ubicacion AS ubicacion,"
                 + "       diagnostico.heladeras.temperatura AS temperatura1,"
                 + "       diagnostico.heladeras.temperatura2 AS temperatura2,"
                 + "       diagnostico.heladeras.tolerancia AS tolerancia "
                 + "FROM diagnostico.heladeras "
                 + "WHERE diagnostico.heladeras.laboratorio = '" + this.Laboratorio + "' "
                 + "ORDER BY diagnostico.heladeras.marca;";
        
        // obtenemos el vector
        nomina = this.Enlace.Consultar(Consulta);
        
        // retornamos el vector
        return nomina;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return idheladera - clave del registro
     * Método que ejecuta la consulta de edición o inserción 
     * según corresponda
     */
    public int grabaHeladera(){
        
        // si está insertando
        if (this.IdHeladera == 0){
            this.nuevaHeladera();
        } else {
            this.editaHeladera();
        }
        
        // retorna la clave
        return this.IdHeladera;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de insercion
     */
    protected void nuevaHeladera(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "INSERT INTO diagnostico.heladeras "
    			+ "        (laboratorio, "
    			+ "         marca, "
    			+ "         ubicacion, "
    			+ "         patrimonio, "
    			+ "         temperatura, "
    			+ "         temperatura2, "
    			+ "         tolerancia, "
    			+ "         usuario) "
    			+ "        VALUES "
    			+ "        (?,?,?,?,?,?,?,?);";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.Laboratorio);
	        preparedStmt.setString (2, this.Marca);
	        preparedStmt.setString (3, this.Ubicacion);
	        preparedStmt.setString (4, this.Patrimonio);
	        preparedStmt.setInt    (5, this.Temperatura1);
	        preparedStmt.setInt    (6, this.Temperatura2);
	        preparedStmt.setInt    (7, this.Tolerancia);
	        preparedStmt.setInt    (8, this.IdUsuario);
	        
	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	        // obtenemos la id
	        this.IdHeladera = this.Enlace.UltimoInsertado();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaHeladera(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "UPDATE diagnostico.heladeras SET "
    			+ "        laboratorio = ?, "
    			+ "        marca = ?, "
    			+ "        ubicacion = ?, "
    			+ "        patrimonio = ?, "
    			+ "        temperatura = ?, "
    			+ "        temperatura2 = ?, "
    			+ "        tolerancia = ?, "
    			+ "        usuario = ? " 
    			+ " WHERE diagnostico.heladeras.id = ?; ";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.Laboratorio);
	        preparedStmt.setString (2, this.Marca);
	        preparedStmt.setString (3, this.Ubicacion);
	        preparedStmt.setString (4, this.Patrimonio);
	        preparedStmt.setInt    (5, this.Temperatura1);
	        preparedStmt.setInt    (6, this.Temperatura2);
	        preparedStmt.setInt    (7, this.Tolerancia);
	        preparedStmt.setInt    (8, this.IdUsuario);
	        preparedStmt.setInt    (9, this.IdHeladera);
	        
	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param patrimonio - número de registro de patrimonio
     * Método que recibe como parámetro el número de patrimonio
     * y verifica que la heladera no se encuentre repetida 
     * para el laboratorio actual, retorna verdadero si es 
     * posible insertar el registro
     */
    public boolean validaHeladera(String patrimonio){
    
        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.heladeras.id) AS registros "
                 + "FROM diagnostico.heladeras "
                 + "WHERE diagnostico.heladeras.patrimonio = '" + patrimonio + "' AND "
                 + "      diagnostico.heladeras.laboratorio = '" + this.Laboratorio + "'; "; 

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos el estado
        return Correcto;
        
    }

    /**
     * @autor Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera
     * Método que ejecuta la consulta de eliminación 
     */
    public void borraHeladera(int idheladera){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "DELETE FROM diagnostico.heladeras "
    			+ " WHERE diagnostico.heladeras.id = ?; ";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setInt    (1, idheladera);
	        
	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera
     * @return Correcto boolean indicando si tiene hijos
     * Método llamado antes de eliminar que verifica no tenga 
     * hijos
     */
    public boolean hijosHeladera(int idheladera){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.temperaturas.id) AS registros "
                 + "FROM diagnostico.temperaturas "
                 + "WHERE diagnostico.temperaturas.heladera = '" + idheladera + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos el estado
        return Correcto;
    	
    }
    
}
