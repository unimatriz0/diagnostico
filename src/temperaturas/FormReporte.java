/*

    Nombre: FormReporte
    Fecha: 11/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que presenta el formulario solicitando 
                 se ingrese el período a reportar

 */

// definición del paquete
package temperaturas;

// inclusión de librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.*;
import funciones.Utilidades;

// definición de la clase
public class FormReporte extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = 1L;

	// definimos las variables de clase
	private JButton btnReporte;
	private JDateChooser dFechaInicio;
	private JDateChooser dFechaFinal;
	
	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param parent - el formulario padre
	 * @param modal - si será modal
	 * Constructor de la clase
	 */
	public FormReporte(java.awt.Dialog parent, boolean modal) {
		
        // setea el padre e inicia los componentes
        super(parent, modal);
		
		// fijamos las propiedades del formulario
		setBounds(100, 100, 353, 189);
		getContentPane().setLayout(null);
		
		// inicializamos el formulario
		this.initForm();
		
	}
	
	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
	private void initForm(){
	
		// presenta el título
		JLabel lTitulo = new JLabel("Indique el período a reportar");
		lTitulo.setBounds(12, 20, 344, 26);
		getContentPane().add(lTitulo);
		
		// pide la fecha de inicio
		JLabel lInicio = new JLabel("Inicio:");
		lInicio.setBounds(12, 55, 51, 26);
		getContentPane().add(lInicio);
        this.dFechaInicio = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaInicio.setToolTipText("Fecha de inicio del reporte");
		this.dFechaInicio.setBounds(61,55,105,26);
		this.dFechaInicio.setToolTipText("Indique la fecha de lectura");
        getContentPane().add(this.dFechaInicio);
		
		// pide la fecha final a reportar
		JLabel lFin = new JLabel("Fin: ");
		lFin.setBounds(188, 55, 43, 26);
		getContentPane().add(lFin);
        this.dFechaFinal = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaFinal.setToolTipText("Fecha de toma de lectura");
		this.dFechaFinal.setBounds(223,55,105,26);
		this.dFechaFinal.setToolTipText("Indique la fecha de lectura");
        getContentPane().add(this.dFechaFinal);
		
		// presenta el botón generar
		this.btnReporte = new JButton("Reporte");
		this.btnReporte.setBounds(137, 113, 117, 26);
		this.btnReporte.setToolTipText("Pulse para generar el reporte");
		this.btnReporte.setIcon(new ImageIcon(FormReporte.class.getResource("/Graficos/mestadistica.png")));		
		getContentPane().add(this.btnReporte);
        this.btnReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReporteActionPerformed(evt);
            }
        });
		
	}
	
	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón exportar
	 */
	protected void btnReporteActionPerformed(java.awt.event.ActionEvent evt){

		// instanciamos la clase herramientas
		Utilidades Herramientas = new Utilidades();
		String fechainicio = Herramientas.fechaJDate(this.dFechaInicio); 
		String fechafinal = Herramientas.fechaJDate(this.dFechaFinal);
		
		// verificamos que halla indicado una fecha inicial
		if ( fechainicio == null){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe indicar la fecha inicial", "Error", JOptionPane.ERROR_MESSAGE);
            this.dFechaInicio.requestFocus();
            return;
			
		}
		
		// verificamos que halla indicado una fecha final
		if (fechafinal == null){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe indicar la fecha Final", "Error", JOptionPane.ERROR_MESSAGE);
            this.dFechaFinal.requestFocus();
            return;
			
		}
		
		// generamos el reporte pasándole las fechas
		new XLSTemperaturas(fechainicio, fechafinal);

	}
	
}
