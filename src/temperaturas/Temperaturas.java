/*

    Nombre: Temperaturas
    Fecha: 28/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Métodos que controlan las tablas de temperaturas

 */

// definición del paquete
package temperaturas;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
public class Temperaturas {

	// declaración de variables
	protected int IdTemperatura;           // clave del registro
	protected int Heladera;                // clave de la heladera
	protected String Fecha;                // fecha de lectura
	protected int Temperatura1;            // Primera lectura
	protected int Temperatura2;            // Segunda lectura
	protected int Controlado;              // clave del usuario
	protected String Usuario;              // nombre del usuario

    // el puntero a la base
    protected Conexion Enlace;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Temperaturas(){

    	// instanciamsos la conexión
    	this.Enlace = new Conexion();

    	// inicializamos las variables
    	this.IdTemperatura = 0;
    	this.Heladera = 0;
    	this.Fecha = "";
    	this.Temperatura1 = 0;
    	this.Temperatura2 = 0;
    	this.Controlado = Seguridad.Id;
    	this.Usuario = "";

    }

    // métodos públicos de asignación de valores
    public void setIdTemperatura(int idtemperatura){
    	this.IdTemperatura = idtemperatura;
    }
    public void setHeladera(int heladera){
    	this.Heladera = heladera;
    }
    public void setFecha(String fecha){
    	this.Fecha = fecha;
    }
    public void setTemperatura1(int temperatura){
    	this.Temperatura1 = temperatura;
    }
    public void setTemperatura2(int temperatura){
    	this.Temperatura2 = temperatura;
    }

    // métodos públicos de retorno de valores
    public int getIdTemperatura(){
    	return this.IdTemperatura;
    }
    public int getHeladera(){
    	return this.Heladera;
    }
    public String getFecha(){
    	return this.Fecha;
    }
    public int getTemperatura1(){
    	return this.Temperatura1;
    }
    public int getTemperatura2(){
    	return this.Temperatura2;
    }
    public String getUsuario(){
    	return this.Usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera - clave de la heladera
     * @return Resultado - resultset con la nómina
     * Método que recibe como parámetro la clave de euna
     * heladera y retorna la nómina lecturas de la misma
     */
    public ResultSet nominaTemperaturas(int idheladera){

    	// declaración de variables
    	String Consulta;
    	ResultSet Resultado;

    	// componemos la consulta sobre la vista
    	Consulta = "SELECT diagnostico.v_heladeras.id_registro AS idregistro, "
    			+ "        diagnostico.v_heladeras.temperatura AS temperatura1, "
    			+ "        diagnostico.v_heladeras.temperatura2 AS temperatura2, "
    			+ "        diagnostico.v_heladeras.tolerancia AS tolerancia, "
    			+ "        diagnostico.v_heladeras.fecha_lectura AS fecha,"
    			+ "        diagnostico.v_heladeras.valor_lectura AS lectura1,"
    			+ "        diagnostico.v_heladeras.valor_lectura2 AS lectura2, "
    			+ "        diagnostico.v_heladeras.controlado AS controlado "
    			+ " FROM diagnostico.v_heladeras "
    			+ " WHERE diagnostico.v_heladeras.id_heladera = '" + idheladera + "'; ";

    	// ejecutamos la consulta
    	Resultado = this.Enlace.Consultar(Consulta);
    	return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio - string con la fecha inicial
     * @param fechafinal - string con la fecha final
     * @return nomina - vector con los registros
     * Método que recibe como parámetro dos fechas y retorna un vector
     * con la nómina de temperaturas de todas las heladeras del laboratorio
     * en el período indicado
     */
    public ResultSet nominaTemperaturas(String fechainicio, String fechafinal){
    	
    	// declaración de variables
    	String Consulta;
    	ResultSet Nomina = null; 

    	// componemos la consulta
    	// componemos la consulta sobre la vista
		Consulta = "SELECT diagnostico.v_heladeras.id_heladera AS id_heladera, "
		        + "        diagnostico.v_heladeras.marca AS marca, " 
    			+ "        diagnostico.v_heladeras.ubicacion AS ubicacion, "
    			+ "        diagnostico.v_heladeras.patrimonio AS patrimonio, "
    			+ "        diagnostico.v_heladeras.temperatura AS temperatura1, "
    			+ "        diagnostico.v_heladeras.temperatura2 AS temperatura2, "
    			+ "        diagnostico.v_heladeras.tolerancia AS tolerancia, "
    			+ "        diagnostico.v_heladeras.fecha_lectura AS fecha, "
    			+ "        diagnostico.v_heladeras.hora_lectura AS hora_lectura, "
    			+ "        diagnostico.v_heladeras.valor_lectura AS lectura1,"
    			+ "        diagnostico.v_heladeras.valor_lectura2 AS lectura2, "
    			+ "        diagnostico.v_heladeras.controlado AS controlado "
    			+ " FROM diagnostico.v_heladeras "
    			+ " WHERE STR_TO_DATE(diagnostico.v_heladeras.fecha_lectura, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND "
    			+ "       STR_TO_DATE(diagnostico.v_heladeras.fecha_lectura, '%d/%m/%Y') <= STR_TO_DATE('" + fechafinal + "', '%d/%m/%Y') AND "
    			+ "       diagnostico.v_heladeras.idlaboratorio = '" + Seguridad.Laboratorio +  "' "
    			+ " ORDER BY diagnostico.v_heladeras.marca, "
    			+ "          diagnostico.v_heladeras.ubicacion; ";

    	// ejecutamos la consulta
    	Nomina = this.Enlace.Consultar(Consulta);
    	
    	// retornamos el vector
    	return Nomina;
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idlectura
     * Método que recibe como parámetro la clave de un registro y
     * asigna en las variables de clase los valores del mismo
     */
    public void getDatosLectura(int idlectura){

    	// declaración de variables
    	String Consulta;
    	ResultSet Resultado;

    	// componemos la consulta
    	Consulta = "SELECT diagnostico.v_heladeras.id_registro AS idregistro, "
    			+ "        diagnostico.v_heladeras.fecha_lectura AS fecha,"
    			+ "        diagnostico.v_heladeras.valor_lectura AS lectura1,"
    			+ "        diagnostico.v_heladeras.valor_lectura2 AS lectura2, "
    			+ "        diagnostico.v_heladeras.controlado AS controlado "
    			+ " FROM diagnostico.v_heladeras "
    			+ " WHERE diagnostico.v_heladeras.id_registro = '" + idlectura + "'; ";

    	// ejecutamos la consulta
    	Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

        	this.Usuario = "";
            // asignamos los valores
            this.IdTemperatura = Resultado.getInt("idregistro");
            this.Fecha = Resultado.getString("fecha");
            this.Temperatura1 = Resultado.getInt("lectura1");
            this.Temperatura2 = Resultado.getInt("lectura2");
            this.Usuario = Resultado.getString("controlado");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return idregistro - clave del registro afectado
     * Método público que ejecuta la consulta de edición
     * o inserción según corresponda
     */
    public int grabaLectura(){

    	// si está insertando
    	if (this.IdTemperatura == 0){
    		this.nuevaLectura();
    	} else {
    		this.editaLectura();
    	}

    	// retornamos la clave
    	return this.IdTemperatura;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de inserción
     */
    protected void nuevaLectura(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "INSERT INTO diagnostico.temperaturas "
    			+ "        (heladera, "
    			+ "         fecha, "
    			+ "         temperatura,"
    			+ "         temperatura2,"
    			+ "         controlado)"
    			+ "        VALUES (?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?);";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.Heladera);
	        preparedStmt.setString (2, this.Fecha);
	        preparedStmt.setInt    (3, this.Temperatura1);
	        preparedStmt.setInt    (4, this.Temperatura2);
	        preparedStmt.setInt    (5, this.Controlado);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	        // obtenemos la id
	        this.IdTemperatura = this.Enlace.UltimoInsertado();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de edición
     */
    protected void editaLectura(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "UPDATE diagnostico.temperaturas SET "
    			+ "        heladera = ?, "
    			+ "        fecha = STR_TO_DATE(?, '%d/%m/%Y'), "
    			+ "        temperatura = ?, "
    			+ "        temperatura2 = ?, "
    			+ "        controlado = ? "
    			+ " WHERE diagnostico.temperaturas.id = ?; ";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.Heladera);
	        preparedStmt.setString (2, this.Fecha);
	        preparedStmt.setInt    (3, this.Temperatura1);
	        preparedStmt.setInt    (4, this.Temperatura2);
	        preparedStmt.setInt    (5, this.Controlado);
	        preparedStmt.setInt    (6, this.IdTemperatura);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	        // obtenemos la id
	        this.IdTemperatura = this.Enlace.UltimoInsertado();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idlectura - clave de la lectura
	 * Método que recibe como parámetro la clave de una lectura
	 * y ejecuta la consulta de eliminación
	 */
	public void borraLectura(int idlectura){

		// declaración de variables
		String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

		// componemos la consulta
		Consulta = "DELETE FROM diagnostico.temperaturas WHERE id = ?";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt (1, idlectura);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
