/*
 *
 * Nombre: formLaboratorios
 * Fecha: 04/01/2019
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Proyecto: Diagnostico
 * Licencia: GPL
 * Producido en: INP - Dr. Mario Fatala Chaben
 * Buenos Aires - Argentina
 * Comentarios: Formulario de presentación de datos de los laboratorios utiliza
 *              la tabla de laboratorios participantes del CCE
 *
 */

// definición del paquete
package laboratorios;

// inclusión de librerías
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import paises.Paises;
import localidades.Localidades;
import provincias.Provincias;
import dependencias.Dependencias;
import funciones.Utilidades;
import mapas.FormMapas;
import usuarios.Usuarios;

// definimos la clase
public class FormLaboratorios extends JPanel {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
	
	// los diccionarios
	protected Paises listaPaises;
	protected Provincias listaProvincias;
	protected Localidades listaLocalidad;
	protected Utilidades Herramientas;
	protected Usuarios responsableLab;
	protected Dependencias listaDependencia;
	    
    // definimos las variables de la clase
	protected JTextField tIdLab;
	protected JTextField tNombreLab;
	protected JTextField tResponsableLab;
	protected JTextField tDireccionLab;
	protected JTextField tCodigoPostalLab;
	protected JTextField tMailLab;
	protected JTextField tCoordenadasLaboratorio;
	protected JTextField tUsuarioLab;
	protected JTextField tFechaAltaLab;
	protected JCheckBox cActivoLab;
	protected JComboBox<String> cPaisLab;
	protected JComboBox<String> cJurisdiccionLab;
	protected JComboBox<String> cLocalidadLab;
	protected JCheckBox cRecibeMuestras;
	protected JComboBox<String> cResponsableRecibe;
	protected JButton btnMapaLaboratorio;
	protected JComboBox<String> cDependencia;
	protected JTextArea tComentariosLab;
	protected JPanel Diagnostico;
	
    // constructor de la clase, recibe como parámetro el 
	// contenedor del panel
    public FormLaboratorios (JTabbedPane Tabulador) {

		// inicializamos el contenedor
		this.Diagnostico = new JPanel();
		this.Diagnostico.setBounds(0, 0, 945, 620);
		this.Diagnostico.setLayout(null);
		this.Diagnostico.setToolTipText("Datos de los Laboratorios");
        
    	// la clase de herramientas
        this.Herramientas = new Utilidades();

        // la clase de usuarios
        this.responsableLab = new Usuarios();
    	
        // el diccionario de paises
        this.listaPaises = new Paises();
        
        // el diccionario de provincias
        this.listaProvincias = new Provincias();
        
        // el diccionario de localidades
        this.listaLocalidad = new Localidades();
        
        // las dependencias
        this.listaDependencia = new Dependencias();
        
        // el label de id
        JLabel lIdLab = new JLabel("ID:");
        lIdLab.setBounds(10, 10, 50, 26);
        this.Diagnostico.add(lIdLab);

        // el texto de la id
        this.tIdLab = new JTextField();
        this.tIdLab.setEditable(false);
        this.tIdLab.setToolTipText("Clave del registro");
        this.tIdLab.setBounds(30, 10, 50, 26);
        this.Diagnostico.add(this.tIdLab);

        // el label del nombre del laboratorio
        JLabel lNombreLab = new JLabel("Laboratorio:");
        lNombreLab.setBounds(85, 10, 100, 26);
        this.Diagnostico.add(lNombreLab);

        // el texto del nombre del laboratorio
        this.tNombreLab = new JTextField();
        this.tNombreLab.setToolTipText("Nombre completo del laboratorio");
        this.tNombreLab.setBounds(180, 10, 435, 26);
        this.Diagnostico.add(this.tNombreLab);

        // agregamos el listener
        this.tNombreLab.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                // si pulsó enter
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    tResponsableLab.requestFocus();
                }
            }
        });

        // el label del responsable del laboratorio
        JLabel lResponsableLab = new JLabel("Responsable:");
        lResponsableLab.setBounds(10, 45, 120, 26);
        this.Diagnostico.add(lResponsableLab);

        // el texto del responsable del laboratorio
        this.tResponsableLab = new JTextField();
        this.tResponsableLab.setToolTipText("Nombre del responsable");
        this.tResponsableLab.setBounds(105, 45, 350, 26);
        this.Diagnostico.add(this.tResponsableLab);

        // agregamos el listener
        this.tResponsableLab.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                // si pulsó enter
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    cDependencia.requestFocus();
                }

            }
        });

        // el label de la dependencia
        JLabel lDependencia = new JLabel("Dependencia:");
        lDependencia.setBounds(460, 45, 120, 26);
        this.Diagnostico.add(lDependencia);

        // el combo de la dependencia
        this.cDependencia = new JComboBox<>();
        this.cDependencia.setToolTipText("Dependencia administrativa del laboratorio");
        this.cDependencia.setBounds(555, 45, 165, 26);
        this.Diagnostico.add(this.cDependencia);

        // el label de la dirección
        JLabel lDireccionLab = new JLabel("Dirección:");
        lDireccionLab.setBounds(10, 80, 120, 26);
        this.Diagnostico.add(lDireccionLab);

        // el texto de la dirección
        this.tDireccionLab = new JTextField();
        this.tDireccionLab.setToolTipText("Dirección postal completa");
        this.tDireccionLab.setBounds(80, 80, 395, 26);
        this.Diagnostico.add(this.tDireccionLab);

        // agregamos el listener
        this.tDireccionLab.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                // si pulsó enter
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    tCodigoPostalLab.requestFocus();
                }

            }
        });

        // el label del código postal
        JLabel lCodigoPostalLab = new JLabel("Código Postal:");
        lCodigoPostalLab.setBounds(490, 80, 120, 26);
        this.Diagnostico.add(lCodigoPostalLab);

        // el texto del código postal
        this.tCodigoPostalLab = new JTextField();
        this.tCodigoPostalLab.setToolTipText("Código Postal de la Dirección");
        this.tCodigoPostalLab.setBounds(595, 80, 105, 26);
        this.Diagnostico.add(this.tCodigoPostalLab);

        // agregamos el listener
        this.tCodigoPostalLab.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                // si pulsó enter
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    cPaisLab.requestFocus();
                }

            }
        });

        // el label del país del laboratorio
        JLabel lPaisLab = new JLabel("País:");
        lPaisLab.setBounds(10, 115, 100, 26);
        this.Diagnostico.add(lPaisLab);

        // el combo del país
        this.cPaisLab = new JComboBox<>();
        this.cPaisLab.setToolTipText("País donde funciona el laboratorio");
        this.cPaisLab.setBounds(50, 115, 150, 26);
        this.Diagnostico.add(this.cPaisLab);

        // el label de la jurisdicción
        JLabel lJurisdiccionLab = new JLabel("Jurisdicción");
        lJurisdiccionLab.setBounds(210, 115, 120, 26);
        this.Diagnostico.add(lJurisdiccionLab);

        // el combo de la jurisdicción
        this.cJurisdiccionLab = new JComboBox<>();
        this.cJurisdiccionLab.setToolTipText("Jurisdicción del Laboratorio");
        this.cJurisdiccionLab.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cJurisdiccionLabItemStateChanged(evt);
            }
        });
        this.cJurisdiccionLab.setBounds(295, 115, 227, 26);
        this.Diagnostico.add(this.cJurisdiccionLab);

        // el label de la localidad
        JLabel lLocalidadLab = new JLabel("Localidad:");
        lLocalidadLab.setBounds(527, 115, 120, 26);
        this.Diagnostico.add(lLocalidadLab);

        // el combo de la localidad
        this.cLocalidadLab = new JComboBox<>();
        this.cLocalidadLab.setToolTipText("Localidad del Laboratorio");
        this.cLocalidadLab.setBounds(602, 115, 215, 26);
        this.Diagnostico.add(this.cLocalidadLab);

        // el label del mail
        JLabel lMailLab = new JLabel("E-Mail:");
        lMailLab.setBounds(5, 150, 120, 26);
        this.Diagnostico.add(lMailLab);

        // el texto del mail
        this.tMailLab = new JTextField();
        this.tMailLab.setToolTipText("Correo Electrónico del Laboratorio");
        this.tMailLab.setBounds(55, 150, 270, 26);
        this.Diagnostico.add(this.tMailLab);

        // agregamos el listener
        this.tMailLab.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                // si pulsó enter
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    cActivoLab.requestFocus();
                }
            }
        });

        // el checkbox de activo
        this.cActivoLab = new JCheckBox();
        this.cActivoLab.setText("Activo");
        this.cActivoLab.setToolTipText("Indica si participa de los controles");
        this.cActivoLab.setBounds(330, 150, 70, 26);
        this.Diagnostico.add(this.cActivoLab);

        // el label de la fecha de alta
        JLabel lFechaAltaLab = new JLabel("Fecha Alta:");
        lFechaAltaLab.setBounds(410, 150, 100, 26);
        this.Diagnostico.add(lFechaAltaLab);

        // el texto de la fecha de alta
        this.tFechaAltaLab = new JTextField();
        this.tFechaAltaLab.setEditable(false);
        this.tFechaAltaLab.setToolTipText("Fecha de alta del registro");
        this.tFechaAltaLab.setBounds(485, 150, 90, 26);
        this.Diagnostico.add(this.tFechaAltaLab);

        // el label del usuario
        JLabel lUsuarioLab = new JLabel("Usuario:");
        lUsuarioLab.setBounds(580, 150, 120, 26);
        this.Diagnostico.add(lUsuarioLab);

        // el texto del usuario
        this.tUsuarioLab = new JTextField();
        this.tUsuarioLab.setEditable(false);
        this.tUsuarioLab.setToolTipText("Usuario que ingresó el registro");
        this.tUsuarioLab.setBounds(640, 150, 80, 26);
        this.Diagnostico.add(this.tUsuarioLab);

        // el checkbox si recibe muestras
        this.cRecibeMuestras = new JCheckBox("Recibe Muestras");
        this.cRecibeMuestras.setToolTipText("Marque si recibe muestras directamente");
        this.cRecibeMuestras.setBounds(0, 185, 150, 26);
        this.Diagnostico.add(this.cRecibeMuestras);

        // el label del responsable de las muestras
        JLabel lRespMuestras = new JLabel("Responsable de Muestras:");
        lRespMuestras.setBounds(155, 185, 205, 26);
        this.Diagnostico.add(lRespMuestras);

        // el combo de quien recibe las muestras
        this.cResponsableRecibe = new JComboBox<>();
        this.cResponsableRecibe.setToolTipText("Seleccione quien recibe las muestras");
        this.cResponsableRecibe.setBounds(350, 185, 345, 26);
        this.Diagnostico.add(this.cResponsableRecibe);

        // el label de comentarios
        JLabel lComentariosLab = new JLabel("Comentarios y Observaciones");
        lComentariosLab.setBounds(5, 215, 236, 26);
        this.Diagnostico.add(lComentariosLab);

        // el contenedor de los comentarios
        JScrollPane scrollLaboratorios = new JScrollPane();

        // el texto de los comentarios
        this.tComentariosLab = new JTextArea();
        this.tComentariosLab.setColumns(20);
        this.tComentariosLab.setRows(5);

        // agregamos los comentarios al contenedor
        scrollLaboratorios.setViewportView(this.tComentariosLab);
        scrollLaboratorios.setBounds(5, 240, 650, 100);
        this.Diagnostico.add(scrollLaboratorios);

        // las coordenadas gps
        tCoordenadasLaboratorio = new JTextField();
        this.tCoordenadasLaboratorio.setEditable(false);
        this.tCoordenadasLaboratorio.setVisible(false);
        this.Diagnostico.add(this.tCoordenadasLaboratorio);

        // el botón del mapa
        JButton btnMapaLaboratorio = new JButton();
        btnMapaLaboratorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/tierra.jpeg")));
        btnMapaLaboratorio.setToolTipText("Pulse para ver el Mapa");
        btnMapaLaboratorio.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMapaLaboratorioActionPerformed(evt);
            }
        });
        btnMapaLaboratorio.setBounds(630, 11, 22, 22);
        this.Diagnostico.add(btnMapaLaboratorio);

        // el botón grabar y fijamos el evento para verificar el formulario y
        // grabar
        JButton btnGrabar = new JButton("Grabar");
        btnGrabar.setBounds(690, 251, 125, 30);
        btnGrabar.setToolTipText("Pulse para grabar el registro");
        this.Diagnostico.add(btnGrabar);
        btnGrabar.setIcon(new ImageIcon(FormLaboratorios.class.getResource("/Graficos/exito.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabaLaboratorio();
            }
        });

        // el botón cancelar y fijamos el evento para cerrar el formulario
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(690, 300, 125, 30);
        btnCancelar.setToolTipText("Pulse para cerrar el formulario");
        btnCancelar.setIcon(new ImageIcon(FormLaboratorios.class.getResource("/Graficos/error.png")));
        this.Diagnostico.add(btnCancelar);

        // cargamos el combo de países

        // fijamos el usuario y la fecha de alta
        this.tFechaAltaLab.setText(this.Herramientas.FechaActual());

        // agregamos el panel al tabulador
        Tabulador.addTab("Laboratorios", 
                          new javax.swing.ImageIcon(getClass().getResource("/Graficos/mSyringe.png")), 
                          this.Diagnostico, 
                          "Datos de las Laboratorios Integrantes");
        
    }

    // método llamado al cambiar la jurisdicción del laboratorio
    private void cJurisdiccionLabItemStateChanged(java.awt.event.ItemEvent evt) {

        // obtenemos el valor seleccionado
        String Jurisdiccion = this.cJurisdiccionLab.getSelectedItem().toString();
        String Pais = this.cPaisLab.getSelectedItem().toString();

        // si es distingo de nulo
        if (!"".equals(Jurisdiccion)) {

            // elimina los elementos de la localidad
            this.cLocalidadLab.removeAllItems();

            // agrega el primer elemento en blanco
            this.cLocalidadLab.addItem("");

            // obtenemos el vector
            ResultSet nominaLocalidades = this.listaLocalidad.listaLocalidades(Jurisdiccion);

            try {

                // recorremos el vector
                while (nominaLocalidades.next()) {

                    // lo agregamos el combo
                    this.cLocalidadLab.addItem(nominaLocalidades.getString("nombre_localidad"));

                }

                // si hubo un error
            } catch (SQLException ex) {

                // presenta el mensaje
                JOptionPane.showMessageDialog(this,
                        "Error al obtener la nómina de localidades",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);

            }

            // aquí también actualizamos los elementos al combo
            // de responsables jurisdiccionales

            // limpiamos los elementos del combo
            this.cResponsableRecibe.removeAllItems();

            // agregamos el primer elemento
            this.cResponsableRecibe.addItem("");

            // obtenemos la nómina de responsables de la jurisdicción
            ResultSet listaResponsables = this.responsableLab.getNominaUsuarios(Jurisdiccion, Pais);

            try {

                // recorremos el vector
                while (listaResponsables.next()) {

                    // agregamos el valor
                    this.cResponsableRecibe.addItem(listaResponsables.getString("nombre_responsable"));

                }

                // si hubo un error
            } catch (SQLException ex) {

                // presenta el mensaje
                JOptionPane.showMessageDialog(this,
                        "Error al obtener la nómina de responsables",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);

            }

        }

    }

    // evento llamado al pulsar sobre el mapa del laboratorio
    private void btnMapaLaboratorioActionPerformed(java.awt.event.ActionEvent evt) {

        // verifica si tiene coordenadas declaradas
        if ("".equals(this.tCoordenadasLaboratorio.getText())) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                    "No hay declaradas coordenadas válidas",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);

            // si está cargado
        } else {

            // obtiene la dirección
            String Coordenadas = this.tDireccionLab.getText() + " - " + this.cJurisdiccionLab.getSelectedItem()
                    .toString() + " - " + this.cPaisLab.getSelectedItem().toString();

            // instancia el formulario, carga el mapa y lo muestra
            FormMapas Mapa = new FormMapas(this.Diagnostico);
            Mapa.setCoordenadas(Coordenadas);
            Mapa.mostrarMapa();
            Mapa.setVisible(true);

        }

    }

    // método que verifica los datos del formulario y luego procede a
    // grabar en la base de datos
    protected void grabaLaboratorio() {

        // instanciamos la clase
        Laboratorios Institucion = new Laboratorios();

        // si está editando
        if (!this.tIdLab.getText().isEmpty()) {

            // setea en la clase
            Institucion.setIdLaboratorio(Integer.parseInt(this.tIdLab.getText()));

        }

        // verifica que ingresó el nombre
        if (this.tNombreLab.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Debe ingresar el nombre del Laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tNombreLab.requestFocus();
            return;

            // si seleccionó
        } else {

            // asigna en la clase
            Institucion.setNombre(this.tNombreLab.getText());

        }

        // verifica si indicó el responsable
        if (this.tResponsableLab.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Indique quien es el responsable",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tNombreLab.requestFocus();
            return;

            // si seleccionó
        } else {

            // asigna en la clase
            Institucion.setResponsable(this.tResponsableLab.getText());

        }

        // verifica si ingresó la dirección
        if (this.tDireccionLab.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Ingrese la dirección del laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tDireccionLab.requestFocus();
            return;

            // si seleccionó
        } else {

            // asigna en la clase
            Institucion.setDireccion(this.tDireccionLab.getText());

        }

        // verifica si ingresó el código postal
        if (this.tCodigoPostalLab.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Indique el Código Postal",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tCodigoPostalLab.requestFocus();
            return;

            // si ingresó
        } else {

            // setea en la clase
            Institucion.setCodigoPostal(this.tCodigoPostalLab.getText());

        }

        // verifica si ingresó el mail
        if (this.tMailLab.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Indique el Correo Electrónico del Laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tMailLab.requestFocus();
            return;

            // si ingresó
        } else if (!this.Herramientas.esEmailCorrecto(this.tMailLab.getText())) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "El formato del Correo Electrónico es incorrecto",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tMailLab.requestFocus();
            return;

            // verifica que no esté repetido
        } else if (Institucion.verificaMail(this.tMailLab.getText())) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Esa dirección de correo ya está registrada",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tMailLab.requestFocus();
            return;

            // si no hubo ningún error
        } else {

            // asigna en la clase
            Institucion.setEmail(this.tMailLab.getText());

        }

        // si está activo
        if (this.cActivoLab.isSelected()) {
            Institucion.setActivo("Si");
        } else {
            Institucion.setActivo("No");
        }

        // verifica que seleccionó país
        if (this.cPaisLab.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Seleccione de la lista el País del Laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.tMailLab.requestFocus();
            return;

            // si seleccionó
        } else {

            // obtenemos la clave del pais y la asignamos a la clase
            int ClavePais = this.listaPaises.getClavePais(this.cPaisLab.getSelectedItem().toString());
            Institucion.setIdPais(ClavePais);

        }

        // verifica que seleccionó jurisdicción
        if (this.cJurisdiccionLab.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Seleccione de la lista la Jurisdicción del Laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.cJurisdiccionLab.requestFocus();
            return;

        }

        // verifica que seleccionó localidad
        if (this.cLocalidadLab.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Seleccione de la lista la localidad del Laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.cLocalidadLab.requestFocus();
            return;

            // si seleccionó
        } else {

            // obtiene la clave de la localidad y asigna en la clase
            String ClaveLocalidad = this.listaLocalidad.getClaveLocalidad(this.cJurisdiccionLab.getSelectedItem().toString(),
                    this.cLocalidadLab.getSelectedItem().toString());
            Institucion.setIdLocalidad(ClaveLocalidad);

        }

        // si recibe muestras
        if (this.cRecibeMuestras.isSelected()) {
            Institucion.setRecibeMuestras("Si");
        } else {
            Institucion.setRecibeMuestras("No");
        }

        // si no indicó el responsable de las muestras
        if (!this.cResponsableRecibe.getSelectedItem().toString().isEmpty()) {

            // si indicó que no recibe muestras
            if (!this.cRecibeMuestras.isSelected()) {

                // presenta el mensaje
                JOptionPane.showMessageDialog(this,
                        "Ha indicado que el laboratorio no recibe\n" +
                                "muestras, seleccione el responsable de recibirlas",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                this.cRecibeMuestras.requestFocus();
                return;

            }

            // si está indicado el responsable de las muestras
        } else {

            // obtenemos la clave del usuario seleccionado
            int ClaveResponsable = this.responsableLab.getIdResponsable(this.cResponsableRecibe.getSelectedItem()
                    .toString(),
                    this.cPaisLab.getSelectedItem().toString(),
                    this.cJurisdiccionLab.getSelectedItem().toString());

            // fijamos las propiedades en la clase
            Institucion.setIdRecibeMuestras(ClaveResponsable);
            Institucion.setRecibeMuestras("No");

        }

        // verifica que seleccionó la dependencia
        if (this.cDependencia.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                    "Indique la dependencia del laboratorio",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.cDependencia.requestFocus();
            return;

            // si seleccionó
        } else {

            // obtiene la clave y asigna en la clase
            int ClaveDependencia = this.listaDependencia.getClaveDependencia(this.cDependencia.getSelectedItem().toString());
            Institucion.setIdDependencia(ClaveDependencia);

        }

        // si ingresó comentarios
        if (!this.tComentariosLab.getText().isEmpty()) {
            Institucion.setObservaciones(this.tComentariosLab.getText());
        }

        // graba el registro
        int Clave = Institucion.grabarLaboratorio();

        // actualiza la id del registro
        this.tIdLab.setText(Integer.toString(Clave));

        // setea el foco
        this.tNombreLab.requestFocus();

    }

}