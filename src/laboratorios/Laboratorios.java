/*

    Nombre: laboratorios
    Fecha: 31/05/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones de la base de datos de la
                 tabla de laboratorios

 */

// definición del paquete
package laboratorios;

//importamos las librerías
import java.sql.ResultSet;
import java.sql.SQLException;

import dbApi.Conexion;
import funciones.Utilidades;
import seguridad.Seguridad;

// declaración de la clase
public class Laboratorios {

	// declaración de variables de clase
    protected Conexion Enlace;               // puntero a la base de datos
    protected int IdLaboratorio;             // clave del laboratorio
    protected String Nombre;                 // nombre del laboratorio
    protected String Responsable;            // responsable del laboratorio
    protected String Pais;                   // país donde queda
    protected int IdPais;                    // clave del país
    protected String Localidad;              // nombre de la localidad
    protected String IdLocalidad;            // clave indec de la localidad
    protected String Provincia;              // nombre de la provincia
    protected String Direccion;              // dirección postal
    protected String CodigoPostal;           // código postal correspondiente
    protected String Coordenadas;            // coordenadas gps del laboratorio
    protected String Dependencia;            // nombre de la dependencia
    protected int IdDependencia;             // clave de la dependencia
    protected String Activo;                 // si el laboratorio está activo
    protected String RecibeMuestras;         // si recibe muestras directamente
    protected String NombreRecibe;           // nombre de quien recibe las muestras
    protected int IdRecibe;                  // clave del usuario que recibe las muestras
    protected String EMail;                  // dirección de correo electrónico
    protected String FechaAlta;              // fecha de alta del registro
    protected String Observaciones;          // observaciones y comentarios
    protected String Usuario;                // usuario que actualizó / insertó
    protected int IdUsuario;                 // clave del responsable

	// constructor de la clase
	public Laboratorios(){

		// abrimos la conexión con la base
        this.Enlace = new Conexion();

		// inicializamos las variables
        this.IdLaboratorio = 0;
        this.Nombre = "";
        this.Responsable = "";
        this.Pais = "";
        this.IdPais = 0;
        this.Localidad = "";
        this.IdLocalidad = "";
        this.Provincia = "";
        this.Direccion = "";
        this.Coordenadas = "";
        this.CodigoPostal = "";
        this.Dependencia = "";
        this.IdDependencia = 0;
        this.Activo = "";
        this.RecibeMuestras = "";
        this.IdRecibe = 0;
        this.NombreRecibe = "";
        this.EMail = "";
        this.FechaAlta = "";
        this.Observaciones = "";
        this.Usuario = "";
        this.IdUsuario = 0;

	}

    // métodos de asignación de valores
    public void setIdLaboratorio(int idlaboratorio) {
        this.IdLaboratorio = idlaboratorio;
    }
    public void setNombre(String nombre){
        this.Nombre = nombre;
    }
    public void setResponsable(String responsable){
        this.Responsable = responsable;
    }
    public void setIdPais(int idpais){
        this.IdPais = idpais;
    }
    public void setIdLocalidad(String idlocalidad){
        this.IdLocalidad = idlocalidad;
    }
    public void setDireccion(String direccion){
        this.Direccion = direccion;
    }
    public void setCodigoPostal(String codigopostal){
        this.CodigoPostal = codigopostal;
    }
    public void setIdDependencia(int iddependencia){
        this.IdDependencia = iddependencia;
    }
    public void setActivo(String activo){
        this.Activo = activo;
    }
    public void setRecibeMuestras(String recibe){
        this.RecibeMuestras = recibe;
    }
    public void setIdRecibeMuestras(int idrecibe){
        this.IdRecibe = idrecibe;
    }
    public void setEmail(String email){
        this.EMail = email;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }
    public void setIdUsuario(int idusuario){
        this.IdUsuario = idusuario;
    }

    // métodos de obtención de datos
    public int getIdLaboratorio(){
        return this.IdLaboratorio;
    }
    public String getNombre(){
        return this.Nombre;
    }
    public String getResponsable(){
        return this.Responsable;
    }
    public String getPais(){
        return this.Pais;
    }
    public String getLocalidad(){
        return this.Localidad;
    }
    public String getProvincia(){
        return this.Provincia;
    }
    public String getDireccion(){
        return this.Direccion;
    }
    public String getCodigoPostal(){
        return this.CodigoPostal;
    }
    public String getCoordenadas(){
        return this.Coordenadas;
    }
    public String getDependencia(){
        return this.Dependencia;
    }
    public String getActivo(){
        return this.Activo;
    }
    public String getRecibe(){
        return this.RecibeMuestras;
    }
    public String getNombreRecibe(){
        return this.NombreRecibe;
    }
    public String getEmail(){
        return this.EMail;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }

    /**
     * @param texto string con la cadena a buscar
     * @return recordset con los registros hallados
     * Método que recibe un texto y lo busca en la tabla de laboratorios
     * retorna el vector con los registros encontrados
     */
    public ResultSet buscaLaboratorio(String texto){

        // definición de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT cce.laboratorios.ID AS idlaboratorio, "
                 + "       UCASE(cce.laboratorios.NOMBRE) AS laboratorio, "
                 + "       CCE.laboratorios.RESPONSABLE AS responsable, "
                 + "       diccionarios.localidades.NOMLOC AS localidad_laboratorio, "
                 + "       diccionarios.provincias.NOM_PROV AS jurisdiccion "
                 + "FROM cce.laboratorios LEFT JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC "
                 + "                      LEFT JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV "
                 + "WHERE cce.laboratorios.NOMBRE LIKE '%" + texto + "%' OR "
                 + "      cce.laboratorios.RESPONSABLE LIKE '%" + texto + "%' OR "
                 + "      diccionarios.localidades.NOMLOC LIKE '%" + texto + "%' OR "
                 + "      diccionarios.provincias.NOM_PROV LIKE '%" + texto + "%' "
                 + "ORDER BY cce.laboratorios.NOMBRE; ";

        // ejecutamos la consulta y retornamos el array
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @param idlaboratorio la clave del registro
     * Método que recibe la clave del laboratorio e instancia las variables
     * de clase con los valores del registro
     */
    public void getDatosLaboratorio(int idlaboratorio){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT cce.laboratorios.ID AS id_laboratorio, "
                 + "       UCASE(cce.laboratorios.NOMBRE) AS nombre, "
                 + "       UCASE(cce.laboratorios.RESPONSABLE) AS responsable, "
                 + "       diccionarios.paises.NOMBRE AS pais, "
                 + "       diccionarios.localidades.NOMLOC AS localidad, "
                 + "       diccionarios.provincias.NOM_PROV AS provincia, "
                 + "       UCASE(cce.laboratorios.DIRECCION) AS direccion, "
                 + "       cce.laboratorios.CODIGO_POSTAL AS codigo_postal, "
                 + "       cce.laboratorios.COORDENADAS AS coordenadas, "
                 + "       diccionarios.dependencias.DEPENDENCIA AS dependencia, "
                 + "       cce.laboratorios.E_MAIL AS e_mail, "
                 + "       DATE_FORMAT(cce.laboratorios.FECHA_ALTA, '%d/%m/%Y') AS fecha_alta, "
                 + "       cce.laboratorios.ACTIVO AS activo, "
                 + "       cce.laboratorios.RECIBE_MUESTRAS_CHAGAS AS recibe_muestras "
                 + "       cce.responsables.NOMBRE AS nombre_recibe "
                 + "       cce.laboratorios.OBSERVACIONES AS observaciones, "
                 + "       usuarios.USUARIO AS usuario "
                 + "FROM CCE.laboratorios INNER JOIN diccionarios.dependencias ON cce.laboratorios.DEPENDENCIA = diccionarios.dependencias.ID "
                 + "                      LEFT JOIN diccionarios.localidades ON cce.laboratorios.LOCALIDAD = diccionarios.localidades.CODLOC "
                 + "                      LEFT JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV "
                 + "                      LEFT JOIN diccionarios.paises ON cce.laboratorios.PAIS = diccionarios.paises.ID "
                 + "                      LEFT JOIN cce.responsables ON cce.laboratorios.ID_RECIBE = cce.responsables.ID "
                 + "                      INNER JOIN cce.responsables AS usuarios ON cce.laboratorios.USUARIO = usuarios.ID "
                 + "WHERE cce.laboratorios.ID = '" + idlaboratorio + "';";

        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos los valores a las variables de clase
            this.IdLaboratorio = Resultado.getInt("id_laboratorio");
            this.Nombre = Resultado.getString("nombre");
            this.Responsable = Resultado.getString("responsable");
            this.Pais = Resultado.getString("pais");
            this.Localidad = Resultado.getString("localidad");
            this.Provincia = Resultado.getString("provincia");
            this.Direccion = Resultado.getString("direccion");
            this.CodigoPostal = Resultado.getString("codigo_postal");
            this.Coordenadas = Resultado.getString("coordenadas");
            this.Dependencia = Resultado.getString("dependencia");
            this.Activo = Resultado.getString("activo");
            this.RecibeMuestras = Resultado.getString("recibe_muestras");
            this.NombreRecibe = Resultado.getString("nombre_recibe");
            this.EMail = Resultado.getString("e_mail");
            this.FechaAlta = Resultado.getString("fecha_alta");
            this.Observaciones = Resultado.getString("observaciones");
            this.Usuario = Resultado.getString("usuario");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @return id del registro afectado
     * Método que produce la consulta de inserción o edición según
     * el caso y retorna la id del registro afectado
     * Usamos la sentencia preparedstatement para evitar conflictos
     * con las cadenas de 8 bits
     */
    public int grabarLaboratorio(){

        // declaración de variables
        String Consulta;
        java.sql.PreparedStatement psInsertar;
        java.sql.Connection Puntero;

        // primero actualizamos las coordenadas
        String Domicilio = this.Direccion + " - " + this.Localidad + " - " + this.Provincia + " - " + this.Pais;
        Utilidades Codificar = new Utilidades();
        this.Coordenadas = Codificar.getCoordenadas(Domicilio);

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // si está definida la clave
        if (this.IdLaboratorio != 0){

            // produce la consulta de edición
            Consulta = "UPDATE laboratorios SET "
                     + "       NOMBRE = ?, "
                     + "       RESPONSABLE = ?, "
                     + "       PAIS = ?, "
                     + "       LOCALIDAD = ?, "
                     + "       DIRECCION = ?, "
                     + "       CODIGO_POSTAL = ?, "
                     + "       COORDENADAS = ?, "
                     + "       DEPENDENCIA = ?, "
                     + "       ACTIVO = ?, "
                     + "       RECIBE_MUESTRAS_CHAGAS = ?, "
                     + "       ID_RECIBE = ?, "
                     + "       E_MAIL = ?, "
                     + "       OBSERVACIONES = ?, "
                     + "       USUARIO = ? "
                     + "WHERE laboratorios.ID = ?;";

            try {

                // ahora preparamos la consulta
                psInsertar = Puntero.prepareStatement(Consulta);

                // asignamos los parámetros de la consulta
                psInsertar.setString(1, this.Nombre);
                psInsertar.setString(2, this.Responsable);
                psInsertar.setInt(3, this.IdPais);
                psInsertar.setString(4, this.IdLocalidad);
                psInsertar.setString(5, this.Direccion);
                psInsertar.setString(6, this.CodigoPostal);
                psInsertar.setString(7, this.Coordenadas);
                psInsertar.setInt(8, this.IdDependencia);
                psInsertar.setString(9, this.Activo);
                psInsertar.setString(10, this.RecibeMuestras);
                psInsertar.setInt(11, this.IdRecibe);
                psInsertar.setString(12, this.EMail);
                psInsertar.setString(13, this.Observaciones);
                psInsertar.setInt(14, Seguridad.Id);
                psInsertar.setInt(15, this.IdLaboratorio);

                // ejecutamos la edición
                psInsertar.executeUpdate();

            // si hubo un error
            } catch (SQLException ex) {

                // presenta el mensaje de error
                System.out.println(ex.getMessage());

            }


        // si no está definida
        } else {

            // produce la consulta de inserción
            Consulta = "INSERT INTO laboratorios "
                     + "       (NOMBRE, "
                     + "        RESPONSABLE, "
                     + "        PAIS, "
                     + "        LOCALIDAD, "
                     + "        DIRECCION, "
                     + "        CODIGO_POSTAL, "
                     + "        COORDENADAS, "
                     + "        DEPENDENCIA, "
                     + "        E_MAIL, "
                     + "        ACTIVO, "
                     + "        RECIBE_MUESTRAS_CHAGAS, "
                     + "        ID_RECIBE, "
                     + "        OBSERVACIONES, "
                     + "        USUARIO) "
                     + "       VALUES "
                     + "       (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            try {

                // ahora preparamos la consulta
                psInsertar = Puntero.prepareStatement(Consulta);

                // asignamos los parámetros de la consulta
                psInsertar.setString(1, this.Nombre);
                psInsertar.setString(2, this.Responsable);
                psInsertar.setInt(3, this.IdPais);
                psInsertar.setString(4, this.IdLocalidad);
                psInsertar.setString(5, this.Direccion);
                psInsertar.setString(6, this.CodigoPostal);
                psInsertar.setString(7, this.Coordenadas);
                psInsertar.setInt(8, this.IdDependencia);
                psInsertar.setString(9, this.EMail);
                psInsertar.setString(10, this.Activo);
                psInsertar.setString(11, this.RecibeMuestras);
                psInsertar.setInt(12, this.IdRecibe);
                psInsertar.setString(13, this.Observaciones);
                psInsertar.setInt(14, Seguridad.Id);

                // ejecutamos la edición
                psInsertar.execute();

            // si hubo un error
            } catch (SQLException ex) {

                // presenta el mensaje de error
                System.out.println(ex.getMessage());

            }


        }

        // si estaba dando altas
        if (this.IdLaboratorio == 0){

            // asigna la id del registro
            this.IdLaboratorio = this.Enlace.UltimoInsertado();

        }

        // retorna la id
        return this.IdLaboratorio;

    }

    /**
     * @param provincia texto con la provincia del laboratorio
     * @param laboratorio texto con el nombre del laboratorio
     * @return entero con la clave del laboratorio
     * Método que recibe como parámetro el nombre de un laboratorio y la
     * provincia del mismo (puede haber mas de un laboratorio con el mismo
     * nombre) y retorna la clave del laboratorio
     */
    public int getClaveLaboratorio(String provincia, String laboratorio){

        // declaración de variables
        String Consulta;
        int claveLaboratorio = 0;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT laboratorios.ID AS id_laboratorio "
                 + "FROM laboratorios INNER JOIN calidad.localidades ON laboratorios.LOCALIDAD = calidad.localidades.CODLOC "
                 + "                  INNER JOIN calidad.provincias ON calidad.localidades.CODPCIA = calidad.provincias.COD_PROV "
                 + "WHERE laboratorios.NOMBRE = '" + laboratorio + "' AND "
                 + "      calidad.provincias.NOM_PROV = '" + provincia + "'; ";

        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            claveLaboratorio = Resultado.getInt("id_laboratorio");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos la clave
        return claveLaboratorio;

    }

    /**
     * @param laboratorio parte del nombre del laboratorio
     * @param provincia nombre de la provincia
     * @return resultset con los registros hallados
     * Método utilizado en el abm de usuarios para autorizar a cargar
     * específicamente un laboratorio, recibe parte del nombre del
     * laboratorio y la provincia y retorna los registros coincidentes
     */
    public ResultSet nominaLaboratorios(String laboratorio, String provincia){

        // definimos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT laboratorios.ID AS id_laboratorio, "
                 + "       laboratorios.NOMBRE AS laboratorio "
                 + "FROM laboratorios INNER JOIN calidad.localidades ON laboratorios.LOCALIDAD = calidad.localidades.CODLOC "
                 + "                  INNER JOIN calidad.provincias ON calidad.localidades.CODPCIA = calidad.provincias.COD_PROV "
                 + "WHERE calidad.provincias.NOM_PROV = '" + provincia + "' AND "
                 + "      laboratorios.NOMBRE LIKE '" + laboratorio + "' "
                 + "ORDER BY laboratorios.NOMBRE; ";

        // ejecutamos la consulta y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @param mail cadena con el correo del laboratorio
     * @return boolean si encontró el correo
     * Método que retorna verdadero si el correo ya se encuentra declarado
     * en la base
     */
    public boolean verificaMail(String mail){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        boolean encontrado = false;

        // compone la consulta
        Consulta = "SELECT COUNT(*) AS registros "
                 + "FROM laboratorios "
                 + "WHERE laboratorios.E_MAIL = '" + mail + "';";

        // obtenemos los registros
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // verificamos si encontró
            if (Resultado.getInt("registros") == 0){

                // retorna falso
                encontrado = false;

            // si lo encontró
            } else {

                // retorna verdadero
                encontrado = true;

            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retorna el resultado de la operación
        return encontrado;

    }

}
