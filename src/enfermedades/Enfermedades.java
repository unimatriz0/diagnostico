/*

    Nombre: Enfermedades
    Fecha: 18/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que contiene los métodos para operar sobre la
                 tabla de enfermedades declaradas

*/

// definición del paquete
package enfermedades;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 *
 */
public class Enfermedades {

    // declaraciòn de variables
    protected int IdEnfermedad;                // clave del registro
    protected String Enfermedad;               // descripción de la enfermedad
    protected String Usuario;                  // usuario de la base
    protected String FechaAlta;                // fecha de alta del registro

    // el puntero a la base de datos
    protected Conexion Enlace;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Enfermedades(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initEnfermedades();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initEnfermedades(){

        // inicializamos las variables
        this.IdEnfermedad = 0;
        this.Enfermedad = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // mètodos de asignaciòn de valores
    public void setIdEnfermedad(int idenfermedad){
        this.IdEnfermedad = idenfermedad;
    }
    public void setEnfermedad(String enfermedad){
        this.Enfermedad = enfermedad;
    }

    // métodos de retorno de valores
    public int getIdEnfermedad(){
        return this.IdEnfermedad;
    }
    public String getEnfermedad(){
        return this.Enfermedad;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param enfermedad - nombre de la enfermedad
     * @return clave - entero con la clave del registro
     * Mètodo que recibe como paràmetro el nombre de una enfermedad
     * y retorna la clave del registro
     */
    public int getClaveEnfermedad(String enfermedad){

        // declaración de variables
        int claveEnfermedad = 0;
        String Consulta;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.enfermedades.id AS clave " +
                   "FROM diagnostico.enfermedades " +
                   "WHERE diagnostico.enfermedades.enfermedad = '" + enfermedad + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            if (Resultado.next()){
                claveEnfermedad = Resultado.getInt("clave");
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos la clave
        return claveEnfermedad;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * @return clave - entero con la clave del registro
     * Mètodo que recibe como paràmetro la clave de una
     * enfermedad y retorna el nombre
     */
    public String getNombreEnfermedad(int clave){

        // declaración de variables
        String nombreEnfermedad = "";
        String Consulta;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.enfermedades.enfermedad AS enfermedad " +
                   "FROM diagnostico.enfermedades " +
                   "WHERE diagnostico.enfermedades.id = '" + clave + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            if (Resultado.next()){
                nombreEnfermedad = Resultado.getString("enfermedad");
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos la clave
        return nombreEnfermedad;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return nomina - resultset con el listado de enfermedades
     * Mètodo que retorna la nòmina de enfermedades registradas
     * en el sistema
     */
    public ResultSet nominaEnfermedades(){

        // declaración de variables
        String Consulta;
        ResultSet listaEnfermedades;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.enfermedades.id AS id, " +
                   "       diagnostico.enfermedades.enfermedad AS enfermedad " +
                   "FROM diagnostico.enfermedades " +
                   "ORDER BY diagnostico.enfermedades.enfermedad;";
        listaEnfermedades = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return listaEnfermedades;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return IdEnfermedad - clave del registro
     * Mètodo que ejecuta la consulta de ediciòn o inserciòn
     * segùn corresponda y retorna la id del registro
     */
    public int grabaEnfermedad(){

        // si està insertando
        if (this.IdEnfermedad == 0){
            this.nuevaEnfermedad();
        } else {
            this.editaEnfermedad();
        }

        // retorna la id
        return this.IdEnfermedad;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo protegido que ejecuta la consulta de inserciòn
     */
    protected void nuevaEnfermedad(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "INSERT INTO diagnostico.enfermedades "
                 + "       (enfermedad, "
                 + "        id_usuario) "
                 + "       VALUES "
                 + "       (?,?); ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString (1, this.Enfermedad);
            preparedStmt.setInt    (2, Seguridad.Id);

            // ejecutamos la consulta
            preparedStmt.execute();

            // obtenemos la id
            this.IdEnfermedad = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de ediciòn
     */
    protected void editaEnfermedad(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "UPDATE diagnostico.enfermedades SET "
                 + "       enfermedad = ?, "
                 + "       id_usuario = ? "
                 + "WHERE diagnostico.enfermedades.id = ?; ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString (1, this.Enfermedad);
            preparedStmt.setInt    (2, Seguridad.Id);
            preparedStmt.setInt    (3, this.IdEnfermedad);

            // ejecutamos la consulta
            preparedStmt.execute();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param enfermedad - nombre a verificar
     * @return Correcto - boolean true si puede insertar
     * Método pùblico que verifica no se encuentre declarada
     * una enfemredad, si el usuario puede insertar retorna
     * verdadero
     */
    public boolean validaEnfermedad(String enfermedad){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.enfermedades.id) AS registros "
                 + "FROM diagnostico.enfermedades "
                 + "WHERE diagnostico.enfermedades.enfermedad = '" + enfermedad + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * @return Correcto - resultado de la operación
     * Método que verifica si una enfermedad no está asignada
     * a un paciente y puede ser eliminada
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.otras_enfermedades.id) AS registros "
                 + "FROM diagnostico.otras_enfermedades "
                 + "WHERE diagnostico.otras_enfermedades.id_enfermedad = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que ejecuta la consulta de eliminación
     */
    public void borraEnfermedad(int clave){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.enfermedades " +
                   "WHERE diagnostico.enfermedades.id = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * asigna los valores en las variables de clase
     */
    public void getDatosEnfermedad(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.enfermedades.id AS id, " +
                   "       diagnostico.enfermedades.enfermedad AS enferemedad, " +
                   "       DATE_FORMAT(diagnostico.enfermedades.fecha_alta, '%d/%m/%Y') AS fecha, " +
                   "       cce.responsables.usuario AS usuario " +
                   "FROM diagnostico.enfermedades INNER JOIN cce.responsables ON diagnostico.enfermedades.id_usuario = cce.responsables.id " +
                   "WHERE diagnostico.enfermedades.id = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos en las variables de clase
            this.IdEnfermedad = Resultado.getInt("id");
            this.Enfermedad = Resultado.getString("enfermedad");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

}
