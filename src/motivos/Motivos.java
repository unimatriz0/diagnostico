/*

    Nombre: Motivos
    Fecha: 20/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que contiene los métodos para operar sobre la
                 tabla de fuentes de motivos de consulta

*/

// definiciòn del paquete
package motivos;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definiciòn de la clase
 */
public class Motivos {

	// declaraciòn de variables
	protected int IdMotivo;              // clave del registro
	protected String Motivo;             // descripciòn del motivo
    protected String Usuario;            // nombre del usuario
    protected String FechaAlta;          // fecha de alta del registro

    // el puntero a la base de datos
    protected Conexion Enlace;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Motivos(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initMotivos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initMotivos(){

    	// inicializamos las variables
    	this.IdMotivo = 0;
    	this.Motivo = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setIdMotivo(int idmotivo){
    	this.IdMotivo = idmotivo;
    }
    public void setMotivo(String motivo){
    	this.Motivo = motivo;
    }

    // métodos de retorno de valores
    public int getIdMotivo(){
        return this.IdMotivo;
    }
    public String getMotivo(){
        return this.Motivo;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idmotivo - entero con la clave del registro
     * @return Motivo - string con el tipo
     * Método que recibe como parámetro la clave de un motivo
     * retorna su nombre
     */
    public String getNombreMotivo(int idmotivo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        String Motivo = "";

        // componemos la consulta
        Consulta = "SELECT diagnostico.motivos.motivo AS motivo " +
                   "FROM diagnostico.motivos " +
                   "WHERE diagnostico.motivos.id_motivo = '" + idmotivo + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);


        try {

            // obtenemos el registro
            Resultado.next();
            Motivo = Resultado.getString("motivo");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos el nombre
        return Motivo;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param motivo - string con el nombre de un motivo
     * @return id del motivo de consulta
     * Método que recibe como parámetro el nombre de un
     * motivo de consulta y retorna la clave del mismo
     */
    public int getClaveMotivo(String motivo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int clave = 0;

        // componemos la consulta
        Consulta = "SELECT diagnostico.motivos.id_motivo AS clave " +
                   "FROM diagnostico.motivos " +
                   "WHERE diagnostico.motivos.motivo = '" + motivo + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);


        try {

            // obtenemos el registro
            Resultado.next();
            clave = Resultado.getInt("clave");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos el nombre
        return clave;

    }


    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la nomina de motivos de consulta
     */
    public ResultSet nominaMotivos(){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.motivos.id_motivo AS id, " +
                   "       diagnostico.motivos.motivo AS motivo " +
                   "FROM diagnostico.motivos " +
                   "ORDER BY diagnostico.motivos.motivo; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int clave del registro afectado
     * Método que ejecuta según corresponda la consulta de inserción
     * o edición y retorna la id del registro afectado
     */
    public int grabaMotivo(){

    	// si está insertando
    	if (this.IdMotivo == 0){
    		this.nuevoMotivo();
    	} else {
    		this.editaMotivo();
    	}

    	// retorna la id
    	return this.IdMotivo;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un nuevo registro
     */
    protected void nuevoMotivo(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "INSERT INTO diagnostico.motivos "
    			+ "        (motivo, "
    			+ "         id_usuario) "
    			+ "        VALUES "
    			+ "        (?,?)";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Motivo);
	        preparedStmt.setInt    (2, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	        // obtenemos la id
	        this.IdMotivo = this.Enlace.UltimoInsertado();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la edición del registro
     */
    protected void editaMotivo(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "UPDATE diagnostico.motivos SET "
    			+ "        motivo = ?, "
    			+ "        id_usuario = ? "
    			+ " WHERE diagnostico.motivos.id_motivo = ?; ";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Motivo);
	        preparedStmt.setInt    (2, Seguridad.Id);
	        preparedStmt.setInt    (3, this.IdMotivo);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un registro 
     * y asigna los valores en las variables de clase
     */
    public void getDatosMotivo(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.motivos.id_motivo AS id, " +
                   "       diagnostico.motivos.motivo AS motivo, " +
                   "       cce.responsables.usuario AS usuario, " +
                   "       DATE_FORMAT(diagnostico.motivos.fecha, '%d/%m/%Y') AS fecha_alta " +
                   "FROM diagnostico.motivos " +
                   "WHERE diagnostico.motivos.id_motivo = '" + clave + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos los valores en las variables de clase
            this.IdMotivo = Resultado.getInt("id");
            this.Motivo = Resultado.getString("motivo");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param motivo - cadena con el motivo
     * @return boolean
     * Método que verifica no se encuentre declarado un motivo
     * de consulta, el que recibe como parámetro, si se puede
     * insertar retorna verdadero
     */
    public boolean validaMotivo(String motivo){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.motivos.id_motivo) AS registros "
                 + "FROM diagnostico.motivos "
                 + "WHERE diagnostico.motivos.motivo = '" + motivo + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que verifica que un motivo no tenga hijos y 
     * pueda ser eliminado
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros " + 
                   "FROM diagnostico.personas " +
                   "WHERE diagnostico.personas.motivo = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

            // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que ejecuta la consulta de eliminación
     */
    public void borraMotivo(int clave){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.motivos " +
                   "WHERE diagnostico.motivos.id_motivo = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);
        
    }

}
